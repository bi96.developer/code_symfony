<?php

namespace App\EventSubscriber;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;

class KernelRequestSubscriber implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Environment
     */
    private $twig;

    public function __construct(
        ContainerInterface $container,
        Environment $twig
    ) {
        $this->container = $container;
        $this->twig = $twig;
    }

    public function setLanguage($event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if ($locale = $event->getRequest()->headers->get('X-Language')) {
            $locale = strtolower($locale);
            $event->getRequest()->setLocale($locale);
        }
    }

    public function underConstruction($event)
    {
        $isUnderConstruction = $this->container->getParameter('is_under_construction');

        if (!$isUnderConstruction) {
            return;
        }

        // Render construction page.
        $pageContent = $this->twig->render('frontend/page/construction.html.twig');

        $event->setResponse(
            new Response(
                $pageContent
            )
        );

        $event->stopPropagation();
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['setLanguage', 16],
                ['underConstruction', 1]
            ],
        ];
    }
}
