<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

use App\Service\RequestService;

class KernelControllerSubscriber implements EventSubscriberInterface
{
    public function __construct(
        RequestService $requestService
    ) {
        $this->requestService = $requestService;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if (!is_array($controllers = $event->getController())) {
            return;
        }
        
        list($controller, $methodName) = $controllers;
        // cache
        $this->requestService->startCache($methodName, $controller);
        
        // permission
        $this->requestService->checkPermission($methodName, $controller);
        
        // log
        $this->requestService->startLogging($methodName, $controller);
        
    }
}
