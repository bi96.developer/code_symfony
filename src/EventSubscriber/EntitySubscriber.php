<?php

namespace App\EventSubscriber;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

use App\Service\BaseService;

class EntitySubscriber extends BaseService implements EventSubscriber
{
    public function __construct(BaseService $baseService)
    {
        $this->reflectFromParent($baseService);
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate
        ];
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityName = $this->commonService->getClassName($entity, true);
        $request = $this->requestStack->getCurrentRequest();
        if ($request) {
            $inputArr = array_merge(
                $request->request->all(),
                $request->query->all(),
                json_decode($request->getContent(), 1) ?? []
            );
        }
        if ($entityName == 'School') {
            if (isset($inputArr['crawlUpdate'])) {
                if ($entity->getCrawlStatus() === 'pending') {
                    $entity->setUpdateData(null);
                    $entity->setCrawlStatus('finish');
                    $this->schoolRepo->save($entity, true);
                    $schoolCrawl = $this->schoolCrawlService->newEntity();
                    $schoolCrawl->setSchool($entity);
                    $schoolCrawl->setData($inputArr);
                    $this->schoolCrawlRepo->save($schoolCrawl);
                }
            } else {
                $changes = $this->getEntityManager()->getUnitOfWork()->getEntityChangeSet($entity);
                $crawlFields = $this->SCHOOL_CRAWL_FIELDS;
                $updateData = [];
                foreach ($changes as $field => $change) {
                    if (in_array($field, $crawlFields) && $change[0] != $change[1]) {
                        $updateData[] = $field;
                    }
                }
                $updateData = array_filter(
                    array_unique(
                        array_merge(
                            $updateData,
                            explode(',', $entity->getUpdateData())
                        )
                    )
                );
                if (count($updateData) > 0) {
                    $entity->setUpdateData(implode(',', $updateData));
                    if ($entity->getCrawlStatus() !== 'pending') {
                        $entity->setCrawlStatus('crawlable');
                    }
                    $this->schoolRepo->save($entity, true);
                }
            }
        }
    }
}