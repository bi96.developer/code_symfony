<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

use App\Service\RequestService;

class KernelResponseSubscriber implements EventSubscriberInterface
{
    public function __construct(
        RequestService $requestService
    ) {
        $this->requestService = $requestService;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    public function onKernelResponse(FilterResponseEvent $event) {
        if(is_null($this->requestService->exception)) {
            $this
            ->requestService
            ->saveLog(
                $event->getResponse()->getStatusCode(),
                $event->getResponse()->getContent()
            );
            if($this->requestService->useCache === true) {
                $cache = $this->requestService->cache;
                $cacheResponse = $cache->getItem($this->requestService->cacheResponseKey);
                $payload = [
                    'code' => $event->getResponse()->getStatusCode(),
                    'data' => json_decode($event->getResponse()->getContent(), 1)
                ];
                $cacheResponse->set(json_encode($payload, 1));
                $cache->save($cacheResponse);
            }
        }
    }
}
