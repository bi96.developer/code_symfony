<?php

namespace App\EventSubscriber;

use App\Exception\BadRequestException;
use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use App\Exception\CacheHitResponse;
use App\Exception\ValidateException;
use App\Service\RequestService;

class KernelExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        RequestService $requestService,
        ParameterBagInterface $params
    ) {
        $this->requestService = $requestService;
        $this->params = $params;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['onKernelException', 10]
            ],
        ];
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $userAgent = $event->getRequest()->headers->get('User-Agent');
        if (strpos($userAgent, 'PostmanRuntime') !== false
            || $event->getRequest()->headers->get('X-Debug')
        ) {
            //return;
        }

        $exception = $event->getException();

        if ($exception instanceof NotFoundHttpException
            && strpos($exception->getMessage(), '/uploads/') !== false
        ) {
            return;
        }
        $response = new JsonResponse();

        if ($exception instanceof CacheHitResponse) {
            $this->requestService->exception = $exception;
            $event->allowCustomResponseCode();
            $response->setStatusCode(($exception->getCode() == 500) ? 200 : $exception->getCode());
            $response->setData(json_decode($exception->getMessage(), 1));

            /*
            $this
            ->requestService
            ->saveLog(
                $event->getResponse()->getStatusCode(),
                $event->getResponse()->getContent()
            );
            */
        } elseif ($exception instanceof ValidationException) {
            $response->setStatusCode($exception->getCode());
            $response->setData([
                'error' => [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                    'violations' => $exception->getViolations()
                ]
            ]);
        } else {

            $exceptionCode = $exception->getCode() != 0
                ? $exception->getCode()
                : (method_exists($exception, 'getStatusCode')
                    ? $exception->getStatusCode()
                    : 500);

            $message = $exception->getMessage();

            $validates = [];

            if ($exception instanceof ValidateException) {
                $validates = $exception->getMessages();
            }

            $responseData = [
                'error' => [
                    'code' => $exceptionCode,
                    'message'  => $message
                ]
            ];

            if($this->params->get('kernel.environment') == 'dev') {
                $responseData['error']['trace'] = $responseData['trace'] = [
                    'position' => $exception->getLine() . " of " . $exception->getFile(),
                    'stack' => $exception->getTrace()
                ];
            }

            if(count($validates) > 0) {
                $responseData['error']['details'] = $validates;
            }

            $response->setStatusCode($exceptionCode);
            $response->setData($responseData);

            $this->requestService->exception = $exception;
            $this->requestService->exceptionCode = $exceptionCode;

//            $this
//            ->requestService
//            ->saveLog(
//                (method_exists($exception, 'getStatusCode')
//                    ? $exception->getStatusCode()
//                    : 500),
//                $event->getResponse()->getContent(),
//                null,
//                true
//            );
        }

        //
        if ($event->getRequest()->isXmlHttpRequest()) {
            $event->setResponse($response);
        }
    }
}
