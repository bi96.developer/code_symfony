<?php

namespace App\Command;

use App\Entity\SyncLog;
use App\Service\SchoolTypeService;
use App\Service\SchoolService;
use App\Repository\SyncLogRepository;
use App\Service\SyncLogService;
use App\Service\RedirectionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;

class SetPointSchoolCommand extends Command
{
    protected static $defaultName = 'app:set-point-school';
    protected static $defaultDescription = 'Set point school';

    /**
     * @var SchoolService
     */
    private $schoolService;

    public function __construct(
        string $name = null,
        SchoolService $schoolService
    ) {
        parent::__construct($name);

        $this->schoolService = $schoolService;
    }

    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $schools = $this->schoolService->repository->findAll();

        foreach ($schools as $school) {
            $this->schoolService->setPoint($school);
        }

        $io->success('Done!');
        return 0;
    }
}
