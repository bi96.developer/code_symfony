<?php

namespace App\Command;

use App\Service\CommonService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MakeSlugCommand extends Command
{
    protected static $defaultName = 'app:make-slug';

    protected static $defaultDescription = 'Generate slug for entity & save to db with field name "slug"';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var CommonService
     */
    private $commonService;

    private $slugField = 'slug';

    private $nameField = 'name';

    public function __construct(
        string $name = null,
        CommonService $commonService,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($name);
        $this->commonService = $commonService;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('entityName', InputArgument::REQUIRED, 'Entity name. Example: EntityName');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $entityName = $input->getArgument('entityName');

        $repository = $this->entityManager->getRepository('App:' . $entityName);

        $entities = $repository->findAll();

        foreach ($entities as $entity) {
            $setFuncName = 'set' . ucfirst($this->slugField);
            $getFuncName = 'get' . ucfirst($this->nameField);

            $entity->{$setFuncName}(
                $this->commonService->slugify(
                    $entity->{$getFuncName}()
                )
            );
        }

        $this->entityManager->flush();

        $io->success('Done.');

        return 0;
    }
}
