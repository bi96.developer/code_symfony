<?php

namespace App\Command;

use App\Entity\School;
use App\Repository\SchoolRepository;
use App\Service\CommonService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MakeSchoolSlugCommand extends Command
{
    protected static $defaultName = 'app:make-school-slug';
    protected static $defaultDescription = 'Make school slug';
    /**
     * @var SchoolRepository
     */
    private $schoolRepository;
    /**
     * @var CommonService
     */
    private $commonService;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        string $name = null,
        SchoolRepository $schoolRepository,
        CommonService $commonService,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($name);
        $this->schoolRepository = $schoolRepository;
        $this->commonService = $commonService;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $qb = $this->schoolRepository->createQueryBuilder('School');
        $qb
//            ->andWhere(
//                $qb->expr()->isNull('School.slug')
//            )
//            ->orWhere(
//                'School.slug = :slugEmpty'
//            )
//            ->setParameter('slugEmpty', '')
        ;

        $schools = $qb->getQuery()->getResult();

        /** @var School $school */
        foreach ($schools as $school) {
            $school->setSlug(
                $this->commonService->slugify($school->getName())
            );
        }

        $this->entityManager->flush();

        $io->success('Done!');

        return 0;
    }
}
