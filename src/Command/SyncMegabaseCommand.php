<?php

namespace App\Command;

use App\Entity\SyncLog;
use App\Service\SchoolTypeService;
use App\Service\SchoolService;
use App\Repository\SyncLogRepository;
use App\Service\SyncLogService;
use App\Service\RedirectionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;

class SyncMegabaseCommand extends Command
{
    protected static $defaultName = 'app:sync-megabase';
    protected static $defaultDescription = 'Sync MegaBase';

    /**
     * @var SyncLogRepository
     */
    private $syncLogRepository;

    /**
     * @var SchoolTypeService
     */
    private $schoolTypeService;

    /**
     * @var SchoolService
     */
    private $schoolService;

    /**
     * @var SyncLogService
     */
    private $syncLogService;

    /**
     * @var RedirectionService
     */
    private $redirectionService;

    public function __construct(
        string $name = null,
        SyncLogRepository $syncLogRepository,
        SchoolTypeService $schoolTypeService,
        SchoolService $schoolService,
        SyncLogService $syncLogService,
        RedirectionService $redirectionService
    ) {
        parent::__construct($name);
        $this->syncLogRepository = $syncLogRepository;
        $this->schoolTypeService = $schoolTypeService;
        $this->schoolService = $schoolService;
        $this->syncLogService = $syncLogService;
        $this->redirectionService = $redirectionService;
    }

    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('startsync', InputArgument::OPTIONAL, 'startSync');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // Get last sync megaBase
        try {
            $qb = $this->syncLogRepository->createQueryBuilder('s')
                ->select('s.lastSync')
                ->andWhere('s.status = :status')
                ->setParameter('status', SyncLog::STATUS_SUCCESS)
                ->addOrderBy('s.id', 'DESC')
                ->setMaxResults(1);
            $getLastSync = $qb->getQuery()->getOneOrNullResult();

            $lastSync = null;
            if ($getLastSync && $input->getArgument('startsync') != "start") {
                $lastSync = $getLastSync['lastSync'] ? $getLastSync['lastSync']->format('Y-m-d H:i:s') : null;
            }

            $currentSync = (new \DateTime());
            $syncMegaBase = $this->syncMegaBase($lastSync);

            if (!$syncMegaBase) {
                $this->syncLogService->log(SyncLog::ACTION_SYNC_NO_DATA, null, SyncLog::STATUS_SUCCESS, $currentSync);
                $io->success('No Data!');
                $this->syncLogService->checkErrorAfterSync();
                return 0;
            }

            if (isset($syncMegaBase['school_type']) && $syncSchoolTypes = $syncMegaBase['school_type']) {
                $this->schoolTypeService->syncSchoolType($syncSchoolTypes, $currentSync);
            }

            if (isset($syncMegaBase['school']) && $syncSchools = $syncMegaBase['school']) {
                $this->schoolService->syncSchool($syncSchools, $currentSync);
            }

            if (isset($syncMegaBase['archive']) && $syncMegaBase['archive']) {

                $archives = $syncMegaBase['archive'];
                $fieldEntity = $syncMegaBase['archive']['columns'];
                $entityName = array_search('entity_name', array_keys($fieldEntity));
                $entityId = array_search('entity_id', array_keys($fieldEntity));
                $dataError = null;

                try {
                    foreach ($archives['data'] as $archiveEntity) {

                        $dataError = $archiveEntity;

                        if ($archiveEntity[$entityName] != 'school_type' && $archiveEntity[$entityName] != 'school') {
                            continue;
                        }

                        if ($archiveEntity[$entityId]) {

                            if ($archiveEntity[$entityName] == 'school_type') {
                                // Set status to inactive
                                $this->schoolTypeService->updateStatusToInActive($archiveEntity[$entityId]);
                            }

                            if ($archiveEntity[$entityName] == 'school') {
                                // Set status to inactive
                                $this->schoolService->updateStatusToInActive($archiveEntity[$entityId]);
                            }
                        }
                    }

                    $this->syncLogService->log(SyncLog::ACTION_SYNC_DELETE, $archives, SyncLog::STATUS_SUCCESS, $currentSync);
                } catch (\Exception $e) {

                    $this->syncLogService->log(SyncLog::ACTION_SYNC_DELETE, $archives, SyncLog::STATUS_ERROR, $currentSync, $dataError, $e->getMessage());
                }
            }

            if (isset($syncMegaBase['old_link']) && $syncOldLinks = $syncMegaBase['old_link']) {
                $this->redirectionService->saveOldLink($syncOldLinks, $currentSync);
            }
        } catch (\Exception $e) {
            $this->syncLogService->log(SyncLog::ACTION_SYNC_ERROR, null, SyncLog::STATUS_ERROR, $currentSync, null, $e->getMessage());
        }

        $this->syncLogService->checkErrorAfterSync();

        $io->success('Done!');
        return 0;
    }

    protected function syncMegaBase($lastSync)
    {

        $endPoint = $_ENV['SYNC_URL'];

        $headers = [
            'Content-Type: text/plain'
        ];


        $body = [
            "key"      => $_ENV['SYNC_TOKEN'],
            "nonce"    => $_ENV['SYNC_NONCE'],
            "lastSync" => $lastSync
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $endPoint);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));

        $responseText = curl_exec($curl);
        $response = json_decode($responseText, true);

        return $response;
    }
}
