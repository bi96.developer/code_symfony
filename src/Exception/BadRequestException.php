<?php

namespace App\Exception;
 
use Exception;
use Symfony\Component\HttpFoundation\Response;
 
class BadRequestException extends Exception implements ApiExceptionInterface
{
    const EXCEL_PARSE_ERROR = 'excel parse error';
    const DUPLICATE_ENTITY = 'cant create another';
    const NO_ENTITY_TO_DELETE = 'not exists to be deleted';
    const NO_ENTITY_TO_UPDATE = 'not exists to be updated';
    const NO_PERMISSION_OF_ENTITY = 'you have no permission to edit this';
    const YOU_ARE_ALREADY_ADMIN = 'you are admin of this course already';
    const ADMIN_REQUEST_DENIED = 'your request for this course is denied';
    const USER_NOT_REQUEST = 'current user did not request this permission';
    const NO_CRAWL_DATA = 'no new data to crawl';
    
    public function __construct($error, array $data = [])
    {
        if (is_string($error)) {
            $error = ['error' => $error];
            $error = array_merge($error, $data);
        }
        $string = '';
        forEach($error as $key => $e) {
            if($key == 'error') {
                $string .= $e;
            } else {
                $string .= $key . ': ' . $e . '\n';
            }
        }
        parent::__construct($string, Response::HTTP_BAD_REQUEST);
    }
}
