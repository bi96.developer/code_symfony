<?php

namespace App\Exception;
 
use Exception;
use Symfony\Component\HttpFoundation\Response;
 
class AccessDeniedException extends Exception implements ApiExceptionInterface
{
    
    const WRONG_PASSWORD = 'password incorrect';
    const NOT_PERMITTED = 'not permitted';
    const USER_NOT_FOUND = 'username does not exist';
    const NOT_SIGNED_IN = 'user not allowed to login';
    const UNKNOWN = 'unknown';
    
    public function __construct(
        string $error = 'not permitted',
        string $action = self::UNKNOWN
    ) {
        parent::__construct($action . ' ' . $error, Response::HTTP_FORBIDDEN);
    }
}