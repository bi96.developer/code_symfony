<?php

namespace App\Exception;
 
use Exception;
use Symfony\Component\HttpFoundation\Response;
 
class CacheHitResponse extends Exception implements ApiExceptionInterface
{
    public function __construct($data, $code)
    {
        parent::__construct($data, (int) $code);
    }
}
