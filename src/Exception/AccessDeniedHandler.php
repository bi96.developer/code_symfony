<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $attributes = $accessDeniedException->getAttributes();
        $message =  [
                        'error' => 'not permitted',
                        'action' => !empty($attributes) ? $attributes[0] : 'unknown',
                        'code' => Response::HTTP_FORBIDDEN,
                    ];
        return new Response(json_encode($message), Response::HTTP_FORBIDDEN);
    }
}