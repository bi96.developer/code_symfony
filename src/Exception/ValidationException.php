<?php

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolation;
use Throwable;

class ValidationException extends \Exception
{
    /** @var ConstraintViolation[] */
    private $errors;

    /**
     * ValidationException constructor.
     * @param ConstraintViolation[] $errors
     * @param Throwable|null $previous
     */
    public function __construct($errors, Throwable $previous = null)
    {
        $message = 'Bad request';

        if (count($errors)) {
            $message = $errors[0]->getMessage();
        }

        parent::__construct($message, 400, $previous);

        $this->errors = $errors;
    }

    public function getViolations()
    {
        $violationMessages = [];

        foreach ($this->errors as $error) {
            $violationMessages[$error->getPropertyPath()] = $error->getMessage();
        }

        return $violationMessages;
    }
}
