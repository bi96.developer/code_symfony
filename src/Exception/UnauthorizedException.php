<?php

namespace App\Exception;
 
use Exception;
use Symfony\Component\HttpFoundation\Response;
 
class UnauthorizedException extends Exception implements ApiExceptionInterface
{
  
    const USER_NOT_FOUND = 'username does not exist';
    const NOT_SIGNED_IN = 'not signed in';
    const BACK_END_ON_LOCAL_SERVER = 'can not use backend on local server';
    const INVALID_TOKEN = 'invalid token';
    const INVALID_NONCE = 'invalid nonce';
    const UNKNOWN = 'unknown';
    const HASH_MISMATCH = 'hash not match';
    const PASSWORD_DECIPHER_FAILED = 'can not decrypt token';
    const PASSWORD_TAMPERED = 'token tampered';
    
    public function __construct(string $error, string $action = self::UNKNOWN) {
        parent::__construct(
            $error . ' at action: ' . $action, 
            Response::HTTP_UNAUTHORIZED
        );
    }
}