<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ValidateException extends BadRequestHttpException
{
    protected $messages = [];

    public function __construct(array $messages, \Throwable $previous = null, int $code = 0, array $headers = [])
    {
        $this->messages = $messages;

        $message = reset($messages);

        if (!$message) {
            $message = '';
        }

        parent::__construct($message, $previous, $code, $headers);
    }

    public function getMessages()
    {
        return $this->messages;
    }
}
