<?php

namespace App\Entity;

use App\Repository\SchoolPointRepository;
use App\Traits\EntityDateTimeAbleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SchoolPointRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class SchoolPoint
{
    use EntityDateTimeAbleTrait;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=School::class, inversedBy="schoolPoints")
     * @ORM\JoinColumn(nullable=false)
     */
    private $school;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orgId;

    /**
     * @ORM\Column(type="float")
     */
    private $point;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rankNumber;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $diplomeoRating;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $diplomeoReview;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $letudiantRating;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $letudiantReview;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $googleRating;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $googleReview;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $saRating;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $saReview;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $pointData = [];

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getPoint(): ?float
    {
        return $this->point;
    }

    public function setPoint(float $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getRankNumber(): ?int
    {
        return $this->rankNumber;
    }

    public function setRankNumber(?int $rankNumber): self
    {
        $this->rankNumber = $rankNumber;

        return $this;
    }

    public function getDiplomeoRating(): ?float
    {
        return $this->diplomeoRating;
    }

    public function setDiplomeoRating(?float $diplomeoRating): self
    {
        $this->diplomeoRating = $diplomeoRating;

        return $this;
    }

    public function getDiplomeoReview(): ?int
    {
        return $this->diplomeoReview;
    }

    public function setDiplomeoReview(?int $diplomeoReview): self
    {
        $this->diplomeoReview = $diplomeoReview;

        return $this;
    }

    public function getLetudiantRating(): ?float
    {
        return $this->letudiantRating;
    }

    public function setLetudiantRating(?float $letudiantRating): self
    {
        $this->letudiantRating = $letudiantRating;

        return $this;
    }

    public function getLetudiantReview(): ?int
    {
        return $this->letudiantReview;
    }

    public function setLetudiantReview(?int $letudiantReview): self
    {
        $this->letudiantReview = $letudiantReview;

        return $this;
    }

    public function getGoogleRating(): ?float
    {
        return $this->googleRating;
    }

    public function setGoogleRating(?float $googleRating): self
    {
        $this->googleRating = $googleRating;

        return $this;
    }

    public function getGoogleReview(): ?int
    {
        return $this->googleReview;
    }

    public function setGoogleReview(?int $googleReview): self
    {
        $this->googleReview = $googleReview;

        return $this;
    }

    public function getSaRating(): ?float
    {
        return $this->saRating;
    }

    public function setSaRating(?float $saRating): self
    {
        $this->saRating = $saRating;

        return $this;
    }

    public function getSaReview(): ?int
    {
        return $this->saReview;
    }

    public function setSaReview(?int $saReview): self
    {
        $this->saReview = $saReview;

        return $this;
    }

    public function getOrgId(): ?int
    {
        return $this->orgId;
    }

    public function setOrgId(?int $orgId): self
    {
        $this->orgId = $orgId;

        return $this;
    }

    public function getPointData(): ?array
    {
        return $this->pointData;
    }

    public function setPointData(?array $pointData): self
    {
        $this->pointData = $pointData;

        return $this;
    }
}
