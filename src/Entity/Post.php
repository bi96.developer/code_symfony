<?php

namespace App\Entity;

use App\Repository\PostRepository;
use App\Traits\EntityDateTimeAbleTrait;
use App\Traits\EntityReactionAbleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Post
{
    //const TYPE_POST = 1;
    //const TYPE_VLOG = 2;
    //const TYPE_COACH = 3;
    const TYPE_REVIEW = 4;
    //const TYPE_COURSE_MEDIA = 5;
    const TYPE_DELETED = 6;
    const TYPE_COURSE_POST = 7;
    //const TYPE_DISCUSSION = 8;
    const TYPE_DIRECTOR_INTERVIEW = 9;
    const REVIEW_ATTRIBUTES = [
        "overall",
        "fees",
        "deposits",
        "platform",
        "education",
        "service"
    ];
    
    use EntityReactionAbleTrait;
    use EntityDateTimeAbleTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalParentComment;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class)
     */
    private $language;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $reviewData = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTotalParentComment(): ?int
    {
        return $this->totalParentComment;
    }

    public function setTotalParentComment(?int $totalParentComment): self
    {
        $this->totalParentComment = $totalParentComment;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getReviewData(): ?array
    {
        return $this->reviewData;
    }

    public function setReviewData(?array $reviewData): self
    {
        $this->reviewData = $reviewData;

        return $this;
    }
}
