<?php

namespace App\Entity;

use App\Repository\SchoolRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=SchoolRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 */
class School
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const POINT_SOCIAL = 0.0001;
    const POINT_REVIEW = 0.001;
    const RATE_SOCIAL = 0.06;
    const RATE_SA_REVIEW = 0.4;
    const RATE_REVIEW = 0.1;
    const CRAWL_FIELDS = [
        'twitterUrl',
        'facebookUrl',
        'instagramUrl',
        'linkedinUrl',
        'youtubeUrl',
        'diplomeoUrl',
        'letudiantUrl',
        'googlePlaceId'
    ];
    use \App\Traits\EntityDateTimeAbleTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $palmes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $league;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zipcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $banner;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twitterUrl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $twitterFollower;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookUrl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $facebookFollower;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instagramUrl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $instagramFollower;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkedinUrl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $linkedinFollower;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $youtubeUrl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $youtubeFollower;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diplomeoUrl;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $diplomeoRating;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $diplomeoReview;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $letudiantUrl;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $letudiantRating;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $letudiantReview;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $googleRating;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googlePlaceId;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $googlePlaceLat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $googlePlaceLng;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $googleReview;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $saReview;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $saRating;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $point;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $pointData = [];

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $reviewRating;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $reviewData = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalReview;

    /**
     * @ORM\ManyToMany(targetEntity=Country::class, inversedBy="schools")
     */
    private $countries;

    /**
     * @ORM\OneToMany(targetEntity=SchoolUser::class, mappedBy="school")
     */
    private $schoolUsers;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orgId;

    /**
     * @ORM\ManyToOne(targetEntity=SchoolType::class, inversedBy="schools")
     */
    private $schoolType;

    /**
     * @ORM\OneToMany(targetEntity=SchoolPoint::class, mappedBy="school", orphanRemoval=true)
     */
    private $schoolPoints;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\OneToOne(targetEntity=SchoolLink::class, mappedBy="school", cascade={"persist", "remove"})
     */
    private $schoolLink;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $updateData;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crawlStatus;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPublished;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logoUrl;

    public function __construct()
    {
        $this->countries = new ArrayCollection();
        $this->schoolUsers = new ArrayCollection();
        $this->schoolPoints = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPalmes(): ?string
    {
        return $this->palmes;
    }

    public function setPalmes(?string $palmes): self
    {
        $this->palmes = $palmes;

        return $this;
    }

    public function getLeague(): ?string
    {
        return $this->league;
    }

    public function setLeague(?string $league): self
    {
        $this->league = $league;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(?string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getAddress3(): ?string
    {
        return $this->address3;
    }

    public function setAddress3(?string $address3): self
    {
        $this->address3 = $address3;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(?string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getBanner(): ?string
    {
        return $this->banner;
    }

    public function setBanner(?string $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getTwitterUrl(): ?string
    {
        return $this->twitterUrl;
    }

    public function setTwitterUrl(?string $twitterUrl): self
    {
        $this->twitterUrl = $twitterUrl;

        return $this;
    }

    public function getTwitterFollower(): ?int
    {
        return $this->twitterFollower;
    }

    public function setTwitterFollower(?int $twitterFollower): self
    {
        $this->twitterFollower = $twitterFollower;

        return $this;
    }

    public function getFacebookUrl(): ?string
    {
        return $this->facebookUrl;
    }

    public function setFacebookUrl(?string $facebookUrl): self
    {
        $this->facebookUrl = $facebookUrl;

        return $this;
    }

    public function getFacebookFollower(): ?int
    {
        return $this->facebookFollower;
    }

    public function setFacebookFollower(?int $facebookFollower): self
    {
        $this->facebookFollower = $facebookFollower;

        return $this;
    }

    public function getInstagramUrl(): ?string
    {
        return $this->instagramUrl;
    }

    public function setInstagramUrl(?string $instagramUrl): self
    {
        $this->instagramUrl = $instagramUrl;

        return $this;
    }

    public function getInstagramFollower(): ?int
    {
        return $this->instagramFollower;
    }

    public function setInstagramFollower(?int $instagramFollower): self
    {
        $this->instagramFollower = $instagramFollower;

        return $this;
    }

    public function getLinkedinUrl(): ?string
    {
        return $this->linkedinUrl;
    }

    public function setLinkedinUrl(?string $linkedinUrl): self
    {
        $this->linkedinUrl = $linkedinUrl;

        return $this;
    }

    public function getLinkedinFollower(): ?int
    {
        return $this->linkedinFollower;
    }

    public function setLinkedinFollower(?int $linkedinFollower): self
    {
        $this->linkedinFollower = $linkedinFollower;

        return $this;
    }

    public function getYoutubeUrl(): ?string
    {
        return $this->youtubeUrl;
    }

    public function setYoutubeUrl(?string $youtubeUrl): self
    {
        $this->youtubeUrl = $youtubeUrl;

        return $this;
    }

    public function getYoutubeFollower(): ?int
    {
        return $this->youtubeFollower;
    }

    public function setYoutubeFollower(?int $youtubeFollower): self
    {
        $this->youtubeFollower = $youtubeFollower;

        return $this;
    }

    public function getDiplomeoUrl(): ?string
    {
        return $this->diplomeoUrl;
    }

    public function setDiplomeoUrl(?string $diplomeoUrl): self
    {
        $this->diplomeoUrl = $diplomeoUrl;

        return $this;
    }

    public function getDiplomeoRating(): ?float
    {
        return $this->diplomeoRating;
    }

    public function setDiplomeoRating(?float $diplomeoRating): self
    {
        $this->diplomeoRating = $diplomeoRating;

        return $this;
    }

    public function getDiplomeoReview(): ?int
    {
        return $this->diplomeoReview;
    }

    public function setDiplomeoReview(?int $diplomeoReview): self
    {
        $this->diplomeoReview = $diplomeoReview;

        return $this;
    }

    public function getLetudiantUrl(): ?string
    {
        return $this->letudiantUrl;
    }

    public function setLetudiantUrl(?string $letudiantUrl): self
    {
        $this->letudiantUrl = $letudiantUrl;

        return $this;
    }

    public function getLetudiantRating(): ?float
    {
        return $this->letudiantRating;
    }

    public function setLetudiantRating(?float $letudiantRating): self
    {
        $this->letudiantRating = $letudiantRating;

        return $this;
    }

    public function getLetudiantReview(): ?int
    {
        return $this->letudiantReview;
    }

    public function setLetudiantReview(?int $letudiantReview): self
    {
        $this->letudiantReview = $letudiantReview;

        return $this;
    }

    public function getGoogleRating(): ?float
    {
        return $this->googleRating;
    }

    public function setGoogleRating(?float $googleRating): self
    {
        $this->googleRating = $googleRating;

        return $this;
    }

    public function getGooglePlaceId(): ?string
    {
        return $this->googlePlaceId;
    }

    public function setGooglePlaceId(?string $googlePlaceId): self
    {
        $this->googlePlaceId = $googlePlaceId;

        return $this;
    }

    public function getGooglePlaceLat(): ?float
    {
        return $this->googlePlaceLat;
    }

    public function setGooglePlaceLat(?float $googlePlaceLat): self
    {
        $this->googlePlaceLat = $googlePlaceLat;

        return $this;
    }

    public function getGooglePlaceLng(): ?float
    {
        return $this->googlePlaceLng;
    }

    public function setGooglePlaceLng(?float $googlePlaceLng): self
    {
        $this->googlePlaceLng = $googlePlaceLng;

        return $this;
    }

    public function getGoogleReview(): ?int
    {
        return $this->googleReview;
    }

    public function setGoogleReview(?int $googleReview): self
    {
        $this->googleReview = $googleReview;

        return $this;
    }

    public function getSaReview(): ?int
    {
        return $this->saReview;
    }

    public function setSaReview(?int $saReview): self
    {
        $this->saReview = $saReview;

        return $this;
    }

    public function getSaRating(): ?float
    {
        return $this->saRating;
    }

    public function setSaRating(?float $saRating): self
    {
        $this->saRating = $saRating;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getPoint(): ?float
    {
        return $this->point;
    }

    public function setPoint(?float $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getPointData(): ?array
    {
        return $this->pointData;
    }

    public function setPointData(?array $pointData): self
    {
        $this->pointData = $pointData;

        return $this;
    }

    public function getReviewRating(): ?float
    {
        return $this->reviewRating;
    }

    public function setReviewRating(?float $reviewRating): self
    {
        $this->reviewRating = $reviewRating;

        return $this;
    }

    public function getReviewData(): ?array
    {
        return $this->reviewData;
    }

    public function setReviewData(?array $reviewData): self
    {
        $this->reviewData = $reviewData;

        return $this;
    }

    public function getTotalReview(): ?int
    {
        return $this->totalReview;
    }

    public function setTotalReview(int $totalReview): self
    {
        $this->totalReview = $totalReview;

        return $this;
    }

    /**
     * @return Collection|Country[]
     */
    public function getCountries(): Collection
    {
        return $this->countries;
    }

    public function addCountry(Country $country): self
    {
        if (!$this->countries->contains($country)) {
            $this->countries[] = $country;
        }

        return $this;
    }

    public function removeCountry(Country $country): self
    {
        $this->countries->removeElement($country);

        return $this;
    }

    /**
     * @return Collection|SchoolUser[]
     */
    public function getSchoolUsers(): Collection
    {
        return $this->schoolUsers;
    }

    public function addSchoolUser(SchoolUser $schoolUser): self
    {
        if (!$this->schoolUsers->contains($schoolUser)) {
            $this->schoolUsers[] = $schoolUser;
            $schoolUser->setSchool($this);
        }

        return $this;
    }

    public function removeSchoolUser(SchoolUser $schoolUser): self
    {
        if ($this->schoolUsers->removeElement($schoolUser)) {
            // set the owning side to null (unless already changed)
            if ($schoolUser->getSchool() === $this) {
                $schoolUser->setSchool(null);
            }
        }

        return $this;
    }

    public function getOrgId(): ?int
    {
        return $this->orgId;
    }

    public function setOrgId(?int $orgId): self
    {
        $this->orgId = $orgId;

        return $this;
    }

    public function getSchoolType(): ?SchoolType
    {
        return $this->schoolType;
    }

    public function setSchoolType(?SchoolType $schoolType): self
    {
        $this->schoolType = $schoolType;

        return $this;
    }

    /**
     * @return Collection|SchoolPoint[]
     */
    public function getSchoolPoints(): Collection
    {
        return $this->schoolPoints;
    }

    public function addSchoolPoint(SchoolPoint $schoolPoint): self
    {
        if (!$this->schoolPoints->contains($schoolPoint)) {
            $this->schoolPoints[] = $schoolPoint;
            $schoolPoint->setSchool($this);
        }

        return $this;
    }

    public function removeSchoolPoint(SchoolPoint $schoolPoint): self
    {
        if ($this->schoolPoints->removeElement($schoolPoint)) {
            // set the owning side to null (unless already changed)
            if ($schoolPoint->getSchool() === $this) {
                $schoolPoint->setSchool(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSchoolLink(): ?SchoolLink
    {
        return $this->schoolLink;
    }

    public function setSchoolLink(SchoolLink $schoolLink): self
    {
        // set the owning side of the relation if necessary
        if ($schoolLink->getSchool() !== $this) {
            $schoolLink->setSchool($this);
        }

        $this->schoolLink = $schoolLink;

        return $this;
    }

    public function getUpdateData(): ?string
    {
        return $this->updateData;
    }

    public function setUpdateData(?string $updateData): self
    {
        $this->updateData = $updateData;

        return $this;
    }

    public function getCrawlStatus(): ?string
    {
        return $this->crawlStatus;
    }

    public function setCrawlStatus(?string $crawlStatus): self
    {
        $this->crawlStatus = $crawlStatus;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(?bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    public function setLogoUrl(?string $logoUrl): self
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }
}
