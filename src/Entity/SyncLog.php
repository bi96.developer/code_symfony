<?php

namespace App\Entity;

use App\Repository\SyncLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SyncLogRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class SyncLog
{
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 0;

    const ACTION_SYNC_SCHOOL_TYPE = 'school_type';
    const ACTION_SYNC_SCHOOL = 'school';
    const ACTION_SYNC_DELETE = 'delete';
    const ACTION_SYNC_NO_DATA = 'no_data';
    const ACTION_SYNC_ERROR = 'sync_error';
    const ACTION_SYNC_OLD_LINK = 'sync_old_link';


    use \App\Traits\EntityDateTimeAbleTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $action;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $data = [];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastSync;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $message;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $dataError = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(?string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(?array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getLastSync(): ?\DateTimeInterface
    {
        return $this->lastSync;
    }

    public function setLastSync(\DateTimeInterface $lastSync): self
    {
        $this->lastSync = $lastSync;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getDataError(): ?array
    {
        return $this->dataError;
    }

    public function setDataError(?array $dataError): self
    {
        $this->dataError = $dataError;

        return $this;
    }
}