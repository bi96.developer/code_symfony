<?php

namespace App\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Repository\GoogleKeyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GoogleKeyRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *   fields={"apiKey", "type"},
 *   errorPath="apiKey",
 *   message="This api key is already in exist."
 * )
 */
class GoogleKey
{
    use \App\Traits\EntityDateTimeAbleTrait;
    
    const STATUS_PENDING = 1;
    const STATUS_USELESS = 2;
    const STATUS_GOOD = 3;
    const TYPE_MAP = 1;
    const TYPE_SEARCH = 2;
    const TYPE_YOUTUBE = 3;
    const TYPE_TRANSLATE = 4;
    const URL_TEST = [
      '',
      'https://maps.googleapis.com/maps/api/place/textsearch/json?query=Creekside%20Golf%20Course',
      'https://customsearch.googleapis.com/customsearch/v1?cx=a94d5b814dd9b6ae6&q=Diamond%20Bay%20Golf%20Villas66546456',
      'https://www.googleapis.com/youtube/v3/videos?part=statistics&id=-biOGdYiF-I',
      'https://translation.googleapis.com/language/translate/v2?source=zh&target=en&q=%E5%85%B3%E4%BA%8E%E6%88%91%E4%BB%AC'
    ];
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apiKey;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $useMeter;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    public function setApiKey(string $apiKey): self
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUseMeter(): ?int
    {
        return $this->useMeter;
    }

    public function setUseMeter(?int $useMeter): self
    {
        $this->useMeter = $useMeter;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }
}
