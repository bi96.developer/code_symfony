<?php

namespace App\Entity;

use App\Repository\YearRankingRepository;
use App\Traits\EntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=YearRankingRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class YearRanking
{
    use EntityTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $previousYear;

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function getPreviousYear(): ?int
    {
        return $this->previousYear;
    }

    public function setPreviousYear(?int $previousYear): self
    {
        $this->previousYear = $previousYear;

        return $this;
    }
}
