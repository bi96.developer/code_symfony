<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity( repositoryClass = "App\Repository\UserRepository" )
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *   fields = {"email"},
 *   errorPath="email",
 *   message="This email is already in use."
 * )
 */
class User implements UserInterface
{

    use \App\Traits\EntityDateTimeAbleTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max = 255)
     * @Assert\NotBlank
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max = 255)
     * @Assert\NotBlank
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=191, unique=true)
     * @Assert\Length(max = 191)
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max = 255)
     * @Groups({"excludeFrontend", "excludeBackend"})
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $authPermitted;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserGroup")
     */
    private $userGroup;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", cascade={"persist"})
     */
    private $subRoles;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\OneToMany(targetEntity=SchoolUser::class, mappedBy="user")
     */
    private $schoolUsers;

    public function __construct()
    {
        $this->subRoles = new ArrayCollection();
        $this->schoolUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you location any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @see UserInterface
     */
    public function getRoles()
    {
        return ['LAKAD MATATAG!! NORMALIN NORMALIN'];
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAuthPermitted(): ?bool
    {
        return $this->authPermitted;
    }

    public function setAuthPermitted(bool $authPermitted): self
    {
        $this->authPermitted = $authPermitted;

        return $this;
    }

    public function getUserGroup(): ?UserGroup
    {
        return $this->userGroup;
    }

    public function setUserGroup(?UserGroup $userGroup): self
    {
        $this->userGroup = $userGroup;

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getSubRoles(): Collection
    {
        return $this->subRoles;
    }

    public function setSubRoles(array $subRoles): self
    {
        $this->subRoles = $subRoles;

        return $this;
    }

    public function addSubRole(Role $subRole): self
    {
        if (!$this->subRoles->contains($subRole)) {
            $this->subRoles[] = $subRole;
        }

        return $this;
    }

    public function removeSubRole(Role $subRole): self
    {
        if ($this->subRoles->contains($subRole)) {
            $this->subRoles->removeElement($subRole);
        }

        return $this;
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function setPicture($picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getFullName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|SchoolUser[]
     */
    public function getSchoolUsers(): Collection
    {
        return $this->schoolUsers;
    }

    public function addSchoolUser(SchoolUser $schoolUser): self
    {
        if (!$this->schoolUsers->contains($schoolUser)) {
            $this->schoolUsers[] = $schoolUser;
            $schoolUser->setUser($this);
        }

        return $this;
    }

    public function removeSchoolUser(SchoolUser $schoolUser): self
    {
        if ($this->schoolUsers->removeElement($schoolUser)) {
            // set the owning side to null (unless already changed)
            if ($schoolUser->getUser() === $this) {
                $schoolUser->setUser(null);
            }
        }

        return $this;
    }

}
