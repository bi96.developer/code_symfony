<?php

namespace App\Entity;

use App\Repository\RedirectionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RedirectionRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Redirection
{

    use \App\Traits\EntityDateTimeAbleTrait;
    const SCHOOL = 'school';
    const SCHOOL_TYPE = 'school_type';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $targetId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $urlTo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */

    private $urlOld;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTargetId(): ?int
    {
        return $this->targetId;
    }

    public function setTargetId(int $targetId): self
    {
        $this->targetId = $targetId;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUrlTo(): ?string
    {
        return $this->urlTo;
    }

    public function setUrlTo(?string $urlTo): self
    {
        $this->urlTo = $urlTo;

        return $this;
    }

    public function getUrlOld(): ?string
    {
        return $this->urlOld;
    }

    public function setUrlOld(?string $urlOld): self
    {
        $this->urlOld = $urlOld;

        return $this;
    }
}