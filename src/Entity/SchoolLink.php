<?php

namespace App\Entity;

use App\Repository\SchoolLinkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SchoolLinkRepository::class)
 */
class SchoolLink
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity=School::class, inversedBy="schoolLink")
     * @ORM\JoinColumn(nullable=false)
     */
    private $school;

    /**
     * @ORM\Column(type="integer")
     */
    private $orgId;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $meilleursMasters;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $meilleursGrandesEcoles;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $meilleuresLicenses;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $eduniversalRanking;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $bestMasters;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $saMasters;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $saMBA;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $saBachelors;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $saGrandesEcoles;

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getMeilleurMaster(): ?string
    {
        return $this->meilleursMasters;
    }

    public function setMeilleurMaster(?string $meilleursMasters): self
    {
        $this->meilleursMasters = $meilleursMasters;

        return $this;
    }

    public function getMeilleursGrandesEcoles(): ?string
    {
        return $this->meilleursGrandesEcoles;
    }

    public function setMeilleursGrandesEcoles(?string $meilleursGrandesEcoles): self
    {
        $this->meilleursGrandesEcoles = $meilleursGrandesEcoles;

        return $this;
    }

    public function getMeilleuresLicenses(): ?string
    {
        return $this->meilleuresLicenses;
    }

    public function setMeilleuresLicenses(?string $meilleuresLicenses): self
    {
        $this->meilleuresLicenses = $meilleuresLicenses;

        return $this;
    }

    public function getEduniversalRanking(): ?string
    {
        return $this->eduniversalRanking;
    }

    public function setEduniversalRanking(?string $eduniversalRanking): self
    {
        $this->eduniversalRanking = $eduniversalRanking;

        return $this;
    }

    public function getBestMasters(): ?string
    {
        return $this->bestMasters;
    }

    public function setBestMasters(?string $bestMasters): self
    {
        $this->bestMasters = $bestMasters;

        return $this;
    }

    public function getOrgId(): ?int
    {
        return $this->orgId;
    }

    public function setOrgId(int $orgId): self
    {
        $this->orgId = $orgId;

        return $this;
    }

    public function getSaMasters(): ?string
    {
        return $this->saMasters;
    }

    public function setSaMasters(?string $saMasters): self
    {
        $this->saMasters = $saMasters;

        return $this;
    }

    public function getSaMBA(): ?string
    {
        return $this->saMBA;
    }

    public function setSaMBA(?string $saMBA): self
    {
        $this->saMBA = $saMBA;

        return $this;
    }

    public function getSaBachelors(): ?string
    {
        return $this->saBachelors;
    }

    public function setSaBachelors(?string $saBachelors): self
    {
        $this->saBachelors = $saBachelors;

        return $this;
    }

    public function getSaGrandesEcoles(): ?string
    {
        return $this->saGrandesEcoles;
    }

    public function setSaGrandesEcoles(?string $saGrandesEcoles): self
    {
        $this->saGrandesEcoles = $saGrandesEcoles;

        return $this;
    }
}
