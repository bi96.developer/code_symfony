<?php

namespace App\Controller\Api;

use App\Service\SchoolService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/schools")
 */
class SchoolController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/{orgId}", requirements={"id"="\d+"})
     */
    public function getSchool(
        $orgId,
        SchoolService $schoolService
    ) {
       if (!($school =  $schoolService->getPublishedSchoolByOrgId($orgId))) {
           throw $this->createNotFoundException('The school does not exist.');
       }

       return $this->json($school);
    }
}
