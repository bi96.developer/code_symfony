<?php

namespace App\Controller\Api;

use App\DTO\News\NewsOutputAdmin;
use App\Service\NewsService;
use App\Exception\BadRequestException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/news")
 */
class NewsController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("")
     */
    public function getList(
        NewsService $newsService,
        Request $request
    ) {

        $news =  $newsService->getListNews($request);
        return $this->json($news);
    }

    /**
     * @Rest\Get("/{id}", requirements={"id"="\d+"})
     */
    public function detail($id, NewsService $newsService)
    {

        $news =  $newsService->getById($id, NewsOutputAdmin::class);

        if (!$news) {
            throw $this->createNotFoundException(
                sprintf('News #%s does not exist', $id)
            );
        }

        return $this->json($news);
    }

    /**
     * @Rest\Post("")
     */
    public function create(NewsService $newsService, Request $request)
    {
        try {
            $entity = $newsService->addEntity($request);
            $return['id'] = $entity['id'];
            $httpStatus = Response::HTTP_CREATED;
        } catch (BadRequestException $e) {
            $return = [
                'error' => 'name',
                'message' => $e->getMessage()
            ];
            $httpStatus = Response::HTTP_BAD_REQUEST;
        }

        return $this->json($return, $httpStatus);
    }

    /**
     * @Rest\Post("/{id}", requirements={"id"="\d+"})
     */
    public function update($id, NewsService $newsService, Request $request)
    {
        $entity = $newsService->getById($id);

        if (!$entity) {
            throw $this->createNotFoundException(
                sprintf('News #%s does not exist', $id)
            );
        }

        try {
            $newsService->updateEntity($entity, $request);
            $return['success'] = true;
            $httpStatus = Response::HTTP_OK;
        } catch (BadRequestException $e) {
            $return = [
                'error' => 'name',
                'message' => $e->getMessage()
            ];
            $httpStatus = Response::HTTP_BAD_REQUEST;
        }

        return $this->json($return, $httpStatus);
    }

    /**
     * @Rest\Delete("/{id}", requirements={"id"="\d+"})
     */
    public function delete($id, NewsService $newsService)
    {
        $entity = $newsService->getById($id);

        if (!$entity) {
            throw $this->createNotFoundException(
                sprintf('News #%s does not exist', $id)
            );
        }

        $newsService->deleteEntity($entity);

        return $this->json([
            'success' => true
        ]);
    }

    /**
     * @Rest\Post("/upload-image")
     */
    public function uploadImage(NewsService $newsService, Request $request)
    {
        if (!$request) {
            throw $this->createNotFoundException(
                sprintf('Images does not exist')
            );
        }

        $result = $newsService->uploadImages($request);

        return $this->json($result);
    }
}