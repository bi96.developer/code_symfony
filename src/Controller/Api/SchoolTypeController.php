<?php

namespace App\Controller\Api;

use App\Service\SchoolTypeService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/school-types")
 */
class SchoolTypeController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("")
     */
    public function getList(
        SchoolTypeService $schoolTypeService
    ) {
        $results = $schoolTypeService->getAllPublishedSchoolTypes();

        return $this->json($results);
    }
}
