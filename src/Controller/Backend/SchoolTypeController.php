<?php

namespace App\Controller\Backend;

use App\Repository\SchoolTypeRepository;
use App\Service\CommonService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\Log;
use App\Annotation\PermissionAdmin;
use App\Annotation\PermissionPublic;

/**
 * @Route("/admin/school-type")
 */
class SchoolTypeController extends BaseController
{
    /**
     * @Rest\Get("/list-all")
     * @PermissionAdmin
     * @return View
     */
    public function getListAllAdmin(Request $request): View
    {
        return View::create(
            $this->currentService->getListAllAdmin($request),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/fix-slug")
     * @PermissionAdmin
     * @return View
     */
    public function fixSlug(
        SchoolTypeRepository $schoolTypeRepository,
        CommonService $commonService,
        EntityManagerInterface $entityManager
    ): View {
        $schoolTypes = $schoolTypeRepository->findAll();

        foreach ($schoolTypes as $schoolType) {
            $schoolType->setSlug(
                $commonService->slugify($schoolType->getName())
            );
        }

        $entityManager->flush();

        return View::create(
            true,
            Response::HTTP_OK
        );
    }
}
