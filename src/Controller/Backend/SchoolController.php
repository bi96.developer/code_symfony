<?php

namespace App\Controller\Backend;

use App\Entity\School;
use App\Repository\SchoolRepository;
use App\Service\SchoolService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\Log;
use App\Annotation\PermissionAdmin;

/**
 * @Route("/admin/school")
 */
class SchoolController extends BaseController
{
    /**
     * @Rest\Post("")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function addEntity(Request $request): View
    {
        $entity = $this->currentService->addEntity($request);
        return View::create($entity, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Post("/update")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function updateEntity(Request $request): View
    {
        $entity = $this->currentService->updateEntity($request);
        return View::create($entity, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/{id}/update-is-published")
     * @param Request $request
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function updateIsPublished(Request $request, $id): View
    {
        $this->currentService->updateIsPublish(
            $id,
            (bool)$request->get('isPublished')
        );

        return View::create(['success' => true], Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/{id}")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function deleteEntity(int $id): View
    {
        $entity = $this->currentService->get($id);
        $schoolFolder = $this
            ->currentService
            ->getPath(
                '/uploads/school/' . $entity->getOrgId()
            );
        $this->currentService->delete($entity);
        $this->currentService->filesystem->remove($schoolFolder);
        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/parse-json")
     * @PermissionAdmin
     * @return View
     */
    public function parseFromJson(): View
    {
        $this->currentService->parseFromJson();
        return View::create(
            (array)[],
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/fix-sa")
     * @PermissionAdmin
     * @return View
     */
    public function fixSA(): View
    {
        $this->currentService->fixSA();
        return View::create(
            (array)[],
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/fix-linkedin")
     * @PermissionAdmin
     * @return View
     */
    public function fixLinkedin(): View
    {
        $this->currentService->fixLinkedin();
        return View::create(
            (array)[],
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/list")
     * @PermissionAdmin
     * @return View
     */
    public function getListAdmin(Request $request): View
    {
        return View::create(
            $this->currentService->getListAdmin($request),
            Response::HTTP_OK
        );
    }


    /**
     * @Rest\Get("/crawl-crawlable")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function crawlCrawlable()
    {
        $crawlables = $this->currentRepo->findBy(['crawlStatus' => 'crawlable'], null, 10);
        forEach ($crawlables as $crawlable) {
            $this->currentService->crawl($crawlable->getId());
        }
        return View::create(
            [],
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/crawl-fix-diplomeo")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function crawlFixDiplomeo()
    {
        return View::create(
            $this->currentService->crawlFixDiplomeo(),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/{id}", requirements={"id"="\d+"})
     * @PermissionAdmin
     * @return View
     */
    public function getSchoolForEdit($id)
    {
        return View::create(
            $this->currentService->getSchoolForEdit($id),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/{id}/point-formula", requirements={"id"="\d+"})
     * @PermissionAdmin
     * @return View
     */
    public function getPointFormula($id)
    {
        return View::create(
            $this->currentService->getPointFormulaTable($id),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/crawl/{id}")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function crawl($id)
    {
        return View::create(
            $this->currentService->crawl($id),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/crawl-force/{id}")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function crawlForce($id)
    {
        return View::create(
            $this->currentService->crawl($id, true),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/update-linkedin-followers")
     * @PermissionAdmin
     * @return View
     */
    public function updateLinkedinFollowers(
        EntityManagerInterface $entityManager,
        SchoolService $schoolService,
        SchoolRepository $schoolRepository
    ) {
        $data = json_decode(file_get_contents('update_linkedin_followers.json'), true);

        foreach ($data['RECORDS'] as $record) {
            $followerNumber = $record['linkedin_follower'];
            $schoolId = $record['id'];

            /** @var School $school */
            $school = $schoolRepository->findOneBy([
                'orgId' => $schoolId
            ]);

            $school->setLinkedinFollower($followerNumber);

            $entityManager->flush();

            $schoolService->setPoint($school);
        }

        return View::create(
            ['success' => 1],
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/update-social-links")
     *
     * @param EntityManagerInterface $entityManager
     * @param SchoolService $schoolService
     * @param SchoolRepository $schoolRepository
     * @return View
     * @throws \Exception
     */
    public function updateSocialLinks(
        EntityManagerInterface $entityManager,
        SchoolService $schoolService,
        SchoolRepository $schoolRepository
    ) {
        if (!file_exists('update_social_links.json')) {
            throw new \Exception('The file update-social-links.json not found in "/public" path');
        }

        $data = json_decode(file_get_contents('update_social_links.json'), true);

        $mappingSocials = [
            'WebSite'         => 'setWebsite',
            'Facebook'        => 'setFacebookUrl',
            'LinkedIn'        => 'setLinkedinUrl',
            'Twitter'         => 'setTwitterUrl',
            'Instagram'       => 'setInstagramUrl',
            'YouTube'         => 'setYoutubeUrl',
            'Google_Business' => 'setGooglePlaceId',
            'Diplomeo'        => 'setDiplomeoUrl',
            'Letudiant'       => 'setLetudiantUrl',
        ];

        foreach ($data['RECORDS'] as $record) {
            $schoolId = $record['IdEcole'];

            /** @var School $school */
            $school = $schoolRepository->findOneBy([
                'orgId' => $schoolId
            ]);

            if (!$school) {
                throw new \Exception(
                    sprintf('School #%s does not exist', $schoolId)
                );
            }

            foreach ($mappingSocials as $fieldName => $setMethodName) {
                if (!array_key_exists($fieldName, $record)) {
                    throw new \Exception(
                        sprintf('Source data missing field name %s', $fieldName)
                    );
                }

                $fieldValue = $record[$fieldName];

                if (!$fieldValue || strtolower(trim($fieldValue)) === 'x') {
                    $school->{$setMethodName}(null);
                } else {
                    $school->{$setMethodName}($fieldValue);
                }
            }

            $entityManager->flush();

            $this->currentService->crawl($school->getId());
        }

        return View::create(
            ['success' => 1],
            Response::HTTP_OK
        );
    }
}
