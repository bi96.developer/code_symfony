<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\Log;
use App\Annotation\PermissionAdmin;
use App\Annotation\PermissionPublic;

class PostController extends BaseController
{   
    /**
     * @Rest\Delete("/admin/review/{reviewId}")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function deleteReview(int $reviewId): View
    {
        $this->reviewService->deleteReview($reviewId);
        return View::create([], Response::HTTP_OK);
    }
    
    /**
     * @Rest\Get("/admin/post/list")
     * @PermissionAdmin
     * @return View
     */
    public function getListAdmin(Request $request): View
    {
        return View::create(
            $this->postService->getList(
                $request, 
                'App\DTO\Post\PostOutputAdmin', 
                null
            ),
            Response::HTTP_OK
        );
    }
    
}