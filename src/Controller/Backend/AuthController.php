<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\Log;
use App\Annotation\PermissionAdmin;

class AuthController extends BaseController
{   
    
    /**
     * Authenticate to get token
     * @Rest\Post("/admin/auth")
     * @Log
     * @param Request $request
     * @return View
     */
    public function authenticate(Request $request): View
    {
        $user = $this->userService->authenticate(json_decode($request->getContent(), 1));
        return View::create(
            (array) $user,
            Response::HTTP_OK,
            ["X-W-Password-Encrypted" => $user->password]
        );
    }
}