<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

use App\Annotation\Log;
use App\Annotation\PermissionAdmin;

/**
 * @Route("/admin/reports")
 */
class ReportController extends BaseController
{   
    /**
     * @Rest\Get("")
     * @return View
     * @PermissionAdmin
     */
    public function getReportList(Request $request): View
    {
        return View::create(
            $this->reportService->getList($request)
        );
    }
    
    /**
     * @Rest\Put("")
     * @return View
     * @Log
     * @PermissionAdmin
     */
    public function updateReport(Request $request): View
    {
        return View::create(
            $this->reportService->updateReport($request)
        );
    }
}