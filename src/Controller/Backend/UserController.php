<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\Log;
use App\Annotation\PermissionAdmin;

/**
 * @Route("/admin/user")
 */
class UserController extends BaseController
{   
    /**
     * @Rest\Get("/list")
     * @return View
     * @PermissionAdmin
     */
    public function getUserList(Request $request): View
    {
        return View::create(
            $this->userService->getListAdmin($request),
            Response::HTTP_OK
        );
    }
    
    /**
     * @Rest\Put("/set-authorization/{mode}/{userId}")
     * @return View
     * @PermissionAdmin
     */
    public function setAuthorization(int $mode, int $userId): View
    {
        return View::create($this->userService->setAuthorization($mode, $userId));
    }
    
    /**
     * @Rest\Put("/delete-content/{userId}")
     * @return View
     * @PermissionAdmin
     */
    public function deleteContent(int $userId): View
    {
        return View::create($this->userService->deleteContent($userId));
    }
    
    /**
     * @Rest\GET("/dummies-factory")
     * @return View
     * @PermissionAdmin
     */
    public function dummiesFactory(): View
    {
        $this->userService->dummiesFactory();
        return View::create([]);
    }
}