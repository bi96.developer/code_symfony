<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

use App\Annotation\Cache;
use App\Annotation\PermissionPublic;
use App\Annotation\PermissionAdmin;

/**
 * @Route("/admin")
 */
class GeoController extends BaseController
{   
    
    /**
     * @Rest\Get("/geo/countries")
     * @Cache
     * @PermissionPublic
     */
    public function getCountryList(): View
    {
        return View::create(
            $this->countryService->getListAvailable(), 
            Response::HTTP_OK
        );
    }
}