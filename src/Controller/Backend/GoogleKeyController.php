<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

use App\Annotation\PermissionAdmin;

/**
 * @Route("/admin/google-key")
 */
class GoogleKeyController extends BaseController
{   
    /**
     * @Rest\Post("/fetch")
     * @PermissionAdmin
     */
    public function fetchGoogleKey(Request $request): View
    {
        return View::create($this->googleKeyService->fetchGoogleKey($request), Response::HTTP_CREATED);
    }
    
}