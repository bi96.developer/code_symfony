<?php

namespace App\Controller\Frontend;

use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractFrontendController
{
    /**
     * @Route("/qui-sommes-nous.html", name="page.qui_sommes_nous")
     */
    public function quiSommesNous()
    {
        $page = $this->getSaPageBySlug('qui-sommes-nous');
        if (!$page) {
            throw $this->createNotFoundException('');
        }
        $this->addTitle($page['title']);
        $this->setDescription($page['description']);

        return $this->render('frontend/page/qui_sommes_nous.html.twig', [
            'page' => $page
        ]);
    }

    /**
     * @Route("/methodologie.html", name="page.methodologie")
     */
    public function methodologie()
    {
        // @todo: Call to sa api
        $this->addTitle('Méthodologie');
        $this->setDescription('');
        return $this->render('frontend/page/methodologie.html.twig');
    }

    /**
     * @Route("/mentions-legales.html", name="page.mentions_legales")
     */
    public function mentionsLegales()
    {
        $page = $this->getSaPageBySlug('mentions-legales');

        $this->addTitle($page['title']);
        $this->setDescription($page['description']);

        return $this->render('frontend/page/mentions_legales.html.twig', [
            'page' => $page
        ]);
    }

    /**
     * @Route("/conditions-utilisation.html", name="page.conditions_utilisation")
     */
    public function conditionsUtilisation()
    {
        $page = $this->getSaPageBySlug('conditions-dutilisation');

        $this->addTitle($page['title']);
        $this->setDescription($page['description']);

        return $this->render('frontend/page/conditions_utilisation.html.twig', [
            'page' => $page
        ]);
    }

    /**
     * @Route("/politique-confidentialite.html", name="page.politique_confidentialite")
     */
    public function politiqueConfidentialite()
    {
        $page = $this->getSaPageBySlug('politique-de-confidentialite');

        $this->addTitle($page['title']);
        $this->setDescription($page['description']);

        return $this->render('frontend/page/politique_confidentialite.html.twig', [
            'page' => $page
        ]);
    }

    protected function getSaPageBySlug($slug)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => $_ENV['SA_API_URL'] . '/page/slug/' . $slug,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'GET',
            CURLOPT_HTTPHEADER     => array(
                'Content-Type: application/json',
            ),
        ));

        $responseText = curl_exec($curl);

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $responseArray = json_decode($responseText, true);

        if (isset($responseArray['error']) || $httpCode !== 200) {
            throw $this->createNotFoundException('The page does not exist.');
        }

        curl_close($curl);

        return $responseArray;
    }
}