<?php

namespace App\Controller\Frontend;

use App\Entity\News;
use App\Misc\ImageUrlHelper;
use App\Service\NewsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NewsController extends AbstractFrontendController
{

    /**
     * @Route("/les-actualites.html", name="news.index")
     */
    public function index(
        NewsService $newsService,
        UrlGeneratorInterface $urlGenerator,
        ImageUrlHelper $imageUrlHelper
    ) {
        $requestData['limit'] = 10;
        $requestData['page'] = 1;

        // Set Seo
        $metaTitle = 'Les dernières actus des écoles';
        $metaDescription = "Découvrez les actualités des écoles sur le classement des écoles";
        $this->setTitle($metaTitle);
        $this->setDescription($metaDescription);

        $news = $newsService->getListAllNews($requestData);



        // BreadcrumbSnippet
        $breadcrumbSnippet = [
            '@context'        => 'https://schema.org',
            '@type'           => 'BreadcrumbList',
            'itemListElement' => [
                [
                    '@type'    => 'ListItem',
                    'position' => 1,
                    'name'     => 'Classement Des Écoles',
                    'item'     => $urlGenerator->generate(
                        'home.index',
                        [],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ],
                [
                    '@type'    => 'ListItem',
                    'position' => 2,
                    'name'     => 'Les dernières actus des écoles',
                ],
            ]
        ];


        // Article snippet
        $newsArticleRichSnippet = [
            '@context'      => 'https://schema.org',
            '@type'         => 'NewsArticle',
            'headline'      => 'Les dernières actus des écoles',
            'image'         => [
                $imageUrlHelper->getNewsImageUrl()
            ],
            'datePublished' => '',
            'dateModified'  => '',
            'author'        => [
                '@type' => 'Organization',
                'name'  => 'Studiesadvisor.net',
                'url'   => 'https://studiesadvisor.net'
            ],
        ];


        $richSnippets = [];
        $richSnippets[] = $breadcrumbSnippet;
        $richSnippets[] = $newsArticleRichSnippet;

        return $this->render('frontend/news/index.html.twig', [
            'news' => $news['list'],
            'total' => $news['total'],
            'total_pages' => $news['totalPages'],
            'richSnippets' => $richSnippets
        ]);
    }

    /**
     * @Route("/load-more", name="news.loadMore")
     */
    public function loadMore(Request $request, NewsService $newsService)
    {

        $requestData['limit'] = 10;
        $requestData['page'] = $request->get('page');

        $news = $newsService->getListAllNews($requestData);
        $data['total_pages'] = $news['totalPages'];
        $data['list'] = $this->renderView(
            'frontend/news/_load_more.html.twig',
            [
                'news_more' => $news['list'],
            ]
        );

        return $this->json([
            'data' => $data
        ]);
    }

    /**
     * @Route("/{newsSlug}-{newsId}.html", name="news.detail", requirements={"newsSlug": "[a-zA-Z0-9\-]+","newsId": "[0-9\-]+"})
     */
    public function detail(
        $newsSlug,
        NewsService $newsService,
        UrlGeneratorInterface $urlGenerator,
        ImageUrlHelper $imageUrlHelper
    ) {
        $news =  $newsService->getNewsBySlug($newsSlug);

        if (!$news) {
            throw $this->createNotFoundException('');
        }

        // Set SEO
        $metaTitle = $news->metaTitle;
        $metaDescription = $news->metaDescription;

        $this->setTitle($metaTitle);
        $this->setDescription($metaDescription);


        $lastNews = $newsService->getListLastNewsLimitNotId(News::LIMIT_LAST_NEWS, $news->id);


        // Breadcrumb snippet
        $breadcrumbSnippet = [
            '@context'        => 'https://schema.org',
            '@type'           => 'BreadcrumbList',
            'itemListElement' => [
                [
                    '@type'    => 'ListItem',
                    'position' => 1,
                    'name'     => 'Classement Des Écoles',
                    'item'     => $urlGenerator->generate(
                        'home.index',
                        [],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ],
                [
                    '@type'    => 'ListItem',
                    'position' => 2,
                    'name'     => 'Actualités',
                    'item'     => $urlGenerator->generate(
                        'news.index',
                        [],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ],
                [
                    '@type'    => 'ListItem',
                    'position' => 3,
                    'name'     => $news->title
                ]
            ]
        ];

        // NewsArticle richSnippet ------------------------------------
        $newsArticleRichSnippet = [
            '@context'      => 'https://schema.org',
            '@type'         => 'NewsArticle',
            'headline'      => $metaTitle,
            'image'         => [
                $news->thumb ? $news->thumb : $imageUrlHelper->getNewsImageUrl()
            ],
            'datePublished' => $news->createdDate ? $news->createdDate->format('Y-m-d H:i:s') : '',
            'dateModified'  => $news->updatedDate ? $news->updatedDate->format('Y-m-d H:i:s') : '',
            'author'        => [
                '@type' => 'Organization',
                'name'  => 'Studiesadvisor.net',
                'url'   => 'https://studiesadvisor.net'
            ],
        ];


        $richSnippets = [];
        $richSnippets[] = $breadcrumbSnippet;
        $richSnippets[] = $newsArticleRichSnippet;

        return $this->render('frontend/news/detail.html.twig', [
            'news' => $news,
            'lastNews' => $lastNews,
            'richSnippets' => $richSnippets
        ]);
    }
}