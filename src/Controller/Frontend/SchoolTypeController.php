<?php

namespace App\Controller\Frontend;

use App\Entity\News;
use App\Misc\ImageUrlHelper;
use App\Service\NewsService;
use App\Service\QuestionService;
use App\Service\SchoolService;
use App\Service\SchoolTypeService;
use App\Service\YearRankingService;
use App\Service\RedirectionService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SchoolTypeController extends AbstractFrontendController
{
    /**
     * @Route("/classement-{schoolTypeSlug}.html", name="schoolType.detail", requirements={"schoolTypeSlug": "[a-zA-Z0-9\-]+"})
     * @param $schoolTypeSlug
     * @param Request $request
     * @param SchoolTypeService $schoolTypeService
     * @param YearRankingService $yearRankingService
     * @param SchoolService $schoolService
     * @param QuestionService $questionService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detail(
        $schoolTypeSlug,
        Request $request,
        RedirectionService $redirectionService,
        SchoolTypeService $schoolTypeService,
        YearRankingService $yearRankingService,
        SchoolService $schoolService,
        QuestionService $questionService,
        UrlGeneratorInterface $urlGenerator,
        ImageUrlHelper $imageUrlHelper,
        NewsService $newsService
    ) {

        $schoolType = $schoolTypeService->getPublishedSchoolTypeBySlug($schoolTypeSlug);

        if (!$schoolType) {

            $path = substr($request->getPathInfo(), 1);
            if (!$path) {
                throw $this->createNotFoundException();
            }
            if ($redirectSchoolType = $redirectionService->checkRedirectionSchoolType($path)) {

                $schoolType = $schoolTypeService->getPublishedSchoolTypeById($redirectSchoolType);

                if ($schoolType) {
                    return $this->redirectToRoute(
                        'schoolType.detail',
                        array('schoolTypeSlug' => $schoolType->slug)
                    );
                }
            }

            throw $this->createNotFoundException('School Type not found');
        }

        $currentYear = $yearRankingService->getCurrentYearRanking();

        $pagedResult = $schoolService->getListSchoolsByType(
            $schoolType->id,
            $currentYear,
            $request
        );

        // get question of school type
        $questions = $questionService->getListQuestionsOfSchoolType($schoolType->id);

        // SEO
        $title = sprintf(
            'Classement des %s %s - Le classement des écoles %s',
            $schoolType->name,
            $currentYear,
            $currentYear
        );
        $this->setTitle($title);
        $descriptionSchool = sprintf(
            'Découvrez le classement des %s %s, les meilleures %s de France présentent dans ce classement des %s %s',
            $schoolType->name,
            $currentYear,
            $schoolType->name,
            $schoolType->name,
            $currentYear
        );
        $this->setDescription($descriptionSchool);
        $h1Content = sprintf('Classement des %s %s', $schoolType->name, $currentYear);

        // Rich snippets
        $richSnippets = [];

        // BreadcrumbSnippet
        $breadcrumbSnippet = [
            '@context'        => 'https://schema.org',
            '@type'           => 'BreadcrumbList',
            'itemListElement' => [
                [
                    '@type'    => 'ListItem',
                    'position' => 1,
                    'name'     => 'Classement Des Écoles',
                    'item'     => $urlGenerator->generate(
                        'home.index',
                        [],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ],
                [
                    '@type'    => 'ListItem',
                    'position' => 2,
                    'name'     => 'Classement des ' . $schoolType->name,
                ],
            ]
        ];
        $richSnippets[] = $breadcrumbSnippet;


        // Article snippet
        $newsArticleRichSnippet = [
            '@context'      => 'https://schema.org',
            '@type'         => 'NewsArticle',
            'headline'      => 'Classement des ' . $schoolType->name,
            'image'         => [
                $imageUrlHelper->getSchoolTypeImageUrl()
            ],
            'datePublished' => $schoolType->createdDate ? $schoolType->createdDate->format('Y-m-d H:i:s') : '',
            'dateModified'  => $schoolType->updatedDate ? $schoolType->updatedDate->format('Y-m-d H:i:s') : '',
            'author'        => [
                '@type' => 'Organization',
                'name'  => 'Studiesadvisor.net',
                'url'   => 'https://studiesadvisor.net'
            ],
        ];
        $richSnippets[] = $newsArticleRichSnippet;

        // Aggregating rich snippet
        list($averageRating, $totalReview) = $schoolTypeService->calculateAverageRatingAndTotalReview($schoolType->id);

        if ($averageRating && $totalReview) {
            $aggregateRichSnippet  = [
                '@context'        => 'https://schema.org',
                '@type'           => 'Product',
                'name'            => 'Classement des ' . $schoolType->name,
                'description'     => $descriptionSchool,
                'image'           => [
                    $imageUrlHelper->getSchoolTypeImageUrl()
                ],
                'brand'        => [
                    '@type' => 'Brand',
                    'name'  => 'Studiesadvisor.net'
                ],
                'provider'        => [
                    '@type' => 'Organization',
                    'name'  => 'Studiesadvisor.net',
                    'sameAs'  => 'https://studiesadvisor.net'
                ],
                'aggregateRating' => [
                    '@type'       => 'AggregateRating',
                    'bestRating'  => 5,
                    'worstRating' => 1,
                    'ratingValue' => $averageRating,
                    'ratingCount' => $totalReview
                ]
            ];

            $richSnippets[] = $aggregateRichSnippet;
        }

        $lastNews = $newsService->getListLastNewsLimit(News::LIMIT_LAST_NEWS_SCHOOL_TYPE);


        return $this->render('frontend/school_type/detail.html.twig', [
            'schoolType'   => $schoolType,
            'pagedResult'  => $pagedResult,
            'currentYear'  => $currentYear,
            'previousYear' => $yearRankingService->getPreviousYearRankingOf($currentYear),
            'h1Content'    => $h1Content,
            'richSnippets' => $richSnippets,
            'questions'    => $questions,
            'lastNews'     => $lastNews,
        ]);
    }

    /**
     * @Route("/school-types/{id}/load-more", name="schoolType.loadMore")
     */
    public function loadMore(
        Request $request,
        $id,
        SchoolTypeService $schoolTypeService,
        YearRankingService $yearRankingService,
        SchoolService $schoolService
    ) {
        $currentYear = $yearRankingService->getCurrentYearRanking();

        $pagedResult = $schoolService->getListSchoolsByType(
            $id,
            $currentYear,
            $request
        );

        $schools = $pagedResult['list'];

        $htmlItems = [];
        foreach ($schools as $key => $school) {
            $htmlItems[] = $this->renderView(
                'frontend/school_type/_row.html.twig',
                [
                    'key'       => $key,
                    'school'       => $school,
                    'currentYear'  => $currentYear,
                    'previousYear' => $yearRankingService->getPreviousYearRankingOf($currentYear)
                ]
            );
        }

        return $this->json([
            'totalPages'  => $pagedResult['totalPages'],
            'currentPage' => $pagedResult['currentPage'],
            'htmlData'    => implode('', $htmlItems)
        ]);
    }
}