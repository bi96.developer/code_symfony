<?php

namespace App\Controller\Frontend;

use App\DTO\Contact\ContactInput;
use App\Exception\ValidationException;
use App\Messenger\NewContactEmail;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ContactController extends AbstractFrontendController
{
    /**
     * @var ContainerInterface
     */
    private $localContainer;

    public function __construct(ContainerInterface $localContainer)
    {
        $this->localContainer = $localContainer;
    }

    /**
     * @Route("/contact.html", name="contact.index")
     */
    public function index()
    {
        $this->addTitle('Contact');
        $this->setDescription('');

        return $this->render('frontend/contact/index.html.twig');
    }

    /**
     * @Route("/contact/submit", methods={"POST"}, name="contact.submit")
     */
    public function submitContact(
        Request $request,
        AutoMapperInterface $autoMapper,
        ValidatorInterface $validator
    ) {
        if (
            $this->localContainer->getParameter('captcha_enabled') &&
            !$this->isCaptchaValid($request->get('g-recaptcha-response'))
        ) {
            throw new BadRequestHttpException('Veuillez cliquer sur la case reCAPTCHA.');
        }

        // validate input
        $input = new ContactInput();

        $autoMapper->mapToObject($request->request->all(), $input);

        $errors = $validator->validate($input);

        if (count($errors)) {
            throw new ValidationException($errors);
        }

        // Send mail to admin
        $this->dispatchMessage(new NewContactEmail($input));

        return $this->json([
            'success' => 1
        ]);
    }

    private function isCaptchaValid($responseCode)
    {
        if ($responseCode) {
            $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
            $recaptcha_secret = $this->localContainer->getParameter('captcha_secret_key');
            $response = file_get_contents($recaptcha_url . '?secret=' . urlencode($recaptcha_secret) . '&response=' . urlencode($responseCode));
            $recaptcha = json_decode($response, true);

            if (!empty($recaptcha) && $recaptcha["success"]) {
                return true;
            }
        }

        return false;
    }


    /**
     * @Route("/contact/preview", methods={"GET"})
     */
    public function previewContactEmail()
    {
        $input = new ContactInput();
        $input->firstName = 'John';
        $input->lastName = 'Doe';
        $input->function = 'Student';
        $input->email = 'john.doe@mail.com';
        $input->phone = '0xxxxxx';
        $input->country = 'VN';
        $input->message = 'Give me more information';

        return $this->render('email/contact/new_contact.html.twig', ['contact' => $input]);
    }
}