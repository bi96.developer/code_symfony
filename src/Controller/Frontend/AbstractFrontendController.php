<?php

namespace App\Controller\Frontend;

use Sonata\SeoBundle\Seo\SeoPageInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class AbstractFrontendController extends AbstractController
{
    public static function getSubscribedServices()
    {
        return array_merge(
            parent::getSubscribedServices(), [
            'sonata.seo.page' => SeoPageInterface::class,
        ]);
    }

    public function addTitle($title)
    {
        $this->container->get('sonata.seo.page')
            ->addTitle($title);
    }

    public function setTitle($title)
    {
        $this->container->get('sonata.seo.page')
            ->setTitle($title);
    }

    public function setDescription($description)
    {
        $this->container->get('sonata.seo.page')
            ->addMeta('name', 'description', $description);
    }

    public function addMeta($type, $name, $content)
    {
        $this->container->get('sonata.seo.page')
            ->addMeta($type, $name, $content);
    }
}
