<?php

namespace App\Controller\Frontend;

use App\Entity\News;
use App\Misc\ImageUrlHelper;
use App\Service\NewsService;
use App\Service\SchoolService;
use App\Service\YearRankingService;
use App\Service\RedirectionService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;

class SchoolController extends AbstractFrontendController
{
    /**
     * @Route("/classement-{schoolTypeSlug}/{schoolSlug}-classement.html", name="school.detail", requirements={"schoolTypeSlug": "[a-zA-Z0-9\-]+", "schoolSlug": "[a-zA-Z0-9\-]+"})
     */
    public function detail(
        $schoolSlug,
        $schoolTypeSlug,
        Request $request,
        RedirectionService $redirectionService,
        SchoolService $schoolService,
        YearRankingService $yearRankingService,
        UrlGeneratorInterface $urlGenerator,
        ImageUrlHelper $imageUrlHelper,
        NewsService $newsService
    ) {

        $school = $schoolService->getPublishedSchoolBySlug($schoolSlug);

        if (!$school || ($school['schoolType']->slug !== $schoolTypeSlug)) {

            // Check url exit redirection
            $path = substr($request->getPathInfo(), 1);
            if (!$path) {
                throw $this->createNotFoundException('');
            };

            if ($redirectSchool = $redirectionService->checkRedirectionSchool($path)) {

                $school = $schoolService->getPublishedSchoolByOrgId($redirectSchool);

                if ($school) {
                    return $this->redirectToRoute(
                        'school.detail',
                        array(
                            'schoolSlug'     => $school['slug'],
                            'schoolTypeSlug' => $school['schoolType']->slug
                        )
                    );
                }
            }

            throw $this->createNotFoundException('');
        }

        $twitterUrl = strpos($school['twitterUrl'], 'twitter.com') !== false || !$school['twitterUrl'] ?
            $school['twitterUrl'] : 'https://twitter.com/' . $school['twitterUrl'];
        $instagramUrl = strpos($school['instagramUrl'], 'instagram.com') !== false || !$school['instagramUrl'] ?
            $school['instagramUrl'] : 'https://www.instagram.com/' . $school['instagramUrl'];
        $facebookUrl = strpos($school['facebookUrl'], 'facebook.com') !== false || !$school['facebookUrl'] ?
            $school['facebookUrl'] : 'https://www.facebook.com/' . $school['facebookUrl'];
        $linkedinUrl = strpos($school['linkedinUrl'], 'linkedin.com') !== false || !$school['linkedinUrl'] ?
            $school['linkedinUrl'] : 'https://www.linkedin.com/' . $school['linkedinUrl'];
        $youtubeUrl = strpos($school['youtubeUrl'], 'youtube.com') !== false || !$school['youtubeUrl'] ?
            $school['youtubeUrl'] : 'https://www.youtube.com/' . $school['youtubeUrl'];
        $googlePlaceUrl = strpos($school['googlePlaceId'], 'google.com') !== false || !$school['googlePlaceId'] ?
            $school['googlePlaceId'] : 'https://www.google.com/maps/search/?api=1&query=Google&query_place_id=' . $school['googlePlaceId'];
        $diplomeoUrl = strpos($school['diplomeoUrl'], 'diplomeo.com') !== false || !$school['diplomeoUrl'] ?
            $school['diplomeoUrl'] : 'https://diplomeo.com/' . $school['diplomeoUrl'];
        $letudiantUrl = strpos($school['letudiantUrl'], 'letudiant.fr') !== false || !$school['letudiantUrl'] ?
            $school['letudiantUrl'] : 'https://www.letudiant.fr/etudes/annuaire-enseignement-superieur/etablissement/' . $school['letudiantUrl'];

        // SEO
        $title = sprintf(
            '%s Classement %s - Le classement des écoles %s',
            $school['name'],
            $yearRankingService->getCurrentYearRanking(),
            $yearRankingService->getCurrentYearRanking()
        );
        $this->setTitle($title);

        $this->setDescription(
            sprintf(
                '%s classement %s, %s prends la %s place dans le classement des %s. Découvrez le classement de %s',
                $school['name'],
                $yearRankingService->getCurrentYearRanking(),
                $school['name'],
                $school['rankNumber'] == 1 ? $school['rankNumber'] . 'ère' : $school['rankNumber'] . 'ème',
                isset($school['schoolType']) ? $school['schoolType']->name : '',
                $yearRankingService->getCurrentYearRanking(),
                $yearRankingService->getCurrentYearRanking()
            )
        );

        $h1Content = sprintf(
            '%s Classement %s',
            isset($school['schoolType']) ? $school['schoolType']->name : '',
            $yearRankingService->getCurrentYearRanking()
        );


        // Breadcrumb snippet
        $breadcrumbSnippet = [
            '@context'        => 'https://schema.org',
            '@type'           => 'BreadcrumbList',
            'itemListElement' => [
                [
                    '@type'    => 'ListItem',
                    'position' => 1,
                    'name'     => 'Classement Des Écoles',
                    'item'     => $urlGenerator->generate(
                        'home.index',
                        [],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ],
                [
                    '@type'    => 'ListItem',
                    'position' => 2,
                    'name'     => $school['schoolType']->name,
                    'item'     => $urlGenerator->generate(
                        'schoolType.detail',
                        ['schoolTypeSlug' => $school['schoolType']->slug],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ],
                [
                    '@type'    => 'ListItem',
                    'position' => 3,
                    'name'     => $school['name']
                ]
            ]
        ];

        // NewsArticle richSnippet ------------------------------------
        $newsArticleRichSnippet = [
            '@context'      => 'https://schema.org',
            '@type'         => 'NewsArticle',
            'headline'      => $title,
            'image'         => [
                $imageUrlHelper->getSchoolLogoUrl($school['logo'])
            ],
            'datePublished' => $school['createdDate'] ? $school['createdDate']->format('Y-m-d H:i:s') : '',
            'dateModified'  => $school['updatedDate'] ? $school['updatedDate']->format('Y-m-d H:i:s') : '',
            'author'        => [
                '@type' => 'Organization',
                'name'  => 'Studiesadvisor.net',
                'url'   => 'https://studiesadvisor.net'
            ],
        ];

        // LocalBusiness & aggregating review rich snippet
        $educationalOrganizationRichSnippet = [
            '@context'        => 'https://schema.org',
            '@type'           => 'EducationalOrganization',
            'name'            => $school['name'],
            'image'           => $school['logo'],
            'address'         => [
                '@type'           => 'PostalAddress',
                'streetAddress'   => $school['address2'],
                'addressLocality' => $school['city'],
                'postalCode'      => $school['zipcode'],
                'addressCountry'  => 'FR'
            ],
            'aggregateRating' => [
                '@type'       => 'AggregateRating',
                'bestRating'  => 5,
                'worstRating' => 1,
                'ratingValue' => $school['averageRating'],
                'ratingCount' => $school['totalReview']
            ]
        ];

        // Review
        $aReviewRichSnippet = [
            '@context'     => 'https://schema.org',
            '@type'        => 'Review',
            'itemReviewed' => [
                '@type' => 'EducationalOrganization',
                'name'  => $school['name']
            ],
            'reviewRating' => [
                '@type'       => 'AggregateRating',
                'bestRating'  => 5,
                'worstRating' => 1,
                'ratingValue' => $school['averageRating'],
                'ratingCount' => $school['totalReview']
            ],
            'publisher'    => [
                '@type' => 'Organization',
                'name'  => 'Studiesadvisor.net',
            ],
            'author'       => [
                '@type' => 'Organization',
                'name'  => 'Studiesadvisor.net',
            ],
        ];

        $richSnippets = [];
        $richSnippets[] = $breadcrumbSnippet;
        $richSnippets[] = $educationalOrganizationRichSnippet;
        $richSnippets[] = $aReviewRichSnippet;
        $richSnippets[] = $newsArticleRichSnippet;

        $lastNews = $newsService->getListLastNewsLimit(News::LIMIT_LAST_NEWS_SCHOOL);

        return $this->render('frontend/school/detail.html.twig', [
            'school'         => $school,
            'currentYear'    => $yearRankingService->getCurrentYearRanking(),
            'h1Content'      => $h1Content,
            'richSnippets'   => $richSnippets,
            'twitterUrl'     => $twitterUrl,
            'instagramUrl'   => $instagramUrl,
            'facebookUrl'    => $facebookUrl,
            'linkedinUrl'    => $linkedinUrl,
            'youtubeUrl'     => $youtubeUrl,
            'googlePlaceUrl' => $googlePlaceUrl,
            'diplomeoUrl'    => $diplomeoUrl,
            'letudiantUrl'   => $letudiantUrl,
            'lastNews'       => $lastNews
        ]);
    }

    /**
     * @Route("/search", name="search")
     */
    public function search(Request $request, SchoolService $schoolService)
    {
        $schoolType = $schoolService->searchSchool($request);
        return $this->json($schoolType);
    }
}