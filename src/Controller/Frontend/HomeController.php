<?php

namespace App\Controller\Frontend;

use App\Entity\News;
use App\Misc\ImageUrlHelper;
use App\Service\NewsService;
use App\Service\SchoolTypeService;
use App\Service\YearRankingService;
use App\Service\SchoolService;
use App\Service\QuestionService;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractFrontendController
{
    /**
     * @Route("/", name="home.index")
     */
    public function index(
        SchoolTypeService $schoolTypeService,
        YearRankingService $yearRankingService,
        SchoolService $schoolService,
        QuestionService $questionService,
        NewsService $newsService,
        ImageUrlHelper $imageUrlHelper
    ) {
        $schoolTypes = $schoolTypeService->getAllPublishedSchoolTypes();

        $questions = $questionService->getQuestion();

        $schoolTypesAndSchools = [];
        if ($schoolTypes) {
            foreach ($schoolTypes as $schoolType) {
                $school = $schoolService->getPublishedSchoolsBySchoolType($schoolType->id, 5);
                if (!$school) {
                    continue;
                }
                $schoolTypesAndSchools[$schoolType->id]['schoolType'] = $schoolType;
                $schoolTypesAndSchools[$schoolType->id]['school'] = $school;
            }
        }


        $title = 'Studies Advisor - Le classement des écoles ' . $yearRankingService->getCurrentYearRanking();
        $this->setTitle($title);
        $this->setDescription(
            sprintf(
                'Découvrez le classement des écoles %s, les meilleurs écoles de France par type d\'écoles: Ecole de commerce, d\'ingenieurs, design, etc...',
                $yearRankingService->getCurrentYearRanking()
            )
        );
        $h1Content = sprintf('Classement des écoles %s', $yearRankingService->getCurrentYearRanking());

        $richSnippets = [];
        $richSnippets[] = [
            '@context' => 'https://schema.org',
            '@type'    => 'NewsArticle',
            'headline' => $title,
            'image'    => [
                $imageUrlHelper->getLogoUrl()
            ],
            'author'   => [
                '@type' => 'Organization',
                'name'  => 'Studiesadvisor.net',
                'url'   => 'https://studiesadvisor.net'
            ],
        ];
        $richSnippets[] = [
            '@context' => 'https://schema.org',
            "@type"    => 'Organization',
            'name'     => 'Studiesadvisor.net',
            'url'      => 'https://studiesadvisor.net/',
            'logo'     => $imageUrlHelper->getLogoUrl(),
            'sameAs'   => [
                'https://www.facebook.com/studiesadvisor',
                'https://twitter.com/Studies_Advisor',
                'https://instagram.com/studiesadvisor_official/?hl=fr'
            ]
        ];

        $lastNews = $newsService->getListLastNewsLimit(News::LIMIT_LAST_NEWS_HOME);

        return $this->render('frontend/home/index.html.twig', [
            'schoolTypesAndSchools' => $schoolTypesAndSchools,
            'questions'             => $questions,
            'lastNews'             => $lastNews,
            'h1Content'             => $h1Content,
            'richSnippets'          => $richSnippets,
        ]);
    }
}