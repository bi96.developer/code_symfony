<?php


namespace App\Controller\Frontend;


use App\Entity\School;
use App\Entity\SchoolType;
use App\Repository\SchoolRepository;
use App\Repository\SchoolTypeRepository;
use App\Service\SchoolService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SitemapController extends AbstractFrontendController
{
    /**
     * @Route("sitemap.xml")
     */
    public function mainSitemap(UrlGeneratorInterface $urlGenerator)
    {
        $sitemapParts = [];

        $sitemapParts[] = $urlGenerator->generate('sitemap.school', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $sitemapParts[] = $urlGenerator->generate('sitemap.schoolType', [], UrlGeneratorInterface::ABSOLUTE_URL);

        $response = new Response();
        $response->headers->set('Content-type', 'text/xml');

        return $this->render('frontend/sitemap/main.html.twig', ['sitemapParts' => $sitemapParts], $response);
    }

    /**
     * @Route("school-sitemap.xml", name="sitemap.school")
     */
    public function schoolSitemap(
        SchoolRepository $schoolRepository,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator
    ) {
        $urlsets = [];

        $qb = $schoolRepository->createQueryBuilder('t');

        $qb->join('t.schoolType', 'SchoolType')
            ->addSelect('SchoolType')
            ->andWhere('t.status = :schoolActiveStatus')
            ->andWhere('t.isPublished = true')
            ->andWhere('SchoolType.status = :schoolTypeActiveStatus')
            ->andWhere('SchoolType.isPublished = true')
            ->setParameter('schoolActiveStatus', School::STATUS_ACTIVE)
            ->setParameter('schoolTypeActiveStatus', SchoolType::STATUS_ACTIVE);

        $iterableResult = $qb->getQuery()->iterate();

        foreach ($iterableResult as $row) {
            /** @var School $school */
            $school = $row[0];

            $urlsets[] = [
                'loc'     => $urlGenerator->generate(
                    'school.detail',
                    ['schoolTypeSlug' => $school->getSchoolType()->getSlug(), 'schoolSlug' => $school->getSlug()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'lastmod' => $school->getUpdatedDate() ? $school->getUpdatedDate()->format('Y-m-d') : null
            ];


            $entityManager->detach($row[0]);
        }

        $response = new Response();
        $response->headers->set('Content-type', 'text/xml');

        return $this->render('frontend/sitemap/child.twig', ['urlsets' => $urlsets], $response);
    }

    /**
     * @Route("school-type-sitemap.xml", name="sitemap.schoolType")
     */
    public function schoolTypeSitemap(
        SchoolTypeRepository $schoolTypeRepository,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator
    ) {
        $response = new Response();
        $response->headers->set('Content-type', 'text/xml');

        $urlsets = [];
        $qb = $schoolTypeRepository->createQueryBuilder('t')
            ->andWhere('t.status = :activeStatus AND t.isPublished = true')
            ->setParameter('activeStatus', SchoolType::STATUS_ACTIVE);

        $iterableResult = $qb->getQuery()->iterate();

        foreach ($iterableResult as $row) {

            /** @var SchoolType $schoolType */
            $schoolType = $row[0];

            $urlsets[] = [
                'loc'     => $urlGenerator->generate(
                    'schoolType.detail',
                    ['schoolTypeSlug' => $schoolType->getSlug()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'lastmod' => $schoolType->getUpdatedDate() ? $schoolType->getUpdatedDate()->format('Y-m-d') : null
            ];

            $entityManager->detach($row[0]);
        }

        $response = new Response();
        $response->headers->set('Content-type', 'text/xml');

        return $this->render('frontend/sitemap/child.twig', ['urlsets' => $urlsets], $response);
    }
}
