<?php

namespace App\Security;

use App\Service\ProfileService;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

use Psr\Cache\CacheItemPoolInterface;

use App\Service\UserService;
use App\Exception\UnauthorizedException;
use App\Exception\AccessDeniedException;

class WAuthProvider implements AuthenticationProviderInterface
{
    public function __construct(
        UserProviderInterface $userProvider,
        CacheItemPoolInterface $cachePool,
        UserService $userService
    ) {
        $this->userProvider = $userProvider;
        $this->cachePool = $cachePool;
        $this->userService = $userService;
    }

    public function authenticate(TokenInterface $token)
    {
        $user = $this->userService->repository->findOneBy(['email' => $token->getUsername()]);
        if(!$user) throw new UnauthorizedException(UnauthorizedException::USER_NOT_FOUND, 'authenticate');
        
        if(!$user->getAuthPermitted()) { 
            throw new AccessDeniedException(AccessDeniedException::NOT_PERMITTED, 'authenticate');
        }

        if ($this->validateDigest($token->digest, $token->nonce, $token->created, $user->getPassword())) {
          
            $permissions = $this->userService->getPermissions($user);

            $authenticatedToken = new WAuthUserToken($permissions, $user);
            $authenticatedToken->setUser($user);

            return $authenticatedToken;
        }
        throw new UnauthorizedException(UnauthorizedException::INVALID_TOKEN, 'authenticate');
    }

    protected function validateDigest($digest, $nonce, $created, $secret)
    {
        // Try to fetch the cache item from pool
        $cacheItem = $this->cachePool->getItem(md5($nonce));

        // Validate that the nonce is *not* in cache
        // if it is, this could be a replay attack
        if (false && $cacheItem->isHit()) {
            throw new UnauthorizedException(UnauthorizedException::INVALID_NONCE, 'authenticate');
        }

        // Store the item in cache for 5 minutes
        $cacheItem->set(null)->expiresAfter(300);
        $this->cachePool->save($cacheItem);

        $expected = hash('sha256', $nonce.$created.hash('sha256', $secret));
        // Validate Secret

        return hash_equals($expected, $digest);
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof WAuthUserToken;
    }
}
