<?php

namespace App\Security;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use App\Entity\User;

class WAuthUserToken extends AbstractToken
{
    private $permissions = array();

    private $info = array();


    public function __construct(array $permissions = array(), $user = null)
    {
        parent::__construct(['LAKAD MATATAG!! NORMALIN NORMALIN']);

        $this->permissions = $permissions;
        $this->setAuthenticated(true);

    }

    public function getCredentials()
    {
        return '';
    }

    public function getPermissions()
    {
        return $this->permissions;
    }

}
