<?php

namespace App\Security;

use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;

class WAuthFactory implements SecurityFactoryInterface
{
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        
        $providerId = 'security.authentication.provider.wAuth.'.$id;
        $container
            ->setDefinition($providerId, new ChildDefinition(WAuthProvider::class))
            ->setArgument(0, new Reference($userProvider))
        ;
        $listenerId = 'security.authentication.listener.wAuth.'.$id;
        $container->setDefinition($listenerId, new ChildDefinition(WAuthListener::class));
        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    public function getPosition()
    {
        return 'pre_auth';
    }

    public function getKey()
    {
        return 'wAuth';
    }

    public function addConfiguration(NodeDefinition $node)
    {
    }
}