<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

use App\Exception\UnauthorizedException;


class WAuthListener implements ListenerInterface
{
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        $authRegex = '/UsernameToken Email="(?P<email>[^"]+)", PasswordDigest="(?P<digest>[^"]+)", Nonce="(?P<nonce>[a-zA-Z0-9+\/]+={0,2})", Created="(?P<created>[^"]+)"/';
        if (!$request->headers->has('X-W-Auth') || 1 !== preg_match($authRegex, $request->headers->get('X-W-Auth'), $matches)) {
            //throw new UnauthorizedException(UnauthorizedException::INVALID_TOKEN, 'no auth header');
            return;
        }
        
        if($matches['email'] == 'undefined') {
            return;
        }

        $token = new WAuthUserToken();
        $token->setUser($matches['email']);

        $token->digest  = $matches['digest'];
        $token->nonce   = $matches['nonce'];
        $token->created = $matches['created'];
        try {
            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);
            return;
        } catch (AuthenticationException $failed) {
            $token = $this->tokenStorage->getToken();
            if ($token instanceof WAuthUserToken
                && $this->providerKey === $token->getProviderKey()
            ) {
                $this->tokenStorage->setToken(null);
            }
            throw new UnauthorizedException(UnauthorizedException::INVALID_TOKEN, 'token check');
            return;
        }

        // By default deny authorization
        $response = new Response();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);
        $event->setResponse($response);
    }
}
