<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Exception\AccessDeniedException;
use App\Exception\BadRequestException;
use App\Repository\PostRepository;

class PostService extends BaseService
{
    /**
     * PhotoService constructor.
     * @param PostRepository $repository
     * @param BaseService $baseService
     */
    public function __construct(
        PostRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
        $this->baseService = $baseService;
    }
    
    public function getList($request, $DTO = null, $extraFilter = null) {
        if(empty($request->query->get('except_type'))) {
            $request->query->set('except_type', $this->TYPE_DELETED);
        }
        $result = $this->repository->getList($request, $DTO, $extraFilter);
        
        return $result;
    }
    
    public function newPost(Request $request) {      
        return (array) $this->add($request, ['user' => $this->getUser()->getId()]);
    }
    
    public function updatePost(Request $request) {
        $id = $request->request->get('id');
        $post = $this->get($id);
        $this->canUpdatePost($post);
       
        return (array) $this->update($post,$request);
    }

    public function deletePost($postId, $checkPermission = true) {
        $post = $this->get($postId);
        if($checkPermission) {
            $this->canDeletePost($post);
        }
        $postType = $post->getType();
        $post->setType($this->TYPE_DELETED);
        $this->repository->save($post);
        return True;
    }
    
    public function canDeletePost($post, $strict = true) {
        if($this->requestService->isBackend) {
            return true;
        }
        return $this->isUserModifyingPost($post, $strict);
    }
    
    public function canUpdatePost($post, $strict = true) {
        return $this->isUserModifyingPost($post, $strict);
    }
  
    public function isUserModifyingPost($post, $strict = true) {
        if($post->getUser()->getId() == $this->getUser()->getId()) {
            return true;
        }
        if($strict) {
            throw new BadRequestException(
                BadRequestException::NO_PERMISSION_OF_ENTITY . ' post'
            );
        } 
        return false;
    }
}
