<?php
namespace App\Service;

use App\Repository\SchoolPointRepository;

class SchoolPointService extends BaseService
{
    public function __construct(
        SchoolPointRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
}
