<?php
namespace App\Service;

use App\Repository\CityRepository;

/**
 * Class CityService
 * @package App\Service
 */
class CityService extends BaseService
{
    /**
     * CityService constructor.
     * @param CityRepository $repository
     */
    public function __construct(
        CityRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
