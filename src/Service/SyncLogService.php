<?php

namespace App\Service;

use App\Entity\SyncLog;
use App\Repository\SyncLogRepository;
use App\Service\MailService;

/**
 * Class SyncLogService
 * @package App\Service
 */
class SyncLogService extends BaseService
{

    /**
     * @var MailService
     */
    private $mailService;


    public function __construct(
        SyncLogRepository $repository,
        BaseService $baseService,
        MailService $mailService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
        $this->mailService = $mailService;
    }

    /**
     * @param string $action
     * @param array $data
     * @param int $status
     * @param array $dataError
     * @param string|null $message
     */

    public function log($action, $data, $status, $currentSync, $dataError = null, $message = null)
    {

        $syncLog = new SyncLog();
        $syncLog->setAction($action);
        $syncLog->setData($data);
        $syncLog->setDataError($dataError);
        $syncLog->setLastSync($currentSync);
        $syncLog->setMessage($message);
        $syncLog->setStatus($status);

        $this->repository->save($syncLog);
    }

    public function checkErrorAfterSync()
    {
        $qb = $this->repository->createQueryBuilder('s');
        $qb->select('s.message');
        $qb->andWhere('s.status = :status');
        $qb->addOrderBy('s.id', 'DESC');
        $qb->setParameter('status', SyncLog::STATUS_ERROR);
        $syncLogs = $qb->getQuery()->getResult();

        if ($syncLogs) {

            $body = [];
            $body['blocks'][0] = [
                "type" => "header",
                "text" => [
                    "type" => "plain_text",
                    "text" => "Sync Error Notification :loudspeaker:"
                ]
            ];

            foreach ($syncLogs as $key => $syncLog) {
                $message = $syncLog['message'] ? $syncLog['message'] : "Message null";
                $body['blocks'][$key + 1] = [
                    "type"     => "section",
                    "text" => [
                        "type" => "plain_text",
                        "text" => '+ ' . $message,
                        "emoji" => true
                    ]
                ];
            }

            $this->sendNotificationSlack($body);
        }
    }

    public function sendNotificationSlack($body)
    {
        if (!isset($_ENV['SLACK_WEBHOOK_URL'])) return false;

        $endPoint = $_ENV['SLACK_WEBHOOK_URL'];
        $headers = [
            'Content-Type: application/json'
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $endPoint);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));

        curl_exec($curl);
        curl_close($curl);
    }
}