<?php
namespace App\Service;

use App\Repository\CountryRepository;

/**
 * Class CountryService
 * @package App\Service
 */
class CountryService extends BaseService
{
    /**
     * CountryService constructor.
     * @param CountryRepository $repository
     */
    public function __construct(
        CountryRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
}
