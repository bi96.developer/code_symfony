<?php
namespace App\Service;

use App\Repository\GoogleKeyRepository;

/**
 * Class GoogleKeyService
 * @package App\Service
 */
class GoogleKeyService extends BaseService
{
    /**
     * GoogleKeyService constructor.
     * @param GoogleKeyRepository $repository
     */
    public function __construct(
        GoogleKeyRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
    
    public function getGoogleKey($type) {
        $googleKey = $this->repository->findOneBy([
                          'status' => $this->STATUS_GOOD,
                          'type' => $type
                      ],[
                          'useMeter' => 'ASC'
                      ]);
        if(is_null($googleKey)) return null;
        $userMeter = $googleKey->getUseMeter() 
                      ? $googleKey->getUseMeter() : 0;
        $googleKey->setUseMeter($userMeter + 1);
        $this->repository->save($googleKey);
        return $googleKey;
    }
    
    public function fetchGoogleKey($request) {
        $parameters = json_decode($request->getContent(), true);
        if(isset($parameters['oldKey'])) {
            $oldGoogleKey = $this->repository->findOneBy([
                              'apiKey' => $parameters['oldKey'],
                              'type' => $parameters['type']
                            ]);
            $oldGoogleKey->setStatus($this->STATUS_USELESS);
            $this->repository->save($oldGoogleKey);
        }
        $googleKey = $this->getGoogleKey($parameters['type']);
        return ['googleKey' => $googleKey->getApiKey()];
    }
}
