<?php
namespace App\Service;

use App\Repository\SchoolLinkRepository;
use Symfony\Component\HttpFoundation\Request;

class SchoolLinkService extends BaseService
{
    public function __construct(
        SchoolLinkRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
}
