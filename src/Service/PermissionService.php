<?php
namespace App\Service;

use App\Repository\PermissionRepository;

/**
 * Class PermissionService
 * @package App\Service
 */
class PermissionService extends BaseService
{
    /**
     * PermissionService constructor.
     * @param PermissionRepository $repository
     */
    public function __construct(
        PermissionRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
    
}
