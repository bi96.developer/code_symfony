<?php

namespace App\Service;

use App\Repository\ReportRepository;

class ReportService extends BaseService
{
    /**
     * ReportService constructor.
     * @param ReportRepository $repository
     */
    public function __construct(
        ReportRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
    
    public function newReport($request) {
        $inputs = [
            'status' => 'pending',
            'user' => $this->getUser()->getId()
        ];
        $this->reportService->add(
            $request, $inputs, null, 'Entity', false, 
            ['user', 'status', 'entityName', 'entityId']
        );
    }
    
    public function getList($request) {
        $result = $this->repository->getList($request, 'App\DTO\Report\ReportOutput');
        $this->fetchEntity($result['list']);
        return $result;
    }
    
    public function updateReport($request) {
        $requestData = json_decode($request->getContent(), true);
        $report = $this->get($requestData['id']);
        $currentStatus = $report->getStatus();
        $report->setStatus($requestData['status']);
        $this->reportRepo->save($report);
        if($requestData['status'] == 'resolved'
          && $currentStatus == 'pending'
        ) {
            if($report->getEntityName() == 'user') {
                $user = $this->userService->get($report->getEntityId());
                $user->setAuthPermitted(false);
                $this->userRepo->save($user);
            }
            if($report->getEntityName() == 'post') {
                $post = $this->postService->get($report->getEntityId());
                if($post->getType() == $this->POST_TYPE_REVIEW) {
                    $this->reviewService->deleteReview($report->getEntityId());
                } else {
                    $this->postService->deletePost($report->getEntityId(), false);
                }
            }
            if($report->getEntityName() == 'comment') {
                $this->commentService->deleteComment($report->getEntityId(), false);
            }
        }
        return ['result' => 'success'];
    }
    
    public function fetchEntity(&$list) {
        $entityIdList = [];
        $entityMap = [];
        forEach($list as $object) {
            if(!isset($entityIdList[$object['entityName']])) {
                $entityIdList[$object['entityName']] = [];
            }
            $entityIdList[$object['entityName']][] = $object['entityId'];
        }
        forEach($entityIdList as $entityName => $idList) {
            $entityList = $this->{$entityName . 'Repo'}->findBy(['id' => $idList]);
            forEach($entityList as $entity) {
                $entityMapKey = $entityName . '-' . $entity->getId();
                $entityMap[$entityMapKey] = $this->autoMapper->map($entity, 'App\DTO\Report\ReportEntityOutput');
            }
        }
        forEach($list as &$object) {
            $entityMapKey = $object['entityName'] . '-' . $object['entityId'];
            if(isset($entityMap[$entityMapKey])) {
                $object['entityDetail'] = $entityMap[$entityMapKey];
            } else {
                $object['entityDetail'] = null;
            }
        }
        return $list;
    }
}
