<?php
namespace App\Service;

use App\Repository\SchoolCrawlRepository;
use Symfony\Component\HttpFoundation\Request;

class SchoolCrawlService extends BaseService
{
    public function __construct(
        SchoolCrawlRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
}
