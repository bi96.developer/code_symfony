<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class ReviewService
 * @package App\Service
 */
class ReviewService extends BaseService
{
    /**
     * ReviewService constructor.
     * @param CourseRepository $repository
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }

    public function newReview($request) {
        $post = $this->postService->newPost($request);
        $school = $this->schoolService->get($post['school']['id']);
        $newTotalReview = 1 + $school->getTotalReview();
        $newOverAllRating = ($school->getReviewRating() * $school->getTotalReview() + $post['reviewData']['overall']) / $newTotalReview;
        $reviewData = [];
        
        forEach($this->POST_REVIEW_ATTRIBUTES as $attr) {
            $oldRating = $school->getReviewData() ? $school->getReviewData()[$attr] : 0;
            $newRating = ($oldRating * $school->getTotalReview() 
                            + $post['reviewData'][$attr]
                          ) / $newTotalReview;
            $reviewData[$attr] = $newRating;
        }
        
        $reviewData['overall'] = $newOverAllRating;
        
        $school->setTotalReview($newTotalReview);
        $school->setReviewRating(round($newOverAllRating, 2));
        $school->setReviewData($reviewData);
        //$newPoint = $this->coursePointService->calculatePoint($course);
        //$course->setPoint($newPoint);
        $this->schoolRepo->save($school);
        return $post;
    }
    
    public function updateReview($id, $request) {
        $post = $this->postService->get($id);
        $school = $post->getSchool();
        $oldReviewData = $post->getReviewData();
        $request->request->set('id', $post->getId());
        $post = $this->postService->updatePost($request);
        $totalReview = $school->getTotalReview();
        $reviewData = [];
        
        forEach($this->POST_REVIEW_ATTRIBUTES as $attributeName) {
            $value = @$school->getReviewData()[$attributeName] ?? 0;
            $coreTotal = $value * $totalReview - $oldReviewData[$attributeName];
            $newValue = ($coreTotal + $post['reviewData'][$attributeName]) / $totalReview;
            $reviewData[$attributeName] = $newValue;
        }
        $school->setReviewRating(round($reviewData["overall"], 2));
        $school->setReviewData($reviewData);
        //$newPoint = $this->coursePointService->calculatePoint($course);
        //$course->setPoint($newPoint);
        $this->schoolRepo->save($school);
        return $post;
    }
    
    public function getReviewInfo($request) {
        $requestData = $request->query->all();
        $school = $this->schoolService->get($requestData['filter_school']);
        $languageFilter = [
            'distinct_relation' => 'language',
            'filter_school' => $requestData['filter_school'],
            'filter_type' => $this->POST_TYPE_REVIEW
        ];
        $languages = $this->postRepo->getDistinctAttributeOfList($languageFilter, 'App\DTO\Post\PostListLanguageOutput');
        return [
            'overallRating' => round($school->getReviewRating(), 1),
            'totalReview' => $school->getTotalReview(),
            'reviewData' => $school->getReviewData(),
            'languages' => $languages
        ];
    }
    
    public function deleteReview($reviewId) {
        $review = $this->postService->get($reviewId);
        $this->postService->deletePost($reviewId);
        $this->recalculateReview($review->getSchool());
    }
    
    public function recalculateReview($school) {
        $reviews = $this->postRepo->findBy([
            'school' => $school->getId(),
            'type' => $this->POST_TYPE_REVIEW
        ]);
        $reviewData = [];
        forEach($reviews as $index => $review) {
            $data = $review->getReviewData();
            forEach($this->POST_REVIEW_ATTRIBUTES as $attr) {
                if(!isset($reviewData[$attr])) {
                    $reviewData[$attr] = 0;
                }
                $reviewData[$attr] = (($reviewData[$attr] * $index) + $data[$attr]) / ($index + 1);
            }
        }
        $school->setTotalReview(count($reviews));
        $school->setReviewRating(round($reviewData['overall'], 2));
        $school->setReviewData($reviewData);
        //$newPoint = $this->coursePointService->calculatePoint($course);
        //$course->setPoint($newPoint);
        $this->schoolRepo->save($school);
    }
}
