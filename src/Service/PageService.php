<?php
namespace App\Service;

use App\Repository\PageRepository;

class PageService extends BaseService
{
    public function __construct(
        PageRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

    public function getPageBySlug($slug)
    {
        return $this->repository->getPageBySlug($slug, 'App\DTO\Page\PageOutput');
    }
}
