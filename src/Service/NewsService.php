<?php

namespace App\Service;

use App\DTO\News\NewsOutput;
use App\DTO\News\NewsListOutput;
use App\DTO\News\NewsOutputAdmin;
use App\DTO\News\NewsListOutputAdmin;

use App\Repository\NewsRepository;
use App\Exception\BadRequestException;
use App\Helper\ImageUploadHelper;
use App\Constant\Common;


class NewsService extends BaseService
{

    public function __construct(
        NewsRepository $repository,
        BaseService $baseService,
        ImageUploadHelper $imageUploadHelper
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
        $this->imageUploadHelper = $imageUploadHelper;
    }

    public function getListLastNewsLimit($limit)
    {
        return $this->repository->getListLastNewsLimit(NewsListOutput::class, $limit);
    }

    public function getListLastNewsLimitNotId($limit, $id)
    {
        return $this->repository->getListLastNewsLimitNotId(NewsListOutput::class, $limit, $id);
    }


    public function getListAllNews($requestData)
    {
        return $this->repository->getListAllNews($requestData, NewsListOutput::class);
    }

    public function getNewsBySlug($slug)
    {
        return $news = $this->repository->getNewsBySlug($slug, NewsOutput::class);
    }

    public function getListNews($request)
    {
        return $this->repository->getListNews($request, NewsListOutputAdmin::class);
    }

    public function getById($id, $dtoClass = null)
    {
        return $this->repository->getById($id, $dtoClass);
    }


    public function getThumbNews($news)
    {
        if (!$news->getThumb()) {
            return null;
        }
        return $this->urlHelper->getAbsoluteUrl(
            sprintf(
                '/uploads/news/%s',
                $news->getThumb()
            )
        );
    }

    public function getImages($name)
    {
        if (!$name) {
            return null;
        }
        return $this->urlHelper->getAbsoluteUrl(
            sprintf(
                '/uploads/images/%s',
                $name
            )
        );
    }


    public function addEntity($request)
    {

        $newsInput = [];

        // Check Name Slug
        if ($request->get('title')) {
            $newsInput['slug'] = $this->commonService->slugify($request->get('title'));

            $newsResult = $this->repository->findBy(['slug' => $newsInput['slug']]);
            if ($newsResult) {
                throw new BadRequestException(Common::ERROR_UNIQUE_SLUG);
            }
        }


        $entity = $this->add($request, $newsInput, null, 'ENTITY', false, [], true);

        // Upload thumb
        if ($request->files->get('thumb_file')) {


            $imageName = $this->imageUploadHelper->save(
                $request->files->get('thumb_file'),
                'uploads_news_dir',
                'thumb.news_width',
                'thumb.news_height'
            );

            $entity->setThumb($imageName);
        }

        $this->repository->save($entity);

        $entity = (array)$this->autoMapper->map($entity, NewsOutputAdmin::class);
        return $entity;
    }

    public function updateEntity($entity, $request)
    {

        $newsInput = [];

        // Check Name Slug
        if ($request->get('title')) {
            $newsInput['slug'] = $this->commonService->slugify($request->get('title'));

            $newsResult = $this->repository->getEntityOther($entity, 'slug', $newsInput['slug']);
            if ($newsResult) {
                throw new BadRequestException(Common::ERROR_UNIQUE_SLUG);
            }
        }

        $entity = $this->update($entity, $request, $newsInput, null, 'ENTITY', false, true);


        // Upload images
        if ($request->files->get('thumb_file')) {

            $oldThumb = $entity->getThumb();
            $imageName = $this->imageUploadHelper->save(
                $request->files->get('thumb_file'),
                'uploads_news_dir',
                'thumb.news_width',
                'thumb.news_height'
            );
            $entity->setThumb($imageName);
        }

        if ($request->get('is_delete_image')) {
            $oldThumb = $entity->getThumb();
            $entity->setThumb('');
        }

        $this->repository->save($entity);

        if (isset($oldThumb) && $oldThumb) {
            $this->imageUploadHelper->unlink($oldThumb, 'uploads_news_dir');
        }

        return $entity;
    }

    public function deleteEntity($entity)
    {
        $oldThumb = $entity->getThumb();
        $this->delete($entity);

        if (isset($oldThumb) && $oldThumb) {
            $this->imageUploadHelper->unlink($oldThumb, 'uploads_news_dir');
        }
    }

    public function uploadImages($request)
    {
        $result = [];
        $imageName = '';
        $uploaded = 0;
        if ($request->files->get('upload')) {;
            $imageName = $this->imageUploadHelper->save(
                $request->files->get('upload'),
                'uploads_images_dir'
            );
            if ($imageName) {
                $uploaded = 1;
            }
        }

        $result = [
            'file_name' => $imageName,
            'uploaded' => $uploaded,
            'url' => $this->getImages($imageName)
        ];

        return $result;
    }
}