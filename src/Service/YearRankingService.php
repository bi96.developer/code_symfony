<?php

namespace App\Service;

class YearRankingService
{
    public function getCurrentYearRanking()
    {
        return $_ENV['CURRENT_YEAR_RANKING'];
    }

    public function getPreviousYearRanking()
    {
        return $_ENV['CURRENT_YEAR_RANKING'] - 1;
    }

    public function getPreviousYearRankingOf($year)
    {
        return $year - 1;
    }
}
