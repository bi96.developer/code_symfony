<?php

namespace App\Service;


use App\Entity\SyncLog;
use App\Repository\RedirectionRepository;
use App\DTO\Redirection\AddRedirectionInput;
use App\DTO\Redirection\UpdateRedirectionInput;
use App\Entity\Redirection;

/**
 * Class RedirectionService
 * @package App\Service
 */
class RedirectionService extends BaseService
{

    /**
     * RedirectionService constructor.
     * @param RedirectionRepository $repository
     */
    public function __construct(
        RedirectionRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

    public function saveOldLink($syncOldLinks, $currentSync)
    {

        if ($syncOldLinks) {

            $keyField = array_flip(array_keys($syncOldLinks['columns']));
            $dataError = null;

            try {

                foreach ($syncOldLinks['data'] as $syncOldLink) {

                    $dataError = $syncOldLink;
                    $redirection = $this->repository->findOneBy([
                        'targetId' => $syncOldLink[$keyField['target_id']],
                        'type' => $syncOldLink[$keyField['type']],
                    ]);

                    if ($redirection) {

                        $updateRedirectInput = new UpdateRedirectionInput();
                        $urlOld = explode(',', $redirection->getUrlOld());

                        $updateRedirectInput->urlOld = $syncOldLink[$keyField['link']];

                        if (!empty($urlOld) && !in_array($syncOldLink[$keyField['link']], $urlOld)) {

                            array_push($urlOld, $syncOldLink[$keyField['link']]);
                            $updateRedirectInput->urlOld = implode(",", $urlOld);
                        }

                        $this->update($redirection, (array)$updateRedirectInput);
                    } else {

                        $addRedirectionInput = new AddRedirectionInput();
                        $addRedirectionInput->targetId = $syncOldLink[$keyField['target_id']];
                        $addRedirectionInput->type = $syncOldLink[$keyField['type']];
                        $addRedirectionInput->urlOld = $syncOldLink[$keyField['link']];

                        $this->add((array)$addRedirectionInput);
                    }
                }

                $this->syncLogService->log(SyncLog::ACTION_SYNC_OLD_LINK, $syncOldLinks, SyncLog::STATUS_SUCCESS, $currentSync);
            } catch (\Exception $e) {

                $this->syncLogService->log(SyncLog::ACTION_SYNC_OLD_LINK, $syncOldLinks, SyncLog::STATUS_ERROR, $currentSync, $dataError, $e->getMessage());
            }
        }
    }

    public function checkRedirectionSchool($path)
    {
        return $this->repository->checkRedirectionSchool($path);
    }

    public function checkRedirectionSchoolType($path)
    {
        return $this->repository->checkRedirectionSchoolType($path);
    }
}