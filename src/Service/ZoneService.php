<?php
namespace App\Service;

use App\Repository\ZoneRepository;

/**
 * Class ZoneService
 * @package App\Service
 */
class ZoneService extends BaseService
{
    /**
     * ZoneService constructor.
     * @param ZoneRepository $repository
     */
    public function __construct(
        ZoneRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
