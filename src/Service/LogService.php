<?php
namespace App\Service;

use App\Repository\LogRepository;

/**
 * Class LogService
 * @package App\Service
 */
class LogService extends BaseService
{
    /**
     * LogService constructor.
     * @param LogRepository $repository
     */
    public function __construct(
        LogRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
