<?php

namespace App\Service;

use App\DTO\School\AddSchoolSyncInput;
use App\DTO\School\SchoolForEditOutput;
use App\DTO\School\SchoolSearchOutput;
use App\DTO\School\SchoolOutput;
use App\DTO\School\SchoolListOutput;
use App\DTO\School\UpdateSchoolSyncInput;
use App\Entity\School;
use App\Entity\SchoolLink;
use App\Entity\SyncLog;
use App\Repository\SchoolRepository;
use App\Repository\SchoolTypeRepository;
use App\Exception\BadRequestException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SchoolService
 * @package App\Service
 */
class SchoolService extends BaseService
{
    /**
     * SchoolService constructor.
     * @param BrokerRepository $repository
     */
    public function __construct(
        SchoolRepository $repository,
        SchoolTypeRepository $schoolTypeRepository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
        $this->schoolTypeRepository = $schoolTypeRepository;
    }

    public function getPublishedSchoolBySlug($slug)
    {
        $schoolDto = $this->repository->getPublishedSchoolBy('slug', $slug, SchoolOutput::class);

        if (!$schoolDto) {
            return;
        }

        $schoolDtoArr = [$schoolDto];

        $this->bindDataForSchoolOutput($schoolDtoArr);

        return $schoolDtoArr[0];
    }


    public function getSchoolForEdit($id)
    {
        $schoolDto = $this->repository->getSchoolById($id, SchoolForEditOutput::class);

        $schoolDtoArr = [$schoolDto];

        $this->schoolLinkRepository->fetchSchoolLink(
            $schoolDtoArr
        );

        return $schoolDtoArr[0];
    }

    public function getPublishedSchoolById($id)
    {
        $schoolDto = $this->repository->getPublishedSchoolBy('id', $id, SchoolOutput::class);

        if (!$schoolDto) {
            return;
        }

        $schoolDtoArr = [$schoolDto];

        $this->bindDataForSchoolOutput($schoolDtoArr);

        return $schoolDtoArr[0];
    }

    public function getPublishedSchoolByOrgId($orgId)
    {
        $schoolDto = $this->repository->getPublishedSchoolBy('orgId', $orgId, SchoolOutput::class);

        if (!$schoolDto) {
            return;
        }

        $schoolDtoArr = [$schoolDto];

        $this->bindDataForSchoolOutput($schoolDtoArr);

        return $schoolDtoArr[0];
    }

    public function getPublishedSchoolsBySchoolType($schoolTypeId, $limit = 5)
    {
        return $this->repository->getPublishedSchoolsBySchoolType($schoolTypeId, SchoolListOutput::class,
            ['point' => 'DESC'], $limit);
    }

    public function updateIsPublish($id, bool $isPublished): void
    {
        /** @var School $school */
        $school = $this->repository->find($id);

        if (!$school) {
            throw new NotFoundHttpException();
        }

        $school->setIsPublished($isPublished);

        $this->entityManager->flush();
    }

    protected function bindDataForSchoolOutput(&$schoolDtoArr)
    {
        // Bind school type
        $this->schoolLinkRepository->fetchSchoolLink($schoolDtoArr, 'App\DTO\SchoolLink\SchoolLinkListOutput');

        // Bind school link
        $this->schoolTypeRepository->fetchSchoolType($schoolDtoArr, 'App\DTO\SchoolType\SchoolTypeListOutput');

        // Bind rank Number
        $this->repository->fetchRankSchoolType($schoolDtoArr);
    }

    public function getListSchoolsByType($schoolType, $year, $request)
    {
        $pagedResult = $this->repository->getListSchoolsByType(
            $schoolType,
            $year,
            $request,
            'App\DTO\School\SchoolListOutput'
        );

        $this->repository->fetchRankSchoolType($pagedResult['list']);

        // Fetch rank for previous year
        $this->schoolPointRepository->fetchSchoolPoint(
            $pagedResult['list'],
            $this->yearRankingService->getPreviousYearRankingOf($year),
            'App\DTO\SchoolPoint\SchoolPointListOutput',
            'schoolPointPrevious'
        );

        // Fetch school type
        $this->schoolTypeRepository->fetchSchoolType(
            $pagedResult['list'],
            'App\DTO\SchoolType\SchoolTypeListOutput'
        );

        return $pagedResult;
    }

    public function getListAdmin($requestData)
    {
        if ($requestData instanceof Request) {
            $requestData = $requestData->query->all();
        }

        // Get sort
        $sortByPointDirection = null;

        foreach ($requestData as $key => $value) {
            if (strpos($key, 'sort_') === false) {
                continue;
            }

            if ($value === 'rankNumber') {
                $sortByPointDirection = strpos($key, '_asc') !== false ? 'DESC' : 'ASC';

                unset($requestData[$key]);
            }
        }

        $result = $this->repository->getList($requestData, 'App\DTO\School\AdminListOutput',
            function (QueryBuilder $queryBuilder) use ($sortByPointDirection, $requestData) {
                if ($sortByPointDirection) {
                    $queryBuilder->orderBy('School.point', $sortByPointDirection);
                }

                $expr = $queryBuilder->expr();

                $socialNames = [
                    'twitter',
                    'facebook',
                    'instagram',
                    'linkedin',
                    'youtube'
                ];

                $reviewNames = [
                    'diplomeo',
                    'letudiant',
                    'google'
                ];

                foreach ($requestData as $key => $value) :
                    foreach ($socialNames as $socialName) :
                        if ($key !== $socialName) {
                            continue;
                        }

                        $socialUrl = sprintf('School.%sUrl', $socialName);
                        $socialFollower = sprintf('School.%sFollower', $socialName);

                        if ($value == 'link_valid') {
                            $queryBuilder->andWhere(
                                $expr->andX(
                                    $expr->isNotNull($socialUrl),
                                    $expr->neq($socialUrl, "''"),
                                    $expr->gt($socialFollower, 0)
                                )
                            );
                        } elseif ($value == 'link_missing') {
                            $queryBuilder->andWhere(
                                $expr->orX(
                                    $expr->isNull($socialUrl),
                                    $expr->eq($socialUrl, "''")
                                )
                            );
                        } elseif ($value == 'link_to_verify') {
                            $queryBuilder->andWhere(
                                $expr->andX(
                                    $expr->isNotNull($socialUrl),
                                    $expr->neq($socialUrl, "''"),
                                    $expr->orX(
                                        $expr->isNull($socialFollower),
                                        $expr->eq($socialFollower, 0)
                                    )
                                )
                            );
                        }
                    endforeach;
                endforeach;

                foreach ($requestData as $key => $value) :
                    foreach ($reviewNames as $reviewName) :
                        if ($key !== $reviewName) {
                            continue;
                        }

                        $urlFieldName = $reviewName == 'google' ?
                            sprintf('School.%sPlaceId', $reviewName) :
                            sprintf('School.%sUrl', $reviewName);
                        $ratingFieldName = sprintf('School.%sRating', $reviewName);
                        $reviewFieldName = sprintf('School.%sReview', $reviewName);

                        if ($value == 'link_valid') {
                            $queryBuilder->andWhere(
                                $expr->andX(
                                    $expr->isNotNull($urlFieldName),
                                    $expr->neq($urlFieldName, "''"),
                                    $expr->gt($ratingFieldName, 0),
                                    $expr->gt($reviewFieldName, 0)
                                )
                            );
                        } elseif ($value == 'link_missing') {
                            $queryBuilder->andWhere(
                                $expr->orX(
                                    $expr->isNull($urlFieldName),
                                    $expr->eq($urlFieldName, "''")
                                )
                            );
                        } elseif ($value == 'link_to_verify') {
                            $queryBuilder->andWhere(
                                $expr->andX(
                                    $expr->isNotNull($urlFieldName),
                                    $expr->neq($urlFieldName, "''"),
                                    $expr->orX(
                                        $expr->isNull($ratingFieldName),
                                        $expr->eq($ratingFieldName, 0),
                                        $expr->isNull($reviewFieldName),
                                        $expr->eq($reviewFieldName, 0)
                                    )
                                )
                            );
                        }
                    endforeach;
                endforeach;


//                if (isset($requestData['valid_point'])) {
//                    $expr = $queryBuilder->expr();
//
//                    $groupOrX = $queryBuilder->expr()->orX();
//
//                    foreach ($socialNames as $socialName) :
//                        $socialUrl = sprintf('School.%sUrl', $socialName);
//                        $socialFollower = sprintf('School.%sFollower', $socialName);
//
//                        // Have social link, but haven't score
//                        $groupOrX->add(
//                            $expr->andX(
//                                $expr->isNotNull($socialUrl),
//                                $expr->neq($socialUrl, "''"),
//                                $expr->orX(
//                                    $expr->isNull($socialFollower),
//                                    $expr->eq($socialFollower, 0)
//                                )
//                            )
//                        );
//                    endforeach;
//
//                    foreach ($reviewNames as $reviewName) :
//                        $urlFieldName = $reviewName == 'google' ?
//                            sprintf('School.%sPlaceId', $reviewName) :
//                            sprintf('School.%sUrl', $reviewName);
//                        $ratingFieldName = sprintf('School.%sRating', $reviewName);
//                        $reviewFieldName = sprintf('School.%sReview', $reviewName);
//
//                        // Have review link, but not haven't review or rating.
//                        $groupOrX->add(
//                            $expr->andX(
//                                $expr->isNotNull($urlFieldName),
//                                $expr->neq($urlFieldName, "''"),
//                                $expr->orX(
//                                    $expr->isNull($ratingFieldName),
//                                    $expr->eq($ratingFieldName, 0),
//                                    $expr->isNull($reviewFieldName),
//                                    $expr->eq($reviewFieldName, 0)
//                                )
//                            )
//                        );
//                    endforeach;
//
//                    if ($requestData['valid_point'] == '0') {
//                        $queryBuilder
//                            ->andWhere(
//                                $groupOrX
//                            );
//                    } else {
//                        $queryBuilder->andWhere(
//                            $expr->not($groupOrX)
//                        );
//                    }
//                }
            });

        $this->repository->fetchRank($result['list']);
        $this->repository->fetchRankSchoolType($result['list']);

        $this->schoolTypeRepository->fetchSchoolType($result['list'], 'App\DTO\SchoolType\SchoolTypeOutput');


        return $result;
    }

    public function parseFromJson()
    {
        $data = file_get_contents('school2.json');
        $data = json_decode($data, 1);
        foreach ($data as $schoolArray) {
            $school = $this->schoolRepo->findOneBy(['orgId' => (int)$schoolArray['id']]);
            if (!is_null($school)) {
                continue;
            }
            $school = $this->newEntity();
            $school->setName($schoolArray['name']);
            $school->setOrgId((int)$schoolArray['id']);
            $school->setWebsite($schoolArray['WebSite'] ?? null);
            $school->setPalmes($schoolArray['Palmes'] ?? null);
            $school->setLeague($schoolArray['Ligue'] ?? null);
            $school->setAddress1($schoolArray['Adr_1_1'] ?? null);
            $school->setAddress2($schoolArray['Adr_1_2'] ?? null);
            $school->setAddress3($schoolArray['Adr_1_3'] ?? null);
            $school->setZipcode($schoolArray['CodePostal_1'] ?? null);
            $school->setCity($schoolArray['Ville_1'] ?? null);
            $school->setAddress2($schoolArray['Adr_1_2'] ?? null);
            $school->setAddress2($schoolArray['Adr_1_2'] ?? null);
            $school->setAddress2($schoolArray['Adr_1_2'] ?? null);
            if ($schoolArray['social']) {
                $school->setBanner($schoolArray['social']['banner'] ?? null);
                $school->setLogo($schoolArray['social']['logo'] ?? null);
                $socialList = [
                    'facebook',
                    'instagram',
                    'twitter',
                    'linkedin',
                    'youtube',
                ];
                foreach ($socialList as $socialName) {
                    $school->{'set' . ucfirst($socialName) . 'Url'}(
                        $schoolArray['social'][$socialName . 'Url'] ?? null
                    );
                    if ($socialName == 'youtube') {
                        $school->{'set' . ucfirst($socialName) . 'Follower'}(
                            (int)$schoolArray['social'][$socialName . 'Subscriber'] ?? null
                        );
                    } else {
                        $school->{'set' . ucfirst($socialName) . 'Follower'}(
                            (int)$schoolArray['social'][$socialName . 'Follower'] ?? null
                        );
                    }
                }
                $school->setDiplomeoUrl(
                    $schoolArray['social']['diplomeoUrl'] ?? null
                );
                $school->setDiplomeoRating(
                    (float)$schoolArray['social']['diplomeoRating'] ?? null
                );
                $school->setDiplomeoReview(
                    (int)$schoolArray['social']['diplomeoReviewCount'] ?? null
                );
                $school->setLetudiantUrl(
                    $schoolArray['social']['letudiantUrl'] ?? null
                );
                $school->setLetudiantRating(
                    (float)$schoolArray['social']['letudiantRating'] ?? null
                );
                $school->setLetudiantReview(
                    (int)$schoolArray['social']['letudiantReviewCount'] ?? null
                );
                $school->setGoogleRating(
                    (float)$schoolArray['social']['googleRating'] ?? null
                );
                $school->setGooglePlaceId(
                    $schoolArray['social']['googlePlaceId'] ?? null
                );
                $school->setGooglePlaceLat(
                    (float)$schoolArray['social']['googlePlaceLat'] ?? null
                );
                $school->setGooglePlaceLng(
                    (float)$schoolArray['social']['googlePlaceLng'] ?? null
                );
                $school->setGoogleReview(
                    (int)$schoolArray['social']['googleReview'] ?? null
                );
                $school->setSaReview(
                    (int)$schoolArray['social']['saReview'] ?? null
                );
                $school->setSaRating(
                    (int)$schoolArray['social']['saRating'] ?? null
                );
            }
            $this->repository->save($school);
            $this->setPoint($school);
        }
    }

    public function getPointAll()
    {
        $schools = $this->repository->findAll();
        foreach ($schools as $school) {
            $this->setPoint($school);
        }
    }

    public function setPoint($school)
    {
        $data = $this->getPoint($school);
        $school->setPoint($data['point']);
        $school->setPointData($data);
        $this->repository->save($school);
    }

    public function getPointFormulaTable($id)
    {
        $school = $this->repository->find($id);

        if (!$school) {
            return null;
        }

        $table = [
            'thead' => [],
            'tbody' => [],
            'tfoot' => []
        ];

        $table['thead'][] = [
            'Name',
            'Parameters',
            'Formula',
            'Point',
        ];

        $pointData = $school->getPointData();

        $socialList = [
            'Facebook',
            'Instagram',
            'Twitter',
            'Linkedin',
            'Youtube',
        ];

        foreach ($socialList as $socialName) {
            $table['tbody'][] = [
                $socialName,
                sprintf('follower=%s', $pointData[$socialName]['value']),
                sprintf('(follower * %s) * %s', School::POINT_SOCIAL, School::RATE_SOCIAL),
                $pointData[$socialName]['point']
            ];
        }

        $reviewList = [
            'Sa',
            'Diplomeo',
            'Letudiant',
            'Google'
        ];

        foreach ($reviewList as $reviewName) {
            $rateReview = $reviewName == 'Sa'
                ? School::RATE_SA_REVIEW
                : School::RATE_REVIEW;

            $table['tbody'][] = [
                $reviewName,
                sprintf(
                    'review=%s; rating=%s; ratingPoint=%s', $school->{'get' . $reviewName . 'Review'}(),
                    $school->{'get' . $reviewName . 'Rating'}(),
                    $this->convertRatingValueToPoint($school->{'get' . $reviewName . 'Rating'}())
                ),
                sprintf('((review * %s) + ratingPoint) * %s', School::POINT_REVIEW, $rateReview),
                $pointData[$reviewName]['point']
            ];
        }

        $table['tfoot'][] = [
            null,
            null,
            null,
            $school->getPoint(),
        ];

        return $table;
    }

    public function getPoint($school)
    {
        $socialList = [
            'Facebook',
            'Instagram',
            'Twitter',
            'Linkedin',
            'Youtube',
        ];
        $data = [
            'Review' => [
                'value' => $school->getReviewRating(),
                'pGive' => $school->getReviewRating(),
                'point' => round($school->getReviewRating()
                    * $this->RATE_REVIEW, 4)
            ],
        ];
        $totalPoint = 0;
        foreach ($socialList as $socialName) {
            $value = $school->{'get' . $socialName . 'Follower'}();
            $pGive = $value * $this->POINT_SOCIAL;
            $point = round($pGive * $this->RATE_SOCIAL, 4);
            $data[$socialName] = [
                'value' => $value,
                'pGive' => round($pGive, 4),
                'point' => $point
            ];
            $totalPoint += $point;
        }
        $reviewList = [
            'Sa',
            'Diplomeo',
            'Letudiant',
            'Google'
        ];
        foreach ($reviewList as $reviewName) {
            $rateReview = $reviewName == 'Sa'
                ? $this->RATE_SA_REVIEW
                : $this->RATE_REVIEW;
            $reviewCount = $school->{'get' . $reviewName . 'Review'}();
            $rating = $school->{'get' . $reviewName . 'Rating'}();
            $pGive = $reviewCount * $this->POINT_REVIEW + $this->convertRatingValueToPoint($rating);
            $point = round($pGive * $rateReview, 4);
            $data[$reviewName] = [
                'value' => $value,
                'pGive' => round($pGive, 4),
                'point' => $point
            ];
            $totalPoint += $point;
        }
        $data['point'] = round($totalPoint, 4);
        return $data;
    }

    public function getLogo($school)
    {
        if ($school->getLogoUrl()) {
            return $school->getLogoUrl();
        }
        if (!$school->getLogo()) {
            return null;
        }
        return null;
    }

    public function getBanner($school)
    {
        if (!$school->getBanner()) {
            return null;
        }
        return $this->urlHelper->getAbsoluteUrl(
            sprintf(
                '/uploads/school/%s/%s',
                $school->getOrgId(),
                'banner.jpg?v=' . $school->getBanner()
            )
        );
    }

    public function addEntity($request)
    {
        $school = $this->add($request, null, null, 'ENTITY', false, [], true);
        $maxOrgId = $this->repository->getMaxOrgId();
        $school->setOrgId($maxOrgId + 1);
        $this->repository->save($school);
        $this->setSchoolFiles($request, $school);
        $this->setPoint($school);

        $school = (array)$this->autoMapper->map($school, 'App\DTO\School\SchoolOutput');
        $this->repository->fetchRank($school);
        return $school;
    }

    public function updateEntity($request)
    {
        /** @var School $school */
        $school = $this->get($request->request->get('id'));

        $this->update($school, $request, null, null, 'ENTITY', false, true);

        if (!empty($request->request->get('schoolLink'))) {
            $schoolLink = $school->getSchoolLink();

            if (!$schoolLink) {
                $schoolLink = new SchoolLink();
                $schoolLink->setSchool($school);
                $schoolLink->setOrgId($school->getOrgId());
            }

            $this->autoMapper->mapToObject(
                $request->request->get('schoolLink'),
                $schoolLink
            );

            $this->entityManager->persist($schoolLink);
            $this->entityManager->flush();
        }

        $this->setSchoolFiles($request, $school);

        $this->setPoint($school);

        $school = (array)$this->autoMapper->map($school, 'App\DTO\School\SchoolOutput');
        $this->repository->fetchRank($school);
        return $school;
    }

    public function setSchoolFiles($request, $school)
    {
        //todo: remove files
        $schoolFolder = $this->getPath('/uploads/school/' . $school->getOrgId());
        $this->getFolder($schoolFolder);
        if ($request->files->get('logo')) {
            $random = md5(uniqid());
            $request->files->get('logo')->move(
                $schoolFolder,
                'logo.jpg'
            );
            $school->setLogo($random);
        } else {
            if (
                $request->request->get('logo') === ''
                && !is_null($school->getLogo())
            ) {
                $school->setLogo(null);
            }
        }
        if ($request->files->get('banner')) {
            $random = md5(uniqid());
            $request->files->get('banner')->move(
                $schoolFolder,
                'banner.jpg'
            );
            $school->setBanner($random);
        } else {
            if (
                $request->request->get('banner') === ''
                && !is_null($school->getBanner())
            ) {
                $school->setBanner(null);
            }
        }
    }

    public function crawl($id, $force = false)
    {
        $request = $this->requestStack->getCurrentRequest();
        $school = $this->get($id);

        if (
            !$force
            && ($school->getCrawlStatus() === 'pending'
                || $school->getCrawlStatus() === 'finish'
            )
        ) {
            $school = (array)$this->autoMapper->map($school, 'App\DTO\School\SchoolOutput');
            return $school;
        }

        if (!$school->getUpdateData()) {
            $school->setCrawlStatus('finish');
            $this->setPoint($school);
            throw new BadRequestException(
                BadRequestException::NO_CRAWL_DATA
            );
        }

        $now = new \DateTime('now');
        $crawlerHost = $this->params->get('crawler.host');
        $url = 'http://' . $crawlerHost . ':6801/schedule.json';
        $inputs = [
            'id'          => $school->getId(),
            'name'        => $school->getName(),
            'callback'    => $request->getSchemeAndHttpHost() . $request->getBasePath() . '/admin/school/update',
            'pendingDate' => $now->format('Y/m/d H:i:s')
        ];
        foreach (explode(',', $school->getUpdateData()) as $field) {
            $value = $school->{'get' . ucfirst($field)}();
            if (!empty($value)) {
                $inputs[$field] = $value;
            }
        }
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);

        $data = "project=social_crawler&spider=single&input=" . base64_encode(json_encode($inputs, 1));

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $resp = curl_exec($curl);
        curl_close($curl);

        $school->setCrawlStatus('pending');
        $this->repository->save($school);
        $school = (array)$this->autoMapper->map($school, 'App\DTO\School\SchoolOutput');
        $school['crawlStatus'] = 'pending';
        return $school;
    }

    public function searchSchool($requestData)
    {
        if ($requestData instanceof Request) {
            $requestData = $requestData->query->all();
        }

        $searchedSchools = $this->repository->searchSchool($requestData, SchoolSearchOutput::class);

        $schools = [];
        foreach ($searchedSchools as $searchedSchool) {
            $schools[] = (array)$searchedSchool;
        }

        $this->repository->fetchRankSchoolType($schools);

        return $schools;
    }

    public function fixSA()
    {
        $schools = $this->repository->findAll();
        foreach ($schools as $school) {
            try {
                $sa = file_get_contents('https://studiesadvisor.net/api/school-rating/id/' . $school->getOrgId());
            } catch (\Exception $e) {
                continue;
            }
            $sa = json_decode($sa, true);
            $school->setSaRating($sa['rating'] / 2);
            $school->setSaReview($sa['total_review']);
            $this->setPoint($school);
        }
    }

    public function fixLinkedin()
    {
        $schools = array_map('str_getcsv',
            file($this->container->getParameter('kernel.project_dir') . '/src/Migrations/sqls/Linkedin.csv'));
        array_walk($schools, function (&$a) use ($schools) {
            $a = array_combine($schools[0], $a);
        });
        array_shift($schools);
        foreach ($schools as $schoolCSV) {
            $school = $this->repository->findOneBy(['orgId' => $schoolCSV['EID']]);
            if (is_null($school)) {
                continue;
            }
            $follower = (int)str_replace(',', '', $schoolCSV['Followers']);
            $school->setLinkedinFollower($follower);
            $this->setPoint($school);
        }
    }

    public function updateStatusToActive($orgId)
    {
        $school = $this->repository->findOneBy([
            'orgId' => $orgId
        ]);

        if (!$school) {
            return;
        }

        $school->setStatus(School::STATUS_ACTIVE);
        $this->repository->flush();
    }

    public function updateStatusToInActive($orgId)
    {
        $school = $this->repository->findOneBy([
            'orgId' => $orgId
        ]);

        if (!$school) {
            return;
        }

        $school->setStatus(School::STATUS_INACTIVE);
        $this->repository->flush();
    }

    public function syncSchool($syncSchools, $currentSync)
    {
        $arrayFields = [
            "name"           => "name",
            "slug"           => "slug",
            "logo"           => "logoUrl",
            "website"        => "website",
            "address"        => "address1",
            "twitter_url"    => "twitterUrl",
            "facebook_url"   => "facebookUrl",
            "instagram_url"  => "instagramUrl",
            "linkedin_url"   => "linkedinUrl",
            "youtube_url"    => "youtubeUrl",
            "diplomeo_url"   => "diplomeoUrl",
            "letudiant_url"  => "letudiantUrl",
            "google_map_url" => "googlePlaceId",
        ];

        if ($syncSchools) {

            $keyField = array_flip(array_keys($syncSchools['columns']));
            $dataError = null;

            try {
                foreach ($syncSchools['data'] as $syncShool) {

                    $dataError = $syncShool;

                    /** @var School $school */
                    $school = $this->repository->findOneBy([
                        'orgId' => $syncShool[$keyField['id']]
                    ]);

                    if ($school) {

                        $updateSchoolInput = new UpdateSchoolSyncInput();

                        if ($syncShool[$keyField['school_type_id']]) {
                            $schoolType = $this->schoolTypeRepository->find($syncShool[$keyField['school_type_id']]);

                            if ($schoolType) {
                                $updateSchoolInput->schoolType = $schoolType;
                            }
                        }

                        foreach ($arrayFields as $key => $value) {
                            if (in_array($key, array_keys($syncSchools['columns']))) {
                                $updateSchoolInput->{$value} = $syncShool[$keyField[$key]] ?? 'isNull';
                            }
                        }

                        // Auto generate slug if it's empty
                        if (!$updateSchoolInput->slug) {
                            $updateSchoolInput->slug = $this->commonService->slugify($updateSchoolInput->name);
                        }

                        $this->update($school, (array)$updateSchoolInput, null, 'App\DTO\School\UpdateSchoolSyncInput');

                        if ($school->getStatus() !== School::STATUS_ACTIVE) {
                            $this->updateStatusToActive($school->getOrgId());
                        }
                    } else {

                        $addSchoolInput = new AddSchoolSyncInput();

                        $addSchoolInput->orgId = $syncShool[$keyField['id']];

                        if ($syncShool[$keyField['school_type_id']]) {
                            $schoolType = $this->schoolTypeRepository->find($syncShool[$keyField['school_type_id']]);
                            if ($schoolType) {
                                $addSchoolInput->schoolType = $schoolType;
                            }
                        }

                        foreach ($arrayFields as $key => $value) {
                            if (in_array($key, array_keys($syncSchools['columns']))) {
                                $addSchoolInput->{$value} = $syncShool[$keyField[$key]];
                            }
                        }

                        $addSchoolInput->status = School::STATUS_ACTIVE;

                        // Auto generate slug if it's empty
                        if (!$addSchoolInput->slug) {
                            $addSchoolInput->slug = $this->commonService->slugify($addSchoolInput->name);
                        }

                        $this->add((array)$addSchoolInput, null, 'App\DTO\School\AddSchoolSyncInput');
                    }
                }

                $this->syncLogService->log(SyncLog::ACTION_SYNC_SCHOOL, $syncSchools, SyncLog::STATUS_SUCCESS,
                    $currentSync);
            } catch (\Exception $e) {

                $this->syncLogService->log(SyncLog::ACTION_SYNC_SCHOOL, $syncSchools, SyncLog::STATUS_ERROR,
                    $currentSync, $dataError, $e->getMessage());
            }
        }
    }

    public function softDeletedSchool($orgIdSchool)
    {
        $school = $this->repository->findOneBy(['orgId' => $orgIdSchool]);
        if ($school) {
            $this->repository->delete($school);
        }
    }

    public function restoreSchool($idSchool)
    {
        if ($this->entityManager->getFilters()->isEnabled('softdeleteable')) {
            $this->entityManager->getFilters()->disable('softdeleteable');
        }
        $school = $this->repository->find($idSchool);
        if ($school) {
            $school->setDeletedAt(null);
            $this->entityManager->flush();
        }
    }

    public function crawlFixDiplomeo()
    {
        $data = file_get_contents('diplomeo2.json');
        $data = json_decode($data, 1);
        foreach ($data as $schoolCSV) {
            $school = $this->repository->findOneBy(['id' => $schoolCSV['id']]);
            if (is_null($school)) {
                continue;
            }
            $reviewCount = (int)str_replace(',', '', $schoolCSV['review_count']);
            $school->setDiplomeoReview($reviewCount);
            $this->setPoint($school);
        }
    }

    /**
     * Convert rating value to point, rating on 5 scale.
     *
     * @param $ratingValue
     * @return int|mixed
     */
    public function convertRatingValueToPoint($ratingValue)
    {
        $map = [
            '1'   => 1,
            '1.1' => 2,
            '1.2' => 3,
            '1.3' => 4,
            '1.4' => 5,
            '1.5' => 6,
            '1.6' => 7,
            '1.7' => 8,
            '1.8' => 9,
            '1.9' => 10,
            '2'   => 11,
            '2.1' => 12,
            '2.2' => 13,
            '2.3' => 14,
            '2.4' => 15,
            '2.5' => 16,
            '2.6' => 17,
            '2.7' => 18,
            '2.8' => 19,
            '2.9' => 20,
            '3'   => 21,
            '3.1' => 22,
            '3.2' => 23,
            '3.3' => 24,
            '3.4' => 25,
            '3.5' => 26,
            '3.6' => 27,
            '3.7' => 28,
            '3.8' => 29,
            '3.9' => 30,
            '4'   => 31,
            '4.1' => 32,
            '4.2' => 33,
            '4.3' => 34,
            '4.4' => 35,
            '4.5' => 36,
            '4.6' => 37,
            '4.7' => 38,
            '4.8' => 39,
            '4.9' => 40,
            '5'   => 41,
        ];

        $ratingValue = (string)round($ratingValue, 1);

        if (isset($map[$ratingValue])) {
            return $map[$ratingValue];
        }

        return 0;
    }
}
