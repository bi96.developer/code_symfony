<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\NamedAddress;
use Twig\Environment;

class MailService
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(
        MailerInterface $mailer,
        Environment $twig,
        ContainerInterface $container
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->container = $container;
    }

    public function send($toAddress, $subject, $body, $attachmentPaths = null)
    {
        $fromAddress = $this->container->getParameter('mail.no_reply.from_address');
        $fromName = $this->container->getParameter('mail.no_reply.from_name');


        $email = (new Email())
            ->from(new NamedAddress($fromAddress, $fromName))
            ->to($toAddress)
            ->subject($subject)
            ->html($body);

        if (!is_array($attachmentPaths) && $attachmentPaths !== null) {
            $attachmentPaths = [$attachmentPaths];
        }

        if (is_array($attachmentPaths) && count($attachmentPaths) > 0) {
            foreach ($attachmentPaths as $attachmentPath) {
                $email->attachFromPath($attachmentPath);
            }
        }

        $this->mailer->send($email);
    }

    public function buildTemplate($template, $data)
    {
        return $this->twig->render($template, $data);
    }
}
