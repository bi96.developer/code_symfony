<?php

namespace App\Service;

use App\DTO\Question\QuestionListOutput;
use App\Repository\QuestionRepository;

/**
 * Class QuestionService
 * @package App\Service
 */
class QuestionService extends BaseService
{

    /**
     * QuestionService constructor.
     * @param QuestionRepository $repository
     */
    public function __construct(
        QuestionRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

    /**
     * @return QuestionListOutput[]
     */
    public function getQuestion(): array
    {
        return $this->repository->getQuestion(QuestionListOutput::class);
    }

    public function getListQuestionsOfSchoolType($schoolTypeId)
    {
        return $this->repository->getQuestionOfSchoolType($schoolTypeId, QuestionListOutput::class);
    }
}
