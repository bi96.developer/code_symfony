<?php

namespace App\Service;

use App\DTO\SchoolType\AddSchoolTypeInput;
use App\DTO\SchoolType\UpdateSchoolTypeInput;
use App\Entity\SchoolType;
use App\Entity\SyncLog;
use App\DTO\SchoolType\SchoolTypeOutput;
use App\Repository\SchoolTypeRepository;
use App\DTO\School\SchoolOutput;

/**
 * Class SchoolService
 * @package App\Service
 */
class SchoolTypeService extends BaseService
{
    private $allPublishedSchoolTypes;

    private $syncLogService;

    /**
     * SchoolService constructor.
     * @param SchoolTypeRepository $repository
     */

    public function __construct(
        SchoolTypeRepository $repository,
        SyncLogService $syncLogService,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
        $this->syncLogService = $syncLogService;
    }

    public function getListAllAdmin()
    {
        return $this->repository->getAllSchoolTypes(SchoolTypeOutput::class);
    }

    public function getSchoolTypeBySlug($slug)
    {
        return $this->repository->findOneBy([
            'slug' => $slug
        ]);
    }

    public function getPublishedSchoolTypeById($id)
    {
        return $this->repository->getPublishedSchoolTypeById($id, SchoolTypeOutput::class);
    }

    public function getPublishedSchoolTypeBySlug($slug)
    {
        return $this->repository->getPublishedSchoolTypeBySlug($slug, SchoolTypeOutput::class);
    }

    /**
     * @return SchoolTypeOutput[]
     */
    public function getAllPublishedSchoolTypes(): array
    {
        if (!$this->allPublishedSchoolTypes) {
            $this->allPublishedSchoolTypes = $this->repository->getAllPublishedSchoolTypes(SchoolTypeOutput::class);
        }

        return $this->allPublishedSchoolTypes;
    }

    public function syncSchoolType($syncSchoolTypes, $currentSync)
    {

        if ($syncSchoolTypes) {

            $keyField = array_flip(array_keys($syncSchoolTypes['columns']));
            $dataError = null;

            try {

                foreach ($syncSchoolTypes['data'] as $syncShoolType) {

                    $dataError = $syncShoolType;

                    /** @var SchoolType $schoolType */
                    $schoolType = $this->repository->find($syncShoolType[$keyField['id']]);

                    if ($schoolType) {

                        $updateSchoolTypeInput = new UpdateSchoolTypeInput();
                        $updateSchoolTypeInput->name = $syncShoolType[$keyField['name']];
                        $updateSchoolTypeInput->slug = $syncShoolType[$keyField['slug']];

                        if (!$updateSchoolTypeInput->slug) {
                            $updateSchoolTypeInput->slug = $this->commonService->slugify($updateSchoolTypeInput->name);
                        }

                        $this->update($schoolType, (array)$updateSchoolTypeInput);

                        if ($schoolType->getStatus() !== SchoolType::STATUS_ACTIVE) {
                            $this->updateStatusToActive($schoolType->getId());
                        }
                    } else {
                        $addSchoolTypeInput = new AddSchoolTypeInput();

                        $addSchoolTypeInput->id = $syncShoolType[$keyField['id']];
                        $addSchoolTypeInput->name = $syncShoolType[$keyField['name']];
                        $addSchoolTypeInput->slug = $syncShoolType[$keyField['slug']];
                        $addSchoolTypeInput->status = SchoolType::STATUS_ACTIVE;

                        if (!$addSchoolTypeInput->slug) {
                            $addSchoolTypeInput->slug = $this->commonService->slugify($addSchoolTypeInput->name);
                        }

                        $this->add((array)$addSchoolTypeInput);
                    }
                }

                $this->syncLogService->log(
                    SyncLog::ACTION_SYNC_SCHOOL_TYPE,
                    $syncSchoolTypes,
                    SyncLog::STATUS_SUCCESS,
                    $currentSync
                );
            } catch (\Exception $e) {

                $this->syncLogService->log(
                    SyncLog::ACTION_SYNC_SCHOOL_TYPE,
                    $syncSchoolTypes,
                    SyncLog::STATUS_ERROR,
                    $currentSync,
                    $dataError,
                    $e->getMessage()
                );
            }
        }
    }

    public function updateStatusToActive($schoolTypeId)
    {
        $schoolType  = $this->repository->find($schoolTypeId);

        if (!$schoolType) {
            return;
        }

        $schoolType->setStatus(SchoolType::STATUS_ACTIVE);
        $this->repository->flush();
    }

    public function updateStatusToInActive($schoolTypeId)
    {
        $schoolType  = $this->repository->find($schoolTypeId);

        if (!$schoolType) {
            return;
        }

        $schoolType->setStatus(SchoolType::STATUS_INACTIVE);
        $this->repository->flush();
    }

    public function softDeletedSchoolType($idSchoolType)
    {
        $schoolType = $this->repository->find($idSchoolType);
        if ($schoolType) {
            $this->repository->delete($schoolType);
        }
    }

    public function restoreSchoolType($idSchoolType)
    {
        if ($this->entityManager->getFilters()->isEnabled('softdeleteable')) {
            $this->entityManager->getFilters()->disable('softdeleteable');
        }
        $schoolType = $this->repository->find($idSchoolType);
        if ($schoolType) {
            $schoolType->setDeletedAt(null);
            $this->entityManager->flush();
        }
    }

    public function calculateAverageRatingAndTotalReview($schoolTypeId)
    {
        $schools = $this->schoolRepository->getPublishedSchoolsBySchoolType($schoolTypeId, SchoolOutput::class);

        if (empty($schools)) {
            return [0, 0];
        }

        $ratingSchool = 0;
        $countSchool = 0;
        $totalReview = 0;
        foreach ($schools as $school) {
            if ($school->averageRating > 0) {
                $ratingSchool += $school->averageRating;
                $countSchool++;
                $totalReview += $school->totalReview;
            }
        }

        if ($ratingSchool == 0 || $totalReview == 0) {
            return [0, 0];
        }

        $averageRating = round($ratingSchool / $countSchool, 2);

        return [
            max($averageRating, 1),
            $totalReview
        ];
    }
}
