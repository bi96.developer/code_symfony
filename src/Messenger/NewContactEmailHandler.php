<?php

namespace App\Messenger;

use App\Service\MailService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class NewContactEmailHandler implements MessageHandlerInterface
{
    /**
     * @var MailService
     */
    private $mailService;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(
        MailService $mailService,
        ContainerInterface $container
    ) {
        $this->mailService = $mailService;
        $this->container = $container;
    }

    public function __invoke(NewContactEmail $newContactEmail)
    {
        $subject = '[' . $this->container->getParameter('site_name') . '] Nouveau contact de ' . $newContactEmail->getContactInfo()->email;
        $body = $this->mailService->buildTemplate('email/contact/new_contact.html.twig', [
            'contact' => $newContactEmail->getContactInfo()
        ]);


        $toEmail = $this->container->getParameter('mail.contact_pack.address');

        $this->mailService->send($toEmail, $subject, $body);
    }

}
