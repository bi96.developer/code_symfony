<?php

namespace App\Messenger;

use App\DTO\Contact\ContactInput;

class NewContactEmail
{
    /** @var ContactInput  */
    public $contactInfo;

    public function __construct(ContactInput $contactInfo)
    {
        $this->contactInfo = $contactInfo;
    }

    /**
     * @return ContactInput
     */
    public function getContactInfo(): ContactInput
    {
        return $this->contactInfo;
    }
}
