<?php


namespace App\Traits;


trait EntityReactionAbleTrait
{
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalView = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalLike = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalComment = 0;

    public function getTotalLike(): ?int
    {
        return $this->totalLike;
    }

    public function setTotalLike(?int $totalLike): self
    {
        $this->totalLike = $totalLike;

        return $this;
    }

    public function getTotalComment(): ?int
    {
        return $this->totalComment;
    }

    public function setTotalComment(?int $totalComment): self
    {
        $this->totalComment = $totalComment;

        return $this;
    }

    public function getTotalView(): ?int
    {
        return $this->totalView;
    }

    public function setTotalView(?int $totalView): self
    {
        $this->totalView = $totalView;

        return $this;
    }
}
