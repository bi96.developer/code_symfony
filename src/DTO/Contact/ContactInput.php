<?php

namespace App\DTO\Contact;

use Symfony\Component\Validator\Constraints as Assert;

class ContactInput
{
    /**
     * @Assert\NotBlank(
     *     message="Prénom est requis"
     * )
     */
    public $firstName;

    /**
     * @Assert\NotBlank(
     *     message="Nom est requis"
     * )
     */
    public $lastName;

    /**
     * @Assert\NotBlank(
     *     message="Pays est requis"
     * )
     */
    public $country;

    public $phone;

    /**
     * @Assert\NotBlank(
     *     message="Email est requis"
     * )
     *
     * @Assert\Email(
     *     message="Email n'est pas valide"
     * )
     */
    public $email;

    public $function;

    public $schoolName;

    /**
     * @Assert\NotBlank(
     *     message="Message est requis"
     * )
     */
    public $message;
}
