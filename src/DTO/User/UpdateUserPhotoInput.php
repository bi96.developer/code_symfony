<?php

namespace App\DTO\User;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class UpdateUserPhotoInput
{
    /**
     * @Assert\NotBlank(
     *     message="Photo can not be blank"
     * )
     */
    public $photoContent;
}
