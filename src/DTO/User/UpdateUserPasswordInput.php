<?php

namespace App\DTO\User;

use Symfony\Component\Validator\Constraints as Assert;
use App\DTO\Validator\ContainsUnique;
use App\Annotation\TransformAnnotation;

class UpdateUserPasswordInput
{
    /**
     * @Assert\NotBlank(
     *     message = "New password can not be blank"
     * )
     * @Assert\Length(
     *     min="6",
     *     minMessage="New password should be longer than 6 characters"
     * )
     */
    public $newPassword;

    /**
     * @Assert\NotBlank(
     *     message = "Current password can not be blank"
     * )
     */
    public $currentPassword;
}
