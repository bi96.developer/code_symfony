<?php

namespace App\DTO\User;

class AdminListOutput
{
    public $id;
    public $subRoles;
    public $firstName;
    public $lastName;
    public $email;
    public $phone;
    public $country;
    public $city;
    public $token;
    public $authPermitted;
    public $contentDeleted;
}
