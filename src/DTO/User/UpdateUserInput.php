<?php

namespace App\DTO\User;

use Symfony\Component\Validator\Constraints as Assert;
use App\DTO\Validator\ContainsUnique;
use App\Annotation\TransformAnnotation;

class UpdateUserInput
{
    /**
     * @Assert\NotBlank(
     *     message = "First name can not be blank"
     * )
     */
    public $firstName;

    /**
     * @Assert\NotBlank(
     *     message = "Last name can not be blank"
     * )
     */
    public $lastName;

    public $phone;

    public $city;

    /**
     * @TransformAnnotation(
     *     type="entity",
     *     class="App\Entity\Country"
     * )
     */
    public $country;

    /**
     * @TransformAnnotation(
     *     type="collectionEntity",
     *     class="App\Entity\Language"
     * )
     */
    public $languages;

    public $handicap;

    /**
     * @Assert\NotBlank(
     *     message = "User group can not be blank"
     * )
     *
     * @TransformAnnotation(
     *     type="collectionEntity",
     *     field="name",
     *     class="App\Entity\Role"
     * )
     */
    public $subRoles;

    public $facebookUrl;

    public $twitterUrl;

    public $youtubeUrl;

    public $instagramUrl;
    public $state;
}
