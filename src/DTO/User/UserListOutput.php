<?php

namespace App\DTO\User;

class UserListOutput
{
    public $id;
    public $subRoles;
    public $firstName;
    public $lastName;
    public $email;
    public $phone;
    public $country;
    public $city;
    public $state;
    public $picture;
}
