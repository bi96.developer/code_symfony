<?php

namespace App\DTO\User;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class UpdateUserCoverInput
{
    /**
     * @Assert\NotBlank(
     *     message="Cover can not be blank"
     * )
     */
    public $coverContent;
}
