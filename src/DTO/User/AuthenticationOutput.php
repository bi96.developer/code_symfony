<?php

namespace App\DTO\User;

use Symfony\Component\Validator\Constraints as Assert;

class AuthenticationOutput
{
    public $id;

    public $email;

    public $userGroup;

    public $firstName;

    public $lastName;

    public $language;

    public $handicap;
}
