<?php

namespace App\DTO\User;

use Symfony\Component\Validator\Constraints as Assert;

class AddUserOutput
{
    public $id;

    public $email;

    public $password;

    public $firstName;

    public $lastName;

    public $subRoles;

    public $phone;

    public $city;
    public $state;

    public $country;
    public $languages;
    public $handicap;

}
