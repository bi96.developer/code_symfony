<?php

namespace App\DTO\User;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\MappingOperation\Operation;
use App\Service\BaseService;

class UserMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $userMapping = $config->registerMapping('App\Entity\User', 'App\DTO\User\UserOutput')
            ->forMember('picture', function (\App\Entity\User $user) {
                return $this->getUserPicture($user);
            })
            ->forMember('country', function (\App\Entity\User $user) {
                if (!$user->getCountry()) {
                    return null;
                }
                return [
                    'id'   => $user->getCountry()->getId(),
                    'name' => $user->getCountry()->getName(),
                    'slug' => $user->getCountry()->getSlug()
                ];
            });
        $meMapping = $config->registerMapping('App\Entity\User', 'App\DTO\User\MeOutput')
            ->copyFromMapping($userMapping)
            ->forMember('subRoles', function (\App\Entity\User $user) {
                if (!$user->getSubRoles()) {
                    return null;
                }
                $roles = [];
                forEach($user->getSubRoles() as $role) {
                    $roles[] = $role->getName();
                }
                return $roles;
            });
        $userListOutput = $config->registerMapping('App\Entity\User', 'App\DTO\User\UserListOutput')
            ->forMember('picture', function (\App\Entity\User $user) {
                return $this->getUserPicture($user);
            })
            ->forMember('subRoles', function (\App\Entity\User $user) {
                if (!$user->getSubRoles()) {
                    return null;
                }
                $roles = [];
                forEach($user->getSubRoles() as $role) {
                    $roles[] = $role->getName();
                }
                return implode(' / ', $roles);
            })
            ->forMember('country', function (\App\Entity\User $user) {
                if (!$user->getCountry()) {
                    return null;
                }

                return $user->getCountry()->getName();
            });
        $config->registerMapping('App\Entity\User', 'App\DTO\User\AdminListOutput')
            ->copyFromMapping($userListOutput)
            ->forMember('token', function (\App\Entity\User $user) {
                return hash('sha256', $user->getPassword());
            })
            ;   
    }
}
