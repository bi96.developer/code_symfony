<?php

namespace App\DTO\School;

class SchoolListOutput
{
    public $id;
    public $name;
    public $slug;
    public $type;
    public $countries;

    public $address1;
    public $address2;
    public $address3;
    public $zipcode;
    public $city;

    public $logo;
    public $banner;

    public $point;

    public $orgId;

    public $schoolTypeId;
    public $schoolPointPrevious;
}
