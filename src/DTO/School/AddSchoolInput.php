<?php

namespace App\DTO\School;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddSchoolInput
{
    /**
     * @Assert\NotBlank(
     *     message="Name is required"
     * )
     */
    public $name;
    
    /**
     * @TransformAnnotation(
     *     type="entity",
     *     class="App\Entity\SchoolType"
     * )
     */
    public $schoolType;
    
    public $palmes;
    public $league;
    public $address1;
    public $address2;
    public $address3;
    public $zipcode;
    public $city;
    
    public $website;
    public $twitterUrl;
    public $twitterFollower;
    public $instagramUrl;
    public $instagramFollower;
    public $linkedinUrl;
    public $linkedinFollower;
    public $facebookUrl;
    public $facebookFollower;
    public $youtubeUrl;
    public $youtubeFollower;
    public $diplomeoUrl;
    public $diplomeoRating;
    public $diplomeoReview;
    public $letudiantUrl;
    public $letudiantRating;
    public $letudiantReview;
    public $googleRating;
    public $googlePlaceId;
    public $googleReview;
    public $saRating;
    public $saReview;
}
