<?php

namespace App\DTO\School;

class SchoolOutput
{
    public $id;
    public $name;
    public $slug;
    public $type;
    public $countries;

    public $palmes;
    public $league;
    public $address1;
    public $address2;
    public $address3;
    public $zipcode;
    public $city;

    public $website;
    public $twitterUrl;
    public $twitterFollower;
    public $instagramUrl;
    public $instagramFollower;
    public $linkedinUrl;
    public $linkedinFollower;
    public $facebookUrl;
    public $facebookFollower;
    public $youtubeUrl;
    public $youtubeFollower;
    public $diplomeoUrl;
    public $diplomeoRating;
    public $diplomeoRatingOf10;
    public $diplomeoReview;
    public $letudiantUrl;
    public $letudiantRating;
    public $letudiantRatingOf10;
    public $letudiantReview;
    public $googleRating;
    public $googleRatingOf10;
    public $googlePlaceId;
    public $googlePlaceUrl;
    public $googlePlaceLat;
    public $googlePlaceLng;
    public $googleReview;
    public $saRating;
    public $saRatingOf10;
    public $averageRating;
    public $totalReview;
    public $saReview;
    public $logo;
    public $banner;
    public $point;
    public $pointData;
    public $orgId;

    public $schoolTypeId;
    public $schoolType;
    public $schoolLink;

    public $crawlStatus;

    public $createdDate;
    public $updatedDate;

    public $url;
}
