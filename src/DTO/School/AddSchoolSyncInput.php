<?php

namespace App\DTO\School;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddSchoolSyncInput
{
    /**
     * @Assert\NotBlank(
     *     message="Org id is required"
     * )
     */
    public $orgId;
    /**
     * @Assert\NotBlank(
     *     message="Name is required"
     * )
     */
    public $name;

    /**
     * @TransformAnnotation(
     *     type="entity",
     *     class="App\Entity\SchoolType"
     * )
     */
    public $schoolType;
    /**
     * @Assert\NotBlank(
     *     message="Slug is required"
     * )
     */
    public $slug;

    public $address1;


    public $website;
    public $twitterUrl;
    public $instagramUrl;
    public $linkedinUrl;
    public $facebookUrl;
    public $youtubeUrl;
    public $diplomeoUrl;
    public $letudiantUrl;
    public $googlePlaceId;
    public $status;
    public $createdDate;
    public $updatedDate;
    public $logoUrl;
}