<?php

namespace App\DTO\School;

use App\Misc\TransformDataHelper;
use App\Service\CommonService;
use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\MappingOperation\Operation;
use App\Service\BaseService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SchoolMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(
        ContainerInterface $container,
        CommonService $commonService,
        AutoMapperInterface $autoMapper,
        TransformDataHelper $transformDataHelper,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage,
        ParameterBagInterface $params,
        EntityManagerInterface $entityManager,
        UrlHelper $urlHelper,
        RequestStack $requestStack,
        UrlGeneratorInterface $urlGenerator
    ) {
        parent::__construct($container, $commonService, $autoMapper, $transformDataHelper, $validator, $tokenStorage,
            $params, $entityManager, $urlHelper, $requestStack);
        $this->urlGenerator = $urlGenerator;
    }

    public function configure(AutoMapperConfigInterface $config): void
    {
        $schoolMapping = $config->registerMapping('App\Entity\School', 'App\DTO\School\SchoolOutput')
            ->forMember('logo', function (\App\Entity\School $school) {
                return $this->schoolService->getLogo($school);
            })
            ->forMember('banner', function (\App\Entity\School $school) {
                return $this->schoolService->getBanner($school);
            })
            ->forMember('pointData', function (\App\Entity\School $school) {
                if ($school->getPointData() === []) {
                    return null;
                }
                return $school->getPointData();
            })
            ->forMember('reviewData', function (\App\Entity\School $school) {
                if ($school->getReviewData() === []) {
                    return null;
                }
                return $school->getReviewData();
            })
            ->forMember('saRatingOf10', function (\App\Entity\School $school) {
                if (!$school->getSaRating()) {
                    return;
                }

                return ($school->getSaRating() * 10) / 5;
            })
            ->forMember('diplomeoRatingOf10', function (\App\Entity\School $school) {
                if (!$school->getDiplomeoRating()) {
                    return;
                }

                return ($school->getDiplomeoRating() * 10) / 5;
            })
            ->forMember('letudiantRatingOf10', function (\App\Entity\School $school) {
                if (!$school->getLetudiantRating()) {
                    return;
                }

                return ($school->getLetudiantRating() * 10) / 5;
            })
            ->forMember('googleRatingOf10', function (\App\Entity\School $school) {
                if (!$school->getGoogleRating()) {
                    return;
                }

                return ($school->getGoogleRating() * 10) / 5;
            })
            ->forMember('googlePlaceUrl', function (\App\Entity\School $school) {
                if (!$school->getGooglePlaceId()) {
                    return null;
                }
                if (strpos($school->getGooglePlaceId(), 'google.com/maps') === false) {
                    return 'https://www.google.com/maps/search/?api=1&query=Google&query_place_id=' . $school->getGooglePlaceId();
                }
                return $school->getGooglePlaceId();
            })
            ->forMember('countries', function (\App\Entity\School $school) {
                $countries = [];
                foreach ($school->getCountries() as $country) {
                    $countries[] = [
                        'id'   => $country->getId(),
                        'name' => $country->getName(),
                        'slug' => $country->getSlug(),
                    ];
                }
                return $countries;
            })
            ->forMember('schoolTypeId', function (\App\Entity\School $school) {
                return $school->getSchoolType() ? $school->getSchoolType()->getId() : null;
            })
            ->forMember('schoolType', function (\App\Entity\School $school) {
                $schoolType = $school->getSchoolType();
                if (!$schoolType) {
                    return null;
                }
                return [
                    'id'   => $schoolType->getId(),
                    'name' => $schoolType->getName(),
                ];
            })
            ->forMember('schoolLink', Operation::ignore())
            ->forMember('averageRating', function (\App\Entity\School $school) {
                $reviewList = [
                    'Sa',
                    'Diplomeo',
                    'Letudiant',
                    'Google'
                ];

                $listRatingAvailable = [];
                foreach ($reviewList as $reviewIdentity) {
                    $getRatingMethod = 'get' . $reviewIdentity . 'Rating';

                    if ($school->{$getRatingMethod}()) {
                        $listRatingAvailable[] = $school->{$getRatingMethod}();
                    }
                }

                if (!count($listRatingAvailable)) {
                    return null;
                }

                $averageRating = round(array_sum($listRatingAvailable) / count($listRatingAvailable), 2);

                return max($averageRating, 1);
            })
            ->forMember('totalReview', function (\App\Entity\School $school) {
                $reviewList = [
                    'Sa',
                    'Diplomeo',
                    'Letudiant',
                    'Google'
                ];

                $totalReview = 0;
                foreach ($reviewList as $reviewIdentity) {
                    $getRatingMethod = 'get' . $reviewIdentity . 'Review';

                    $totalReview += $school->{$getRatingMethod}() ? $school->{$getRatingMethod}() : 0;
                }

                return $totalReview;
            })
            ->forMember('url', function (\App\Entity\School $school) {
                if (!$school->getSchoolType()) {
                    return;
                }

                return $this->getContainer()->getParameter('base_url') . $this->urlGenerator->generate('school.detail', [
                        'schoolSlug'     => $school->getSlug(),
                        'schoolTypeSlug' => $school->getSchoolType()->getSlug()
                    ]);
            })
        ;

        $config->registerMapping('App\Entity\School', 'App\DTO\School\AdminListOutput')
            ->copyFromMapping($schoolMapping);

        $config->registerMapping('App\Entity\School', 'App\DTO\School\SchoolListOutput')
            ->copyFromMapping($schoolMapping);

        $config->registerMapping('App\Entity\School', 'App\DTO\School\SchoolForEditOutput')
            ->forMember('logo', function (\App\Entity\School $school) {
                return $this->schoolService->getLogo($school);
            })
            ->forMember('banner', function (\App\Entity\School $school) {
                return $this->schoolService->getBanner($school);
            })
            ->forMember('schoolLink', Operation::ignore())
            ->forMember('schoolType', Operation::ignore())
            ->forMember('schoolTypeId', function (\App\Entity\School $school) {
                return $school->getSchoolType() ? $school->getSchoolType()->getId() : null;
            });

        $config->registerMapping('App\Entity\School', 'App\DTO\School\SchoolSearchOutput')
            ->forMember('logo', function (\App\Entity\School $school) {
                return $this->schoolService->getLogo($school);
            })
            ->forMember('url', function (\App\Entity\school $school) {
                $schoolType = $school->getSchoolType();
                if (!$schoolType) {
                    return null;
                }

                return $this->urlGenerator->generate('school.detail', [
                    'schoolSlug'     => $school->getSlug(),
                    'schoolTypeSlug' => $school->getSchoolType()->getSlug()
                ]);
            });
    }
}
