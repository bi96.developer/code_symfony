<?php

namespace App\DTO\School;

class AdminListOutput
{
    public $id;
    public $name;
    public $type;
    public $countries;

    public $palmes;
    public $league;
    public $address1;
    public $address2;
    public $address3;
    public $zipcode;
    public $city;

    public $website;
    public $twitterUrl;
    public $twitterFollower;
    public $instagramUrl;
    public $instagramFollower;
    public $linkedinUrl;
    public $linkedinFollower;
    public $facebookUrl;
    public $facebookFollower;
    public $youtubeUrl;
    public $youtubeFollower;
    public $diplomeoUrl;
    public $diplomeoRating;
    public $diplomeoReview;
    public $letudiantUrl;
    public $letudiantRating;
    public $letudiantReview;
    public $googleRating;
    public $googlePlaceId;
    public $googlePlaceLat;
    public $googlePlaceLng;
    public $googleReview;
    public $saRating;
    public $saReview;
    public $logo;
    public $banner;
    public $point;
    public $pointData;
    public $orgId;
    public $schoolTypeId;
    public $crawlStatus;
    public $status;
    public $isPublished;
}
