<?php

namespace App\DTO\School;

class SchoolForEditOutput
{
    public $id;
    public $name;
    public $slug;
    public $type;
    public $city;
    public $website;
    public $twitterUrl;
    public $twitterFollower;
    public $instagramUrl;
    public $instagramFollower;
    public $linkedinUrl;
    public $linkedinFollower;
    public $facebookUrl;
    public $facebookFollower;
    public $youtubeUrl;
    public $youtubeFollower;
    public $diplomeoUrl;
    public $diplomeoRating;
    public $diplomeoReview;
    public $letudiantUrl;
    public $letudiantRating;
    public $letudiantReview;
    public $googlePlaceId;
    public $googleRating;
    public $googleReview;
    public $logo;
    public $banner;
    public $orgId;
    public $schoolTypeId;
    public $schoolLink;
}
