<?php

namespace App\DTO\School;

class SchoolSearchOutput
{
    public $id;
    public $name;
    public $logo;
    public $rankNumber;
    public $url;
}