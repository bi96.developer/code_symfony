<?php

namespace App\DTO\Redirection;

class AddRedirectionInput
{
    public $targetId;
    public $type;
    public $urlTo;
    public $urlOld;
}