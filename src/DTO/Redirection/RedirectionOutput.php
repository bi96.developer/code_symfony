<?php

namespace App\DTO\Redirection;

class RedirectionOutput
{
    public $id;
    public $type;
}