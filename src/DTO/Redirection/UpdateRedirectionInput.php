<?php

namespace App\DTO\Redirection;

class UpdateRedirectionInput
{
    public $urlTo;
    public $urlOld;
}