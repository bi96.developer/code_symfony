<?php

namespace App\DTO\SchoolPoint;

class SchoolPointOutput
{
    public $schoolId;
    public $orgId;
    public $year;
    public $point;
    public $rankNumber;

}
