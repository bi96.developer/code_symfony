<?php

namespace App\DTO\SchoolPoint;

class SchoolPointListOutput
{
    public $schoolId;
    public $year;
    public $orgId;
    public $point;
    public $rankNumber;
}
