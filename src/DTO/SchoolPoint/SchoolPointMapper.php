<?php

namespace App\DTO\SchoolPoint;

use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\DataType;
use AutoMapperPlus\MappingOperation\Operation;
use App\Service\BaseService;
use AutoMapperPlus\NameConverter\NamingConvention\CamelCaseNamingConvention;
use AutoMapperPlus\NameConverter\NamingConvention\SnakeCaseNamingConvention;

class SchoolPointMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $schoolPointMapping = $config->registerMapping('App\Entity\SchoolPoint',
            'App\DTO\SchoolPoint\SchoolPointOutput')
            ->forMember('schoolId', function (\App\Entity\SchoolPoint $schoolPoint) {
                return $schoolPoint->getSchool()->getId();
            });

        $config->registerMapping('App\Entity\SchoolPoint', 'App\DTO\SchoolPoint\SchoolPointListOutput')
            ->copyFromMapping($schoolPointMapping);

        $schoolPointArrayMapping = $config->registerMapping(DataType::ARRAY, 'App\DTO\SchoolPoint\SchoolPointOutput')
            ->withNamingConventions(
                new SnakeCaseNamingConvention(),
                new CamelCaseNamingConvention()
            );

        $config->registerMapping(DataType::ARRAY, 'App\DTO\SchoolPoint\SchoolPointListOutput')
            ->copyFromMapping($schoolPointArrayMapping);
    }
}
