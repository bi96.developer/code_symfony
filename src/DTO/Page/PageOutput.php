<?php

namespace App\DTO\Page;

class PageOutput
{
    public $title;
    public $slug;
    public $content;
    public $createdDate;
    public $updatedDate;
}
