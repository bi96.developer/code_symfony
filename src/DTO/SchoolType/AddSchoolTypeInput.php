<?php

namespace App\DTO\SchoolType;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddSchoolTypeInput
{
    /**
     * @Assert\NotBlank(
     *     message="Id is required"
     * )
     */
    public $id;
    /**
     * @Assert\NotBlank(
     *     message="Name is required"
     * )
     */
    public $name;
    /**
     * @Assert\NotBlank(
     *     message="Slug is required"
     * )
     */
    public $slug;
    public $status;
    public $createdDate;
    public $updatedDate;
}