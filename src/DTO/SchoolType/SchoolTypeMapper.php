<?php

namespace App\DTO\SchoolType;

use App\Misc\TransformDataHelper;
use App\Service\CommonService;
use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\MappingOperation\Operation;
use App\Service\BaseService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SchoolTypeMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(
        ContainerInterface $container,
        CommonService $commonService,
        AutoMapperInterface $autoMapper,
        TransformDataHelper $transformDataHelper,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage,
        ParameterBagInterface $params,
        EntityManagerInterface $entityManager,
        UrlHelper $urlHelper,
        RequestStack $requestStack,
        UrlGeneratorInterface $urlGenerator
    ) {
        parent::__construct($container, $commonService, $autoMapper, $transformDataHelper, $validator, $tokenStorage,
            $params, $entityManager, $urlHelper, $requestStack);
        $this->urlGenerator = $urlGenerator;
    }

    public function configure(AutoMapperConfigInterface $config): void
    {
        $schoolTypeMapping = $config->registerMapping('App\Entity\SchoolType', 'App\DTO\SchoolType\SchoolTypeOutput')
            ->forMember('url', function (\App\Entity\SchoolType $schoolType) {
                return $this->getContainer()->getParameter('base_url') . $this->urlGenerator->generate('schoolType.detail',
                        [
                            'schoolTypeSlug' => $schoolType->getSlug(),
                        ]);
            });

        $config->registerMapping('App\Entity\SchoolType', 'App\DTO\SchoolType\SchoolTypeListOutput')
            ->copyFromMapping($schoolTypeMapping);
    }
}
