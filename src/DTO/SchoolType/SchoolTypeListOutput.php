<?php

namespace App\DTO\SchoolType;

class SchoolTypeListOutput
{
    public $id;
    public $name;
    public $description;
    public $slug;
    public $status;
    public $createdDate;
    public $updatedDate;
    public $url;
}
