<?php

namespace App\DTO\Question;

class QuestionListOutput
{
    public $id;
    public $name;
    public $content;
    public $status;
}