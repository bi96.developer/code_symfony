<?php

namespace App\DTO\Question;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class QuestionMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {

        $config->registerMapping('App\Entity\Question', 'App\DTO\Question\QuestionListOutput')
            ->forMember('schoolTypeId', function (\App\Entity\question $question) {
                return $question->getSchoolType() ? $question->getSchoolType() : null;
            });
    }
}