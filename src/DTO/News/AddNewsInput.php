<?php

namespace App\DTO\News;

use Symfony\Component\Validator\Constraints as Assert;

class AddNewsInput
{
    /**
     * @Assert\NotBlank(
     *     message="Title is required"
     * )
     */
    public $title;
    public $description;
    public $content;

    public $slug;

    /**
     * @Assert\File(
     *     maxSize="10m",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/png"},
     *     mimeTypesMessage = "File upload invalid"
     * )
     */
    public $thumbFile;

    public $metaTitle;
    public $metaDescription;

    public $status = 1;
}