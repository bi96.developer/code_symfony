<?php

namespace App\DTO\News;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class NewsMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $newsListOutput = $config->registerMapping('App\Entity\News', 'App\DTO\News\NewsListOutput')
            ->forMember('thumb', function (\App\Entity\News $news) {
                return $this->newsService->getThumbNews($news);
            });

        $config->registerMapping('App\Entity\News', 'App\DTO\News\NewsOutput')
            ->copyFromMapping($newsListOutput);

        $config->registerMapping('App\Entity\News', 'App\DTO\News\NewsListOutputAdmin')
            ->copyFromMapping($newsListOutput);

        $config->registerMapping('App\Entity\News', 'App\DTO\News\NewsOutputAdmin')
            ->copyFromMapping($newsListOutput);
    }
}