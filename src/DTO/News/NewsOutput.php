<?php

namespace App\DTO\News;

class NewsOutput
{
    public $id;
    public $title;
    public $description;
    public $thumb;
    public $content;
    
    public $metaTitle;
    public $metaDescription;

    public $createdDate;
    public $updatedDate;
}