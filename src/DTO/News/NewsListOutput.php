<?php

namespace App\DTO\News;

class NewsListOutput
{
    public $id;
    public $title;
    public $description;
    public $thumb;
    public $slug;

    public $createdDate;
    public $updatedDate;
}