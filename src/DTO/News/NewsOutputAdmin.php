<?php

namespace App\DTO\News;

class NewsOutputAdmin
{
    public $id;
    public $title;
    public $description;
    public $content;
    public $thumb;
    public $metaTitle;
    public $metaDescription;

    public $status;
}