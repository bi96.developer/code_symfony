<?php

namespace App\DTO\News;

class NewsListOutputAdmin
{
    public $id;
    public $title;
    public $description;
    public $status;
    public $thumb;

    public $createdDate;
    public $updatedDate;
}