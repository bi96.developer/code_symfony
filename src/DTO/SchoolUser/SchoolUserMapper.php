<?php
namespace App\DTO\SchoolUser;
use App\Service\BaseService;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\MappingOperation\Operation;

class SchoolUserMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping('App\Entity\SchoolUser', 'App\DTO\SchoolUser\SchoolUserOutput')
            ->forMember('school', Operation::mapTo('App\DTO\School\SchoolOutput'))
            ->forMember('user', Operation::mapTo('App\DTO\User\UserListOutput'));
        $config->registerMapping('App\Entity\SchoolUser', 'App\DTO\SchoolUser\AdminListOutput')
            ->forMember('school', function (\App\Entity\SchoolUser $schoolUser) {
                $school = $schoolUser->getBroker();
                $countries = $school->getCountries();
                return [
                    'id' => $school->getId(),
                    'name' => $school->getName(),
                    'logo' => $this->schoolService->getLogo($school),
                    'country' => $countries[0] ? $countries[0]->getName() : '',
                    'website' => $school->getWebsite(),
                ];
            })
            ->forMember('telephone', function (\App\Entity\SchoolUser $schoolUser) {
                if(is_null($schoolUser->getTelephone())) {
                    return $schoolUser->getUser()->getPhone();
                }
                return $schoolUser->getTelephone();
            })
            ->forMember('user', Operation::mapTo('App\DTO\User\UserListOutput'));
    }
}
