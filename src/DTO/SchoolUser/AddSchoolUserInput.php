<?php

namespace App\DTO\SchoolUser;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddSchoolUserInput
{
    /**
     * @Assert\NotBlank(
     *     message = "User can not be blank"
     * )
     *
     * @TransformAnnotation(
     *     type="entity",
     *     field="name",
     *     class="App\Entity\User"
     * )
     */
    public $user;
    
    /**
     * @Assert\NotBlank(
     *     message = "Broker can not be blank"
     * )
     *
     * @TransformAnnotation(
     *     type="entity",
     *     field="name",
     *     class="App\Entity\School"
     * )
     */
    public $school;

    public $firstName;
    public $lastName;
    public $email;
    public $telephone;
    public $file1;
    public $file2;
    public $role;
    public $gender;
}
