<?php

namespace App\DTO\SchoolUser;

class AdminListOutput
{
    public $school;
    public $user;
    public $createdDate;
    public $id;
    public $role;
    public $gender;
    public $firstName;
    public $lastName;
    public $email;
    public $telephone;
}
