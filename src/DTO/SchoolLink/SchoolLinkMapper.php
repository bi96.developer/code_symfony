<?php

namespace App\DTO\SchoolLink;

use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\DataType;
use AutoMapperPlus\MappingOperation\Operation;
use App\Service\BaseService;
use AutoMapperPlus\NameConverter\NamingConvention\CamelCaseNamingConvention;
use AutoMapperPlus\NameConverter\NamingConvention\SnakeCaseNamingConvention;

class SchoolLinkMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(DataType::ARRAY, 'App\Entity\SchoolLink');

        $schoolLinkMapper = $config->registerMapping(DataType::ARRAY, 'App\DTO\SchoolLink\SchoolLinkOutput')
            ->withNamingConventions(
                new SnakeCaseNamingConvention(),
                new CamelCaseNamingConvention()
            );

        $config->registerMapping(DataType::ARRAY, 'App\DTO\SchoolLink\SchoolLinkListOutput')
            ->copyFromMapping($schoolLinkMapper);
    }
}