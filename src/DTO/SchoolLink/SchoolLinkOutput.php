<?php

namespace App\DTO\SchoolLink;

class SchoolLinkOutput
{
    public $schoolId;
    public $orgId;
    public $meilleursMasters;
    public $meilleursGrandesEcoles;
    public $meilleuresLicenses;
    public $eduniversalRanking;
    public $bestMasters;
    public $saMasters;
    public $saMba;
    public $saBachelors;
    public $saGrandesEcoles;
}
