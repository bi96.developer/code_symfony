<?php

namespace App\DTO\Post;

class PostListLanguageOutput
{
    public $id;
    public $name;
    public $code;
}
