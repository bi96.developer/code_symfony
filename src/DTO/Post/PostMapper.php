<?php

namespace App\DTO\Post;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class PostMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $postOutput = $config->registerMapping('App\Entity\Post', 'App\DTO\Post\PostOutput')
            ->forMember('user', function (\App\Entity\Post $post) {
                $coursePostType = [
                    $this->POST_TYPE_COURSE_POST
                ];
                if($this->requestService->isBackend) {
                    return $this->userService->getUserMapped($post, false, false, null, false, true);
                }
                if(in_array($post->getType(), $coursePostType)) {
                    return null;
                }
                return $this->userService->getUserMapped($post);
            })
            ->forMember('language', function (\App\Entity\Post $post) {
                $language = $post->getLanguage();
                return [
                    'id' => $language->getId(),
                    'name' => $language->getName()
                ];
            })
            ->forMember('school', function (\App\Entity\Post $post) {
                if (!$post->getSchool()) {
                    return null;
                }
                $school = $post->getSchool();
                $returnData = [
                  'id' => $school->getId(),
                  'name' => $school->getName(),
                  'countries' => [],
                ];
                forEach($school->getCountries() as $country) {
                    $returnData['countries'][] = [
                        'id' => $country->getId(),
                        'name' => $country->getName(),
                        'slug' => $country->getSlug(),
                    ];
                }
                return $returnData;
            })
            ->forMember('canUpdate', function (\App\Entity\Post $post) {
                if(!$this->isLoggedIn()) return false;
                return $this->postService->canUpdatePost(
                    $post,
                    false
                );
            })
            ->forMember('canDelete', function (\App\Entity\Post $post) {
                if(!$this->isLoggedIn()) return false;
                return $this->postService->canDeletePost($post, false);
            });
        $config->registerMapping('App\Entity\Post', 'App\DTO\Post\PostOutputAdmin')
            ->copyFromMapping($postOutput)
            ->forMember('school', function (\App\Entity\Post $post) {
                if (!$post->getSchool()) {
                    return null;
                }
                $school = $post->getSchool();
                $returnData = [
                  'id' => $school->getId(),
                  'name' => $school->getName(),
                  'countries' => [],
                  'logo' => $this->schoolService->getLogo($school),
                ];
                forEach($school->getCountries() as $country) {
                    $returnData['countries'][] = [
                        'id' => $country->getId(),
                        'name' => $country->getName(),
                        'slug' => $country->getSlug(),
                    ];
                }
                return $returnData;
            });
    }
}
