<?php

namespace App\DTO\Post;

use App\DTO\Validator\ContainsVideoUrl;
use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class UpdatePostInput
{
    /**
     * @Assert\NotBlank(
     *     message="Type is required"
     * )
     */
    public $type;
    
    /**
     * @TransformAnnotation(
     *     class="App\Entity\Course",
     *     type="entity"
     * )
     */
    public $course;

    /**
     * @Assert\NotBlank(
     *     message="Content is required"
     * )
     * @TransformAnnotation(
     *     type="strip_tags"
     * )
     */
    public $content;

    /**
     * @TransformAnnotation(
     *     class="App\Entity\Language",
     *     type="entity"
     * )
     */
    public $language;

    /**
     * @Assert\All(
     *     @Assert\File(
     *          maxSize="10m",
     *          mimeTypes = {"image/jpg", "image/jpeg", "image/png"},
     *          mimeTypesMessage = "Please upload a valid photo"
     *      )
     * )
     */
    public $photoFiles;

    /**
     * @Assert\All(
     *      @ContainsVideoUrl
     * )
     */
    public $youtubes;
    
    public $title;
    
    public $reviewData;
    
    
    /**
     * @TransformAnnotation(
     *     type="collectionEntity",
     *     class="App\Entity\Tag"
     * )
     */
    public $tags;
}
