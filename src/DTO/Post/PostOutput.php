<?php

namespace App\DTO\Post;

class PostOutput
{
    public $id;
    public $content;
    
    public $user;
    public $school;
    public $language;

    public $createdDate;
    public $updatedDate;
    public $reviewData;
    public $type;
    public $title;
    public $canUpdate;
    public $canDelete;
}
