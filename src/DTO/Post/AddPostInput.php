<?php

namespace App\DTO\Post;

use App\DTO\Validator\ContainsVideoUrl;
use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddPostInput
{
    /**
     * @Assert\NotBlank(
     *     message="Type is required"
     * )
     */
    public $type;
    
    /**
     * @TransformAnnotation(
     *     class="App\Entity\School",
     *     type="entity"
     * )
     */
    public $school;
    
    /**
     * @TransformAnnotation(
     *     class="App\Entity\User",
     *     type="entity"
     * )
     */
    public $user;

    /**
     * @TransformAnnotation(
     *     type="strip_tags"
     * )
     */
    public $content;

    /**
     * @TransformAnnotation(
     *     class="App\Entity\Language",
     *     type="entity"
     * )
     */
    public $language;
    
    public $title;
    
    public $reviewData;
    
}
