<?php

namespace App\DTO\Post;

class PostOutputAdmin
{
    public $id;
    public $content;
    
    public $user;
    public $school;
    public $language;

    public $createdDate;
    public $updatedDate;
    public $reviewData;
    public $reviewRating;
    public $type;
    public $title;
    public $canUpdate;
    public $canDelete;
}
