<?php

namespace App\DTO\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsUnique extends Constraint
{
    public $message = 'This value is already used.';

    public $entityClass;

    public $field;
}
