<?php

namespace App\DTO\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsVideoUrl extends Constraint
{
    public $message = 'The video url invalid.';
}
