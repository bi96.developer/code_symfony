<?php

namespace App\DTO\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsVideoUrlValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $youtubeRegex = "/^(http|https):\/\/(?:www\.)?(?:youtube.com|youtu.be)\/((?:watch\?(?=.*v=([\w\-]+))(?:\S+)?|([\w\-]+))|([\w\-\?\=]+))$/";

        preg_match($youtubeRegex, $value, $matches);

        $matches = array_filter($matches, function($var) {
            return($var !== '');
        });
        
        if (sizeof($matches) >= 3) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }
}
