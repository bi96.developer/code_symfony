<?php

namespace App\DTO\Report;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddReportInput
{
    /**
     * @Assert\NotBlank(
     *     message="EntityName is required"
     * )
     */
    public $entityName;

    /**
     * @Assert\NotBlank(
     *     message="EntityId is requried"
     * )
     */
    public $entityId;
    
    public $category;
    public $reason;
    public $status;
    /**
     * @TransformAnnotation(
     *     type="entity",
     *     class="App\Entity\User"
     * )
     */
    public $user;
}
