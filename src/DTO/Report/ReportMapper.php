<?php

namespace App\DTO\Report;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class ReportMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping('App\Entity\Report', 'App\DTO\Report\ReportOutput')
            ->forMember('user', function (\App\Entity\Report $report) {
                return $this->userService->getUserMapped($report);
            })
            ->forMember('handler', function (\App\Entity\Report $report) {
                $user = $report->getHandledBy();
                if(!$user) return null;
                return [
                    'name' => $user->getFirstName() . ' ' . $user->getLastName()
                ];
            });
        $config->registerMapping('App\Entity\User', 'App\DTO\Report\ReportEntityOutput')
            ->forMember('name', function (\App\Entity\User $user) {
                return $user->getFirstName() . ' ' . $user->getLastName();
            })
            ->forMember('picture', function (\App\Entity\User $user) {
                return $this->getUserPicture($user);
            });
        $config->registerMapping('App\Entity\Post', 'App\DTO\Report\ReportEntityOutput')
            ->forMember('name', function (\App\Entity\Post $post) {
                $user = $post->getUser();
                return $user->getFirstName() . ' ' . $user->getLastName();
            })
            ->forMember('postType', function (\App\Entity\Post $post) {
                return $post->getType();
            })
            ->forMember('ownerId', function (\App\Entity\Post $post) {
                return $post->getUser()->getId();
            });
        $config->registerMapping('App\Entity\Comment', 'App\DTO\Report\ReportEntityOutput')
            ->forMember('name', function (\App\Entity\Comment $comment) {
                $user = $comment->getUser();
                return $user->getFirstName() . ' ' . $user->getLastName();
            })
            ->forMember('postId', function (\App\Entity\Comment $comment) {
                return $comment->getEntityId();
            })
            ->forMember('ownerId', function (\App\Entity\Comment $comment) {
                return $comment->getUser()->getId();
            });
    }
}
