<?php

namespace App\DTO\Report;

class ReportOutput
{
    public $id;
    public $entityName;
    public $entityId;
    public $category;
    public $reason;
    public $status;
    public $user;
    public $handler;
    public $createdDate;
    public $updatedDate;
    
}
