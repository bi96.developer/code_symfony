<?php

namespace App\DTO\Report;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class ReportEntityOutput
{
    public $name;
    public $email;
    public $text;
    public $picture;
    public $id;
    public $ownerId;
    public $ownerName;
    public $postId;
    public $postType;
}
