<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210920043945 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE school_link ADD sa_masters VARCHAR(500) DEFAULT NULL, ADD sa_mba VARCHAR(500) DEFAULT NULL, ADD sa_bachelors VARCHAR(500) DEFAULT NULL, ADD sa_grandes_ecoles VARCHAR(500) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE school_link DROP sa_masters, DROP sa_mba, DROP sa_bachelors, DROP sa_grandes_ecoles');
    }
}
