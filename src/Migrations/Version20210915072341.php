<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210915072341 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE school_link (school_id INT NOT NULL, org_id INT NOT NULL, meilleurs_masters VARCHAR(500) DEFAULT NULL, meilleurs_grandes_ecoles VARCHAR(500) DEFAULT NULL, meilleures_licenses VARCHAR(500) DEFAULT NULL, eduniversal_ranking VARCHAR(500) DEFAULT NULL, best_masters VARCHAR(500) DEFAULT NULL, PRIMARY KEY(school_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE school_link');
    }
}
