<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211227043433 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAWareTrait;

    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema) : void
    {
        parent::postUp($schema);

        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $this->conn = $entityManager->getConnection();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $rootDir = $this->container->getParameter('kernel.root_dir');
        $spreadsheet = $reader->load($rootDir . "/Migrations/sqls/SchoolComplete.xlsx");
        $sheet = $spreadsheet->getSheet(0);
        $changedRows = array_merge(
            $this->getSheetChangedRows($spreadsheet->getSheet(0))
        );
        //echo print_r($changedRows);exit;
        foreach($changedRows as $row) {
            echo $row . '
';
            $query = $this->conn->prepare($row);
            $query->execute();
        }
    }
    public function getSheetChangedRows($sheet) {
        $cellMap = [
            'C' => ['website', 'website'],
            'F' => ['twitter_url', 'twitterUrl', ['twitter_follower']],
            'G' => ['instagram_url', 'instagramUrl', ['instagram_follower']],
            'D' => ['linkedin_url', 'linkedinUrl', ['linkedin_follower']],
            'E' => ['facebook_url', 'facebookUrl', ['facebook_follower']],
            'H' => ['youtube_url', 'youtubeUrl', ['youtube_follower']],
            'I' => ['google_place_id', 'googlePlaceId', ['google_rating', 'google_review']],
            'J' => ['diplomeo_url', 'diplomeoUrl', ['diplomeo_rating', 'diplomeo_review']],
            'K' => ['letudiant_url', 'letudiantUrl', ['letudiant_rating', 'letudiant_review']],
        ];
        $changedRows = [];
        foreach ($sheet->getRowIterator() as $indexRow => $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);
            $data = [];
            $id = null;
            $columns = [];
            if($indexRow === 1) continue;
            foreach ($cellIterator as $index => $cell) {
                if($index === 'A') {
                    $id = $cell->getValue();
                }
                if($index === 'B') {
                    $data[] = "name = " . $this->conn->quote($cell->getValue());
                }
                if(in_array($index, array_keys($cellMap))) {
                    if(is_null($cell->getValue())
                      || strtolower($cell->getValue()) === 'x'
                    ) {
                        $data[] = $cellMap[$index][0] . " = null";
                        foreach($cellMap[$index][2] as $related) {
                            $data[] = $related . " = null";
                        }
                    } else {
                        $data[] = $cellMap[$index][0] . " = " . $this->conn->quote($cell->getValue());
                        $columns[] = $cellMap[$index][1];
                    }
                }
            }
            if(count($data) > 0) {
                $setQuery = implode(',', $data) . ",update_data = '" . implode(',', $columns) . "', crawl_status = 'crawlable', updated_date = NOW()";
                $changedRows[] = "Update school set " . $setQuery . " where org_id = " . $id . ";";
            }
        }
        return $changedRows;
    }
}
