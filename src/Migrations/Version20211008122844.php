<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\YearRanking;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211008122844 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAWareTrait;

    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE year_ranking (year INT AUTO_INCREMENT NOT NULL, previous_year INT DEFAULT NULL, status SMALLINT DEFAULT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, PRIMARY KEY(year)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE year_ranking');
    }

    public function postUp(Schema $schema) : void
    {
        parent::postUp($schema);

        $entityManager = $this->container->get('doctrine.orm.entity_manager');

        $entityManager->getConnection()->exec(
            file_get_contents($this->container->getParameter('kernel.root_dir') . '/Migrations/sqls/year_ranking.sql')
        );
    }
}
