<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Symfony\Component\Finder\Finder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

use App\Entity\User;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210831083446 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAWareTrait;
    
    private $em;
    
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        
    }

    public function down(Schema $schema) : void
    {
        
    }
    
    public function postUp(Schema $schema) : void
    {
        parent::postUp($schema);
        
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        
        $this->insertRole();
        
        $this->insertUser();
        
    }
    
    
    private function insertRole() {
        $query = $this->em->getConnection()->prepare("
            INSERT INTO role (id, name) VALUES
            (1,	'Administrator'),
            (2,	'User');");
        $query->execute();
    }
  
    private function insertUser() {
        $passwordEncoder = $this->container->get("security.password_encoder");
        // create admin account
        $admins = [
            ['Super', 'Admin', getenv('ADMIN_USERNAME')],
        ];
        
        $conn = $this->em->getConnection();
        $roleId = $conn->fetchColumn('SELECT id FROM role WHERE name = ?', ['Administrator'], 0);
        forEach($admins as $admin) {
            $user = new User();
            $user->setFirstName($admin[0]);
            $user->setLastName($admin[1]);
            $user->setEmail($admin[2]);
            $user->setAuthPermitted(true);
            $conn->insert(
                'user', 
                [
                    'first_name' => $admin[0], 
                    'last_name' => $admin[1],
                    'email' => $admin[2],
                    'password' => $passwordEncoder->encodePassword($user, getenv('ADMIN_PASSWORD')),
                    'auth_permitted' => true,
                    'created_date' => '2019-09-17 03:36:47',
                    'updated_date' => '2019-09-17 03:36:47'
                ]
            );
            
        }
        $conn->insert(
            'user_role', 
            [
                'role_id' => 1,
                'user_id' => 1,
            ]
        );
        
    }
    
}
