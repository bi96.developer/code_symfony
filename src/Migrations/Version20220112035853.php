<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112035853 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE school_type ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE school ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE sync_log ADD data_error LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE school_type DROP deleted_at');
        $this->addSql('ALTER TABLE school DROP deleted_at');
        $this->addSql('ALTER TABLE sync_log DROP data_error');
    }
}