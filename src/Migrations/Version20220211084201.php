<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220211084201 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAWareTrait;
    
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
    
    public function preUp(Schema $schema) : void
    {
        parent::preUp($schema);
        $conn = $this->container
                ->get('doctrine.orm.entity_manager')
                ->getConnection();
        $schools = array_map('str_getcsv', file($this->container->getParameter('kernel.project_dir') . '/src/Migrations/sqls/LinkedinV2.csv'));
        $schools[0][0] = 'id';
        array_walk($schools, function(&$a) use ($schools) {
          $a = array_combine($schools[0], $a);
        });
        array_shift($schools);
        forEach($schools as $school) {
            if(empty($school['Followers']) || empty($school['id'])) {
                continue;
            }
            $conn->update(
                'school', 
                [
                    'linkedin_follower' => (int) str_replace(',', '', $school['Followers']),
                    'updated_date' => (new \Datetime('now'))->format('Y-m-d H:i:s')
                ],
                [
                    'org_id' => $school['id']
                ]
            );
        }
    }
}
