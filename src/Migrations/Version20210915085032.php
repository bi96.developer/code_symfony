<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210915085032 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE school_point (year INT NOT NULL, school_id INT NOT NULL, org_id INT DEFAULT NULL, point DOUBLE PRECISION NOT NULL, rank_number INT DEFAULT NULL, diplomeo_rating DOUBLE PRECISION DEFAULT NULL, diplomeo_review INT DEFAULT NULL, letudiant_rating DOUBLE PRECISION DEFAULT NULL, letudiant_review INT DEFAULT NULL, google_rating DOUBLE PRECISION DEFAULT NULL, google_review INT DEFAULT NULL, sa_rating DOUBLE PRECISION DEFAULT NULL, sa_review INT DEFAULT NULL, point_data JSON DEFAULT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_695F453EC32A47EE (school_id), PRIMARY KEY(school_id, year)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE school_point ADD CONSTRAINT FK_695F453EC32A47EE FOREIGN KEY (school_id) REFERENCES school (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE school_point');
    }
}
