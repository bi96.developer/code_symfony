<?php

namespace App\Misc;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\MappingOperation\Operation;
use AutoMapperPlus\NameConverter\NamingConvention\CamelCaseNamingConvention;
use AutoMapperPlus\NameConverter\NamingConvention\SnakeCaseNamingConvention;

class AutoMapperConfig implements AutoMapperConfiguratorInterface
{
    /**
     * Use this method to register your mappings.
     *
     * @param AutoMapperConfigInterface $config
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        // Default auto mapper config
        $config->getOptions()->ignoreNullProperties();
        $config->getOptions()->createUnregisteredMappings();
        $config->getOptions()->dontSkipConstructor();

        $config->registerMapping('App\Entity\Country', 'App\DTO\Country\CountryListOutput')
            ->forMember('zone', Operation::ignore());

    }
}
