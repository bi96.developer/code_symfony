<?php

namespace App\Misc;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ImageUrlHelper
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(
        ContainerInterface $container
    ) {
        $this->container = $container;
    }

    public function getSchoolLogoUrl($schoolLogo)
    {
        if ($schoolLogo) {
            return $schoolLogo;
        }

        return $this->container->getParameter('static_url') . '/assets/images/default_logo.png';
    }

    public function getSchoolTypeImageUrl()
    {
        return $this->container->getParameter('static_url') . '/assets/images/logo-main.png';
    }

    public function getNewsImageUrl()
    {
        return $this->container->getParameter('static_url') . '/assets/images/logo-main.png';
    }

    public function getLogoUrl()
    {
        return $this->container->getParameter('static_url') . '/assets/images/logo-main.png';
    }
}