<?php

namespace App\Misc;

class ArrayUtil
{
    public static function keyBy($array, $key = 'id')
    {
        $newArray = [];

        foreach ($array as $item) {
            if (is_array($item)) {
                $newArray[$item[$key]] = $item;
            } else {
                $newArray[$item->{$key}] = $item;
            }
        }

        return $newArray;
    }

    public static function groupBy($array, $key = 'id')
    {
        $newArray = [];

        foreach ($array as $item) {
            $keyValue = $item[$key];
            if (!array_key_exists($keyValue, $newArray)) {
                $newArray[$keyValue] = [];
            }

            $newArray[$keyValue][] = $item;
        }

        return $newArray;
    }
}
