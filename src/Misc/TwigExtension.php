<?php

namespace App\Misc;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('splitToTwoColumns', [$this, 'splitToTwoColumns']),
            new TwigFunction('arrayGet', [$this, 'arrayGet']),
            new TwigFunction('objectGet', [$this, 'arrayGet']),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('formatNumber', [$this, 'formatNumber']),
            new TwigFilter('roundPoint', [$this, 'roundPoint']),
        ];
    }

    public function splitToTwoColumns($array)
    {
        $count = count($array);

        return [
            array_slice($array, 0, ceil($count / 2)),
            array_slice($array, ceil($count / 2), $count)
        ];
    }

    public function roundPoint($point)
    {
        if (!$point) {
            return $point;
        }

        return sprintf(
            "%.3f",
            floor((float)$point * 1000) / 1000
        );
    }

    public function arrayGet($array, $key, $default = null)
    {
        if (!is_array($array) && !is_object($array)) {
            return $default;
        }

        if (is_null($key)) {
            return $array;
        }

        if (is_array($array) && array_key_exists($key, $array)) {
            return $array[$key];
        } elseif (is_object($array) && property_exists($array, $key)) {
            return $array->{$key};
        }


        foreach (explode('.', $key) as $segment) {
            if (is_array($array) && array_key_exists($segment, $array)) {
                $array = $array[$segment];
            } elseif (is_object($array) && property_exists($array, $segment)) {
                $array = $array->{$segment};
            } else {
                return $default;
            }
        }

        return $array;
    }

    public function formatNumber($number)
    {
        return number_format($number, 0, '.', ' ');
    }
}
