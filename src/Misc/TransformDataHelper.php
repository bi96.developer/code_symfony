<?php

namespace App\Misc;

use App\Annotation\TransformAnnotation;
use AutoMapperPlus\AutoMapperInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;

class TransformDataHelper
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AutoMapperInterface
     */
    private $autoMapper;

    public function __construct(
        EntityManagerInterface $entityManager,
        AutoMapperInterface $autoMapper
    ) {
        $this->entityManager = $entityManager;
        $this->autoMapper = $autoMapper;
    }

    public function transform($object, $inputProperties)
    {
        $reflectionClass = new \ReflectionClass(get_class($object));

        $properties = $reflectionClass->getProperties();
        $reader = new AnnotationReader();

        foreach ($properties as $property) {
            // @todo: check property readable
            $propertyName = $property->getName();
            $propertyValue = $object->{$propertyName};
            if($propertyValue === "") {
                $propertyValue = null;
            }
            /*
            if(!in_array($propertyName, $inputProperties)) {
                unset($object->{$propertyName});
                continue;
            }
            */

            /** @var TransformAnnotation $transformAnnotation */
            $transformAnnotation = $reader->getPropertyAnnotation($property, TransformAnnotation::class);

            if (!$transformAnnotation) {
                continue;
            }

            $newValue = $this->transformValue(
                $propertyValue,
                $transformAnnotation,
                $object
            );

            $object->{$propertyName} = $newValue;
        }
    }

    private function transformValue($value, TransformAnnotation $annotation, $object = null)
    {
        switch ($annotation->type) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'string':
                return (string)$value;
            case 'bool':
            case 'boolean':
                return (bool)$value;
            case 'date':
                return is_null($value) ? null : \DateTime::createFromFormat('Y-m-d', $value);
            case 'float':
                return (float) $value;
            case 'strip_tags':
                // 9 is directory interview
                // 8 is discussion
                $isPost = isset($object->type) && ($object->type == 9 || $object->type == 8);
                $isRichText = isset($annotation->isRichText) && $annotation->isRichText === 'true';
                $containsHTML = $value != strip_tags($value);
                if(($isPost || $isRichText) && $containsHTML) {
                    return $this->strip_tags_secured($value, [
                        'a', 'p', 'strong', 'u',
                        'h1', 'h2', 'h3',
                        'ul', 'li', 'ol',
                        'wrapquestion',
                        'question1', 'question2', 'question3',
                        'question4', 'question5', 'question6',
                        'question7', 'directorname', 'directorposition',
                        'img', 'iframe'
                    ]);
                } else {
                    return strip_tags($value);
                }
            case 'entity':
                return $this->castValueToEntity($value, $annotation->class);
            case 'collectionEntity':
                return $this->castValueToCollectionEntity($value, $annotation);
            case 'collectionObject':
                return $this->castValueToObject($value, $annotation->class);
            case 'imageBase64':
                return $this->caseValueToImgBase64($value);
        }

        return $value;
    }

    private function castValueToEntity($value, $class)
    {
        if (!$value) {
            return null;
        }
        
        if ($value === 'isNull') {
            return $value;
        }
        
        try {
            return $this->entityManager->getReference($class, $value);
        } catch (\Exception $exception) {
            //
        }

        return $value;
    }

    private function castValueToCollectionEntity($value, $annotation)
    {
        if (!is_array($value)) {
            return null;
        }

        try {
            $repository = $this->entityManager->getRepository($annotation->class);
            if(isset($annotation->field)) {
                $filter = [];
                $filter[$annotation->field] = $value;
                $entity = $repository->findBy($filter);
            } else {
                $entity = $repository->findBy([
                    'id' => $value
                ]);
            }
            

            return $entity;
        } catch (\Exception $exception) {
            //
        }

        return $value;
    }

    private function castValueToObject($value, $class)
    {
        if (!class_exists($class)) {
            return $value;
        }

        $newArr = [];
        foreach ($value as $key => $attributes) {
            $newArr[$key] = $this->autoMapper->map($attributes, $class);
        }

        return $newArr;
    }

    private function caseValueToImgBase64($value)
    {
        if (!$value) {
            return null;
        }

        $pureBase64 = Base64FileExtractor::extractBase64String($value);

        if (!$pureBase64) {
            return null;
        }

        return new UploadedBase64File($pureBase64, 'image');
    }
    
    function strip_tags_secured(
        $html_str, 
        $allowed_tags=[], 
        $allowed_attrs=['href', 'alt', 'src', 'frameborder', 'allowfullscreen']
    ) {
      $xml = new \DOMDocument();
      //Suppress warnings: proper error handling is beyond scope of example
        libxml_use_internal_errors(true);
      if (!strlen($html_str)){return false;}
      $html_str = mb_convert_encoding($html_str, 'HTML-ENTITIES', "UTF-8");
      if ($xml->loadHTML($html_str, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD)){
        foreach ($xml->getElementsByTagName("*") as $tag){
          if (!in_array($tag->tagName, $allowed_tags)){
            $tag->parentNode->removeChild($tag);
          }else{
            foreach ($tag->attributes as $attr){
              if (!in_array($attr->nodeName, $allowed_attrs)){
                $tag->removeAttribute($attr->nodeName);
              }
            }
          }
        }
      }
      return trim($xml->saveHTML());
    }
}
