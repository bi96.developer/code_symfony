<?php

namespace App\Repository;

use App\Entity\Question;

/**
 * @method Question|null find($id, $lockMode = null, $lockVersion = null)
 * @method Question|null findOneBy(array $criteria, array $orderBy = null)
 * @method Question[]    findAll()
 * @method Question[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionRepository extends BaseRepository
{
    public function getQuestion($dtoClass)
    {
        $question = $this->findBy([
            'status' => Question::STATUS_ACTIVE,
            'schoolType' => null,
        ]);

        return (array) $this->autoMapper->mapMultiple($question, $dtoClass);
    }

    public function getQuestionOfSchoolType($schoolTypeId, $dtoClass)
    {
        $question = $this->findBy([
            'status' => Question::STATUS_ACTIVE,
            'schoolType' => $schoolTypeId,
        ]);

        return (array) $this->autoMapper->mapMultiple($question, $dtoClass);
    }
}
