<?php

namespace App\Repository;

use App\Entity\SchoolPoint;
use App\Misc\ArrayUtil;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SchoolPoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method SchoolPoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method SchoolPoint[]    findAll()
 * @method SchoolPoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolPointRepository extends BaseRepository
{
    public function fetchSchoolPoint(&$schoolDtos, $year, $dtoClass, $mapField)
    {
        $schoolIds = array_map(function ($school) {
            return $school['id'];
        }, $schoolDtos);

        $conn = $this->getConnection();

        $sql = '
            select *
            from school_point
            where school_id in (:schoolIds) AND year = :year
        ';

        $schoolPoints = $conn->fetchAllAssociative(
            $sql,
            [
                'schoolIds' => $schoolIds,
                'year' => $year,
            ],
            [
                'schoolIds' => \Doctrine\DBAL\Connection::PARAM_INT_ARRAY,
            ]
        );

        $schoolPoints = ArrayUtil::keyBy(
            $this->autoMapper->mapMultiple($schoolPoints, $dtoClass),
            'schoolId'
        );

        foreach ($schoolDtos as &$schoolDto) {
            $schoolId = $schoolDto['id'];
            if (isset($schoolPoints[$schoolId]))  {
                $schoolDto[$mapField] = $schoolPoints[$schoolId];
            }
        }
    }
}
