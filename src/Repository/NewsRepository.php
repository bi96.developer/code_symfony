<?php

namespace App\Repository;

use App\Entity\News;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends BaseRepository
{

  public function getListAllNews($requestData = null, $outputClass)
  {

    $nameEntity = $this->getEntityName();

    $queryBuilder = $this->createQueryBuilder($nameEntity);
    $queryBuilder->addOrderBy($nameEntity . '.id', 'DESC');

    if (!$queryBuilder || !$outputClass) {
      return $queryBuilder->getQuery()->getArrayResult();
    }

    return $this->getPagingData($queryBuilder, $requestData, $outputClass);
  }

  public function getListLastNewsLimit($outputClass, $limit)
  {
    $nameEntity = $this->getEntityName();

    $queryBuilder = $this->createQueryBuilder($nameEntity);
    $queryBuilder->addOrderBy($nameEntity . '.id', 'DESC');
    $queryBuilder->setMaxResults($limit);
    $news = $queryBuilder->getQuery()->getResult();

    if (!$news || !$outputClass) {
      return $news;
    }

    return $this->autoMapper->mapMultiple($news, $outputClass);
  }

  public function getListLastNewsLimitNotId($outputClass, $limit, $id)
  {
    $nameEntity = $this->getEntityName();

    $queryBuilder = $this->createQueryBuilder($nameEntity);
    $queryBuilder->andWhere($nameEntity . '.id != :id');
    $queryBuilder->setParameter('id', $id);
    $queryBuilder->addOrderBy($nameEntity . '.id', 'DESC');
    $queryBuilder->setMaxResults($limit);
    $news = $queryBuilder->getQuery()->getResult();

    if (!$news || !$outputClass) {
      return $news;
    }

    return $this->autoMapper->mapMultiple($news, $outputClass);
  }

  public function getNewsBySlug($slug, $outputClass)
  {
    $news = $this->findOneBy([
      'slug' => $slug,
      'status' => News::STATUS_ACTIVE
    ]);

    if (!$news) {
      return null;
    }

    return $this->autoMapper->map($news, $outputClass);
  }


  public function getEntityOther($entity, $filed, $value)
  {
    $id = $entity->getId();

    $conn = $this->getEntityManager()->getConnection();
    $stmt = $conn->executeQuery(
      'select count(id) as id from news where id != :id and slug = :slug',
      [
        'id' => $id,
        $filed => $value
      ]
    );
    $result = $stmt->fetchColumn();
    return (int) $result;
  }

  public function getListNews($requestData, $outputClass)
  {
    if ($requestData instanceof Request) {
      $requestData = $requestData->query->all();
    }

    $queryBuilder = $this->createQueryBuilder($this->getEntityName());

    if (!$queryBuilder || !$outputClass) {
      return $queryBuilder->getQuery()->getArrayResult();
    }

    $this->setFilter($queryBuilder, $requestData);
    $this->setSort($queryBuilder, $requestData);

    return $this->getPagingData($queryBuilder, $requestData, $outputClass);
  }

  public function getById($id, $outputClass = null)
  {
    $news = $this->findOneBy([
      'id' => $id
    ]);

    if (!$news || !$outputClass) {
      return $news;
    }

    return $this->autoMapper->map($news, $outputClass);
  }
}