<?php

namespace App\Repository;

use App\Entity\School;
use App\Service\YearRankingService;
use Symfony\Component\HttpFoundation\Request;

class SchoolRepository extends BaseRepository
{
    public function getSchoolById($id, $dtoClass)
    {
        $qb = $this->createQueryBuilder('School');
        $qb->leftJoin('School.schoolLink', 'SchoolLink');

        if (!$school = $this->find($id)) {
            return;
        }

        return (array) $this->autoMapper->map($school, $dtoClass);
    }

    public function getPublishedSchoolBy($field, $value, $dtoClass)
    {
        $school = $this->findOneBy([
            $field => $value,
            'status' => School::STATUS_ACTIVE,
            'isPublished' => true
        ]);

        if (!$school) {
            return null;
        }

        $schoolDto = $this->autoMapper->map($school, $dtoClass);

        return (array) $schoolDto;
    }

    public function getListSchoolsByType($schoolTypeId, $year, $requestData, $dtoClass)
    {
        if ($requestData instanceof Request) {
            $requestData = $requestData->query->all();
        }

        $qb = $this->createQueryBuilder($this->getEntityName());

        $qb
            ->andWhere(
                $qb->expr()->eq('School.schoolType', $schoolTypeId)
            )
            ->andWhere(
                'School.point > 0'
            )
            ->andWhere('School.status = :statusActive')
            ->setParameter('statusActive', School::STATUS_ACTIVE)
            ->andWhere('School.isPublished = 1');

        $qb->orderBy('School.point', 'DESC');

        return $this->getPagingData($qb, $requestData, $dtoClass);
    }

    public function fetchRank(&$schools, $isFullRank = true)
    {
        if (empty($schools)) {
            return;
        }

        $isList = !isset($schools['id']);
        $ids = $isList ? array_column($schools, 'id') : [$schools['id']];
        $conn = $this->getEntityManager()->getConnection();
        $SQL = "
            select a.id,
              count(b.name)+1 as rankNumber
            from school a 
              left join school b 
                on (a.point<b.point 
                      or (a.point = b.point && a.updated_date < b.updated_date)) 
                    and b.point is not null
            where a.point is not null and a.point > 0
              and a.id in (?)
            group by a.id";

        $result = $conn
            ->executeQuery($SQL, array($ids), array(
                \Doctrine\DBAL\Connection::PARAM_INT_ARRAY
            ))->fetchAll();
        $rankMap = [];
        foreach ($result as $rank) {
            $rankMap[$rank['id']] = $rank['rankNumber'];
        }
        if ($isList) {
            foreach ($schools as &$school) {
                if (isset($rankMap[$school['id']])) {
                    $school['rank'] = (int)$rankMap[$school['id']];
                }
            }
        } else {
            if (isset($rankMap[$schools['id']])) {
                $schools['rank'] = (int)$rankMap[$schools['id']];
            }
        }
        if ($isFullRank) {
            $this->fetchRankPalmes($schools);
            $this->fetchRankLeague($schools);
        }
    }

    public function fetchRankPalmes(&$schools)
    {
        $isList = !isset($schools['id']);
        $ids = $isList ? array_column($schools, 'id') : [$schools['id']];
        $conn = $this->getEntityManager()->getConnection();
        $SQL = "
            select a.id,
              count(b.name)+1 as rankNumber
            from school a
              left join school b 
                on (a.point < b.point 
                      or (a.point = b.point 
                          && a.updated_date < b.updated_date))
                    and b.point is not null
                    and b.palmes = a.palmes
            where 
              a.point is not null 
              and a.point > 0
              and a.id in (?)
              and a.palmes is not null
            group by a.id";

        $result = $conn
            ->executeQuery($SQL, array($ids), array(
                \Doctrine\DBAL\Connection::PARAM_INT_ARRAY
            ))->fetchAll();
        $rankMap = [];
        foreach ($result as $rank) {
            $rankMap[$rank['id']] = $rank['rankNumber'];
        }
        if ($isList) {
            foreach ($schools as &$school) {
                if (isset($rankMap[$school['id']])) {
                    $school['rankPalmes'] = (int)$rankMap[$school['id']];
                }
            }
        } else {
            if (isset($rankMap[$schools['id']])) {
                $schools['rankPalmes'] = (int)$rankMap[$schools['id']];
            }
        }
    }

    public function fetchRankLeague(&$schools)
    {
        $isList = !isset($schools['id']);
        $ids = $isList ? array_column($schools, 'id') : [$schools['id']];
        $conn = $this->getEntityManager()->getConnection();
        $SQL = "
            select a.id,
              count(b.name)+1 as rankNumber
            from school a
              left join school b 
                on (a.point < b.point 
                      or (a.point = b.point 
                          && a.updated_date < b.updated_date))
                    and b.point is not null
                    and b.league = a.league
            where 
              a.point is not null 
              and a.point > 0
              and a.id in (?)
              and a.palmes is not null
            group by a.id";

        $result = $conn
            ->executeQuery($SQL, array($ids), array(
                \Doctrine\DBAL\Connection::PARAM_INT_ARRAY
            ))->fetchAll();
        $rankMap = [];
        foreach ($result as $rank) {
            $rankMap[$rank['id']] = $rank['rankNumber'];
        }
        if ($isList) {
            foreach ($schools as &$school) {
                if (isset($rankMap[$school['id']])) {
                    $school['rankLeague'] = (int)$rankMap[$school['id']];
                }
            }
        } else {
            if (isset($rankMap[$schools['id']])) {
                $schools['rankLeague'] = (int)$rankMap[$schools['id']];
            }
        }
    }

    public function updateRankBySchoolType($year)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = '
        UPDATE school_point t1
        JOIN (
            SELECT a.id, count(b.id) + 1 as rankNumber
            FROM
                (
                    select sp.school_id id, sp.point, s.school_type_id
                    from school_point sp 
                    join school s on sp.school_id = s.id
                    where sp.year = ? and sp.point > 0
                ) a
                left join (
                    select sp.school_id id, sp.point, s.school_type_id
                    from school_point sp 
                    join school s on sp.school_id = s.id
                    where sp.year = ? and sp.point > 0
                ) b on a.school_type_id = b.school_type_id and a.point < b.point
            group by a.id
        ) t2 on t1.school_id = t2.id and t1.year = ?
        set t1.rank_number = t2.rankNumber
        ';

        $conn->executeStatement($sql, [$year, $year, $year]);
    }

    public function fetchRankSchoolType(&$schools)
    {
        $isList = !isset($schools['id']);
        $ids = $isList ? array_column($schools, 'id') : [$schools['id']];

        $conn = $this->getEntityManager()->getConnection();
        $sql = '
            select a.id,
              count(b.id) + 1 as rankNumber
            from school a
            left join school b 
                on a.point < b.point
                and b.point > 0
                and b.school_type_id = a.school_type_id
                and b.status = 1 and b.is_published = 1
            where a.status = 1 and a.is_published = 1 and a.point > 0
              and a.id in (?)
            group by a.id
        ';

        $result = $conn
            ->executeQuery($sql, array($ids), array(
                \Doctrine\DBAL\Connection::PARAM_INT_ARRAY
            ))->fetchAll();

        $rankMap = [];
        foreach ($result as $rank) {
            $rankMap[$rank['id']] = $rank['rankNumber'];
        }
        if ($isList) {
            foreach ($schools as &$school) {
                if (isset($rankMap[$school['id']])) {
                    $school['rankNumber'] = (int)$rankMap[$school['id']];
                }
            }
        } else {
            if (isset($rankMap[$schools['id']])) {
                $schools['rankNumber'] = (int)$rankMap[$schools['id']];
            }
        }
    }

    public function getMaxOrgId()
    {
        $conn = $this->getEntityManager()->getConnection();
        $SQL = "select max(org_id) from school";
        return (int)$conn->executeQuery($SQL)->fetchOne();
    }

    public function searchSchool($resultsData, $dtoClass)
    {
        $queryBuilder = $this->createQueryBuilder('s');
        $queryBuilder->andWhere("s.schoolType <> ''");
        $queryBuilder->andWhere('s.name LIKE :keyword');

        if (isset($resultsData['idSchoolType'])) {
            $queryBuilder->andWhere('s.schoolType = :idSchoolType');
        }

        $queryBuilder->andWhere('s.status = :schoolStatusActive')
            ->setParameter('schoolStatusActive', School::STATUS_ACTIVE);

        $queryBuilder->andWhere('s.isPublished = :schoolPublished')
            ->setParameter('schoolPublished', true);

        $queryBuilder->setParameter('keyword', '%' . $resultsData['keyword'] . '%');
        if (isset($resultsData['idSchoolType'])) {
            $queryBuilder->setParameter('idSchoolType', $resultsData['idSchoolType']);
        }

        $schoolTypes = $this->autoMapper->mapMultiple(
            $queryBuilder->getQuery()->getResult(),
            $dtoClass
        );

        return $schoolTypes;
    }

    public function getPublishedSchoolsBySchoolType($schoolTypeId, $dtoClass, $order = null, $limit = null)
    {
        $school = $this->findBy([
            'schoolType' => $schoolTypeId,
            'status' => School::STATUS_ACTIVE,
            'isPublished' => true
        ], $order, $limit);

        return (array) $this->autoMapper->mapMultiple($school, $dtoClass);
    }
}
