<?php

namespace App\Repository;

class PostRepository extends BaseRepository
{
  public function getLatestReview() {
      $queryBuilder = $this->createQueryBuilder('Post');
      $queryBuilder->andWhere('Post.type = 4');
      $queryBuilder->addOrderBy('Post.createdDate', 'DESC');
      $queryBuilder->setMaxResults(1);
      $result = $queryBuilder->getQuery()->getResult();
      if(count($result) == 0) return null;
      return $result[0];
  }
}
