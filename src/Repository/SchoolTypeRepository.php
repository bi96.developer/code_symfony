<?php

namespace App\Repository;

use App\Entity\SchoolType;
use App\Misc\ArrayUtil;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SchoolType|null find($id, $lockMode = null, $lockVersion = null)
 * @method SchoolType|null findOneBy(array $criteria, array $orderBy = null)
 * @method SchoolType[]    findAll()
 * @method SchoolType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolTypeRepository extends BaseRepository
{
    public function getAllSchoolTypes($dtoClass)
    {
        $results = $this->findAll();

        return $this->autoMapper->mapMultiple($results, $dtoClass);
    }

    public function getAllPublishedSchoolTypes($dtoClass)
    {

        $results = $this->findBy([
            'status'      => SchoolType::STATUS_ACTIVE,
            'isPublished' => true
        ]);

        return $this->autoMapper->mapMultiple($results, $dtoClass);
    }

    public function getPublishedSchoolTypeById($id, $dtoClass)
    {
        $result = $this->findOneBy([
            'id'          => $id,
            'status'      => SchoolType::STATUS_ACTIVE,
            'isPublished' => true
        ]);

        return $this->autoMapper->map($result, $dtoClass);
    }

    public function getPublishedSchoolTypeBySlug($slug, $dtoClass)
    {
        $result = $this->findOneBy([
            'slug'        => $slug,
            'status'      => SchoolType::STATUS_ACTIVE,
            'isPublished' => true
        ]);

        return $this->autoMapper->map($result, $dtoClass);
    }

    public function fetchSchoolType(&$schoolDtos, $dtoClass, $mapField = 'schoolType')
    {
        $schoolTypeIds = array_map(function ($school) {
            return $school['schoolTypeId'];
        }, $schoolDtos);
        if (count($schoolTypeIds) === 0) {
            return;
        }
        $queryBuilder = $this->createQueryBuilder('SchoolType');

        $queryBuilder->where(
            $queryBuilder->expr()->in('SchoolType.id', $schoolTypeIds)
        );

        $schoolTypes = $this->autoMapper->mapMultiple(
            $queryBuilder->getQuery()->getResult(),
            $dtoClass
        );

        $schoolTypes = ArrayUtil::keyBy($schoolTypes, 'id');

        foreach ($schoolDtos as &$schoolDto) {
            $schoolTypeId = $schoolDto['schoolTypeId'];

            if (isset($schoolTypes[$schoolTypeId])) {
                $schoolDto[$mapField] = $schoolTypes[$schoolTypeId];
            }
        }
    }
}
