<?php

namespace App\Repository;

use App\Entity\YearRanking;

/**
 * @method YearRanking|null find($id, $lockMode = null, $lockVersion = null)
 * @method YearRanking|null findOneBy(array $criteria, array $orderBy = null)
 * @method YearRanking[]    findAll()
 * @method YearRanking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class YearRankingRepository extends BaseRepository
{

}
