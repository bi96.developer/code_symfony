<?php

namespace App\Repository;

use App\Entity\Redirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Redirection|null find($id, $lockMode = null, $lockVersion = null)
 * @method Redirection|null findOneBy(array $criteria, array $orderBy = null)
 * @method Redirection[]    findAll()
 * @method Redirection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RedirectionRepository extends BaseRepository
{
    public function checkRedirectionSchool($path)
    {

        $conn = $this->getEntityManager()->getConnection();
        $sql = 'select target_id
                from redirection
                where type = ? and FIND_IN_SET( ? , url_old)';

        return $conn->executeQuery($sql, [Redirection::SCHOOL, $path])->fetchColumn();
    }

    public function checkRedirectionSchoolType($path)
    {

        $conn = $this->getEntityManager()->getConnection();
        $sql = 'select target_id
                from redirection
                where type = ? and FIND_IN_SET( ? , url_old)';

        return $conn->executeQuery($sql, [Redirection::SCHOOL_TYPE, $path])->fetchColumn();
    }
}