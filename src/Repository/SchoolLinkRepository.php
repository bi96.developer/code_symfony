<?php

namespace App\Repository;

use App\Entity\SchoolLink;
use App\Misc\ArrayUtil;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SchoolLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method SchoolLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method SchoolLink[]    findAll()
 * @method SchoolLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolLinkRepository extends BaseRepository
{
    public function fetchSchoolLink(&$schoolDtos, $dtoClass = 'App\DTO\SchoolLink\SchoolLinkOutput', $mapField = 'schoolLink')
    {
        $schoolIds = array_map(function ($school) {
            return $school['id'];
        }, $schoolDtos);

        $conn = $this->getConnection();

        $sql = '
            select *
            from school_link
            where school_id in (:schoolIds)
        ';

        $schoolLinks = $conn->fetchAllAssociative(
            $sql,
            ['schoolIds' => $schoolIds],
            ['schoolIds' => \Doctrine\DBAL\Connection::PARAM_INT_ARRAY]
        );

        $schoolLinkDtos = ArrayUtil::keyBy(
            $this->autoMapper->mapMultiple($schoolLinks, $dtoClass),
            'schoolId'
        );

        foreach ($schoolDtos as &$schoolDto) {
            $schoolId = $schoolDto['id'];
            if (isset($schoolLinkDtos[$schoolId]))  {
                $schoolDto[$mapField] = $schoolLinkDtos[$schoolId];
            }
        }
    }
}
