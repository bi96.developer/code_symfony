<?php

namespace App\Repository;

use App\Entity\Page;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends BaseRepository
{
    public function getPageBySlug($slug, $outputClass = null)
    {
        $page = $this->findOneBy([
            'slug' => $slug
        ]);

        if (!$page || !$outputClass) {
            return $page;
        }

        return $this->autoMapper->map($page, $outputClass);
    }
}
