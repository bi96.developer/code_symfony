<?php

namespace App\Repository;

use App\Entity\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Country|null find($id, $lockMode = null, $lockVersion = null)
 * @method Country|null findOneBy(array $criteria, array $orderBy = null)
 * @method Country[]    findAll()
 * @method Country[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CountryRepository extends BaseRepository
{
    public function getTotalCountry()
    {
        $conn = $this->getConnection();

        $stmt = $conn->executeQuery('
            SELECT COUNT(*) as total
            FROM country
            WHERE id IN (
                SELECT DISTINCT country_id
                FROM course
            ) 
        ');

        return (int) $stmt->fetchColumn();
    }
}
