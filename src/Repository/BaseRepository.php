<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

use App\Misc\CustomEntityManager;
use App\Service\CommonService;
use AutoMapperPlus\AutoMapperInterface;

/**
 * @property \AutoMapperPlus\AutoMapperInterface $autoMapper
 */
class BaseRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        CustomEntityManager $customEntityManager,
        CommonService $commonService,
        AutoMapperInterface $autoMapper,
        ContainerInterface $container
    ) {
        $this->commonService = $commonService;
        $thisName = $this->commonService->getClassName($this, true);
        parent::__construct($registry, 'App\Entity\\' . $thisName);
        $this->_em = $customEntityManager;
        $this->autoMapper = $autoMapper;
        $this->container = $container;
    }

    public function getEntityManager()
    {
        return $this->_em;
    }

    public function save($entity, $inTransaction = false)
    {
        $this->getEntityManager()->persist($entity);
        if (!$inTransaction) {
            $this->getEntityManager()->flush();
        }
    }

    public function persist($entity)
    {
        $this->getEntityManager()->persist($entity);
    }

    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    public function clear()
    {
        $this->getEntityManager()->clear();
    }

    public function delete($entities, $inTransaction = false)
    {
        if (!is_array($entities)) {
            $entities = [$entities];
        }
        foreach ($entities as $entity) {
            if ($entity instanceof \App\Entity\Post) {
                $entity->setType(\App\Entity\Post::TYPE_DELETED);
            } else if ($entity instanceof \App\Entity\Media) {
                $entity->setType(\App\Entity\Media::TYPE_DELETED);
            } else {
                $this->getEntityManager()->remove($entity);
            }
        }
        if (!$inTransaction) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return \Doctrine\DBAL\Connection
     */
    public function getConnection()
    {
        return $this->getEntityManager()->getConnection();
    }

    public function __get($propertyName)
    {
        preg_match('/([a-zA-Z0-9]+)Service/i', $propertyName, $serviceMatches);
        if (count($serviceMatches) > 0) {
            return $this
                ->container
                ->get('App\Service\\' . ucfirst($serviceMatches[1]) . 'Service');
        }

        preg_match('/([a-zA-Z0-9]+)Repo/i', $propertyName, $repositoryMatches);
        if (count($repositoryMatches) > 0) {
            return $this
                ->container
                ->get('App\Service\\' . ucfirst($repositoryMatches[1]) . 'Service')
                ->repository;
        }

        $entityName = $this->commonService->getClassName($this, true);
        $cacheKey = 'Const.' . $entityName . '.' . $propertyName;
        $cachedConstantItem = $this->commonService->cache->getItem($cacheKey);

        if ($cachedConstantItem->isHit()) {
            return $cachedConstantItem->get();
        }

        $constant = @constant('App\Entity\\' . $entityName . '::' . $propertyName);
        if (!is_null($constant)) {
            $cachedConstantItem->set($constant);
            $this->commonService->cache->save($cachedConstantItem);
            return $constant;
        }

        $metas = $this->_em->getMetadataFactory()->getAllMetadata();
        foreach ($metas as $meta) {
            $classPath = $meta->getName();
            $name = strtoupper($this->commonService->toSnakeCase(str_replace('App\Entity\\', '', $classPath)));
            preg_match('/(' . $name . ')_(.+)/i', $propertyName, $matches);
            if (count($matches) > 0) {
                $prop = $matches[2];
                $constant = @constant($classPath . '::' . $prop);
                if (!is_null($constant)) {
                    $cachedConstantItem->set($constant);
                    $this->commonService->cache->save($cachedConstantItem);
                    return $constant;
                }
            }
        }

        trigger_error('Could not found property ' . $propertyName . ' in ' . $this->commonService->getClassName($this), E_USER_ERROR);
    }

    public function getPagingData($queryBuilder, $paging = null, $DTO = null)
    {
        if (is_null($paging)) {
            $paging = [
                'page'  => 1,
                'limit' => 20
            ];
        }
        if (!isset($paging['page'])) {
            $paging['page'] = 1;
        }
        if (!isset($paging['limit'])) {
            $paging['limit'] = 20;
        }
        $queryBuilder
            ->setFirstResult(($paging['page'] - 1) * $paging['limit'])
            ->setMaxResults($paging['limit']);

        $paginator = new Paginator($queryBuilder, $fetchJoinCollection = true);
        $total = count($paginator);
        $list = [];
        foreach ($paginator as $entity) {
            if (is_null($DTO)) {
                $outputDTO = 'App\DTO\\' . $this->getEntityName() . '\\' . $this->getEntityName() . 'Output';
                if (class_exists($outputDTO)) {
                    $DTO = $outputDTO;
                }
            }
            if (is_null($DTO)) {
                $list[] = $entity;
            } else {
                $this->autoMapper->map($entity, $DTO);

                $array = (array)$this->autoMapper->map($entity, $DTO);
                foreach ($array as $key => $value) {
                    if (is_string($value)) {
                        $array[$key] = mb_convert_encoding($value, "UTF-8");
                    }
                }
                $list[] = $array;
            }
        }
        return [
            'total'       => $total,
            'totalPages'  => ceil($total / $paging['limit']),
            'list'        => $list,
            'currentPage' => 1 * $paging['page']
        ];
    }

    public function setFilter($queryBuilder, $requestData)
    {
        foreach ($requestData as $key => $value) {
            if ($value === '') {
                continue;
            }
            if (strpos($key, 'filter_') !== false) {
                $filter = str_replace('filter_', '', $key);
                if (strpos($filter, '_like') !== false) {
                    $fieldName = $this->getEntityName() . '.' . str_replace('_like', '', $filter);
                    $parameterKey = uniqid('k_');
                    $queryBuilder->andWhere(
                        $queryBuilder
                            ->expr()
                            ->like($fieldName, ':' . $parameterKey)
                    );
                    $queryBuilder->setParameter($parameterKey, '%' . $value . '%');
                } else {
                    $fieldName = $this->getEntityName() . '.' . $filter;
                    if (is_array($value)) {
                        $queryBuilder
                            ->andWhere(
                                $queryBuilder
                                    ->expr()
                                    ->in($fieldName, ':in' . $filter)
                            )
                            ->setParameter(
                                'in' . $filter,
                                $value,
                                \Doctrine\DBAL\Connection::PARAM_INT_ARRAY
                            );
                    } else if ($value === 'falseOrNull') {
                        $queryBuilder->andWhere($fieldName . ' = 0 or ' . $fieldName . ' is NULL');
                    } else if ($value === 'isNull') {
                        $queryBuilder->andWhere($fieldName . ' is NULL');
                    } else if ($value === 'true' || $value === 'false') {
                        $queryBuilder->andWhere(
                            $queryBuilder->expr()->eq($fieldName, ':' . $filter)
                        );
                        $queryBuilder->setParameter($filter, $value === 'true');
                    } else {
                        $queryBuilder->andWhere(
                            $queryBuilder->expr()->eq($fieldName, ':' . $filter)
                        );
                        $queryBuilder->setParameter($filter, $value);
                    }
                }
            } else if (strpos($key, 'except_') !== false) {
                $except = str_replace('except_', '', $key);
                $isInt = is_int(explode(',', $value)[0]);
                if ($isInt) {
                    $value = array_map('intval', explode(',', $value));
                } else {
                    $value = array_map('strval', explode(',', $value));
                }
                $fieldName = $this->getEntityName() . '.' . $except;

                $queryBuilder
                    ->andWhere(
                        $queryBuilder
                            ->expr()
                            ->orX(
                                $queryBuilder->expr()->isNull($fieldName),
                                $queryBuilder
                                    ->expr()
                                    ->notIn($fieldName, ':notIn' . $except)
                            )
                    )
                    ->setParameter(
                        'notIn' . $except,
                        $value,
                        $isInt
                            ? \Doctrine\DBAL\Connection::PARAM_INT_ARRAY
                            : \Doctrine\DBAL\Connection::PARAM_STR_ARRAY
                    );
            } else if (strpos($key, 'has_') !== false) {
                $has = str_replace('has_', '', $key);
                $relationName = explode('_', $has)[0];
                $relationProperty = explode('_', $has)[1];
                $fieldName = $this->getEntityName() . '.' . $relationName;
                $queryBuilder
                    ->leftJoin($fieldName, $relationName)
                    ->andWhere($relationName . '.' . $relationProperty . ' = :' . $relationName . $relationProperty)
                    ->setParameter(
                        $relationName . $relationProperty,
                        $value
                    );
            } else if (strpos($key, 'memberOf_') !== false) {
                $relationName = str_replace('memberOf_', '', $key);
                $fieldName = $this->getEntityName() . '.' . $relationName;
                $queryBuilder
                    ->leftJoin($fieldName, $relationName)
                    ->andWhere(':' . $relationName . ' member of ' . $fieldName)
                    ->setParameter(
                        $relationName,
                        $value
                    );
            }
        }
    }

    public function setSort($queryBuilder, $requestData)
    {
        foreach ($requestData as $key => $value) {
            if (strpos($key, 'sort_') === false) {
                continue;
            }

            if (strpos($value, '.') === false) {
                $fieldName = $this->getEntityName() . '.' . $value;
            } else {
                $fieldName = $value;
            }

            $queryBuilder->addOrderBy($fieldName, strpos($key, '_asc') !== false ? 'ASC' : 'DESC');
        }
    }

    public function getList($requestData, $DTO = null, callable $extraFilter = null)
    {
        if ($requestData instanceof Request) {
            $requestData = $requestData->query->all();
        }
        $queryBuilder = $this->createQueryBuilder($this->getEntityName());
        if (!is_null($extraFilter)) {
            $extraFilter($queryBuilder);
        }
        $this->setFilter($queryBuilder, $requestData);
        $this->setSort($queryBuilder, $requestData);

        return $this->getPagingData($queryBuilder, $requestData, $DTO);
    }

    public function getDistinctAttributeOfList($requestData, $DTO = null, callable $extraFilter = null)
    {
        $entityName = $this->getEntityName();
        if ($requestData instanceof Request) {
            $requestData = $requestData->query->all();
        }
        if (isset($requestData['distinct_attribute'])) {
            $attributeType = 'attribute';
        } else if (isset($requestData['distinct_relation'])) {
            $attributeType = 'relation';
        } else return [];

        $queryBuilder = $this->createQueryBuilder($entityName);
        if (!is_null($extraFilter)) {
            $extraFilter($queryBuilder);
        }
        $this->setFilter($queryBuilder, $requestData);
        $this->setSort($queryBuilder, $requestData);

        if ($attributeType == 'attribute') {
            $queryBuilder
                ->select($entityName . '.' . $requestData['distinct_attribute'])
                ->groupBy($entityName . '.' . $requestData['distinct_attribute']);
        } else if ($attributeType == 'relation') {
            $queryBuilder
                ->join($entityName . '.' . $requestData['distinct_relation'], $requestData['distinct_relation'])
                ->addSelect($requestData['distinct_relation'])
                ->addSelect('COUNT(' . $entityName . ') as count')
                ->groupBy($requestData['distinct_relation']);
        }
        $result = $queryBuilder->getQuery()->getResult();
        $attributeList = [];
        foreach ($result as $entity) {
            $attributeName = $requestData[$attributeType == 'attribute' ? 'distinct_attribute' : 'distinct_relation'];
            $attribute = $entity[0]->{'get' . $attributeName}();
            //var_dump($attribute);
            if ($attributeType == 'attribute') {
                $attributeList[] = [
                    'value' => $attribute,
                    'count' => (int) $entity['count'],
                ];
            } else if ($attributeType == 'relation') {
                $array = !is_null($DTO)
                    ? (array) $this->autoMapper->map($attribute, $DTO)
                    : (array) $attribute;
                foreach ($array as $key => $value) {
                    if (is_string($value)) {
                        $array[$key] = mb_convert_encoding($value, "UTF-8");
                    }
                }
                $array['count'] = (int) $entity['count'];
                $attributeList[] = $array;
            }
        }
        return $attributeList;
    }

    public function entityToDtoArray($entity, $DTO = null)
    {
        $array = (array)$this->autoMapper->map($entity, $DTO);
        foreach ($array as $key => $value) {
            if (is_string($value)) {
                $array[$key] = mb_convert_encoding($value, "UTF-8");
            }
        }

        return $array;
    }

    public function getEntityName()
    {
        return $this->commonService->getClassName($this, true);
    }
}