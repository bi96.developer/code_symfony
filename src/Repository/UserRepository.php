<?php

namespace App\Repository;

class UserRepository extends BaseRepository
{
    public function recountTotalFollowing($userIdList) {
        if(empty($userIdList)) return;
        $conn = $this->getEntityManager()->getConnection();
        $conn->executeUpdate('
        update user u1 inner join (
          Select u.id, count(f.id) as following
          from user u 
            join profile p on u.id = p.user_id
            left join follow f on f.profile_id = p.id
          where u.id in (' . implode(',', $userIdList) . ')
          group by u.id
          ) t on u1.id = t.id
        set u1.total_following = t.following
        where u1.id in (' . implode(',', $userIdList) . ')
        ');
    }
    
    public function recountTotalFollower($userIdList) {
        if(empty($userIdList)) return;
        $conn = $this->getEntityManager()->getConnection();
        $conn->executeUpdate('
        Update user u1 inner join (
          select 
            u.id as id,
            count(f.id) as totalFollower
          from user u 
            left join follow f on f.entity_id = u.id and f.entity_name = "user"
          where u.id in (' . implode(',', $userIdList) . ')
          group by u.id
          ) u2 on u1.id = u2.id
        set u1.total_follow = u2.totalFollower
        where u1.id in (' . implode(',', $userIdList) . ')
        ');
    }
    
}
