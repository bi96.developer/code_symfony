<?php

namespace App\Annotation;

/**
 * @Annotation
 */
class TransformAnnotation
{
    public $type;

    public $class;
    
    public $field;
    
    public $isRichText;
}
