$(document).ready(function () {
	// remove WoW animation on mobile
	wow = new WOW(
      {
      	mobile: false,
      }
    )
  wow.init();

	//Initialize for Banner/Hompage
	let options = {};
  if ( $(".mySwiper .swiper-slide").length == 1 ) {
 		$('.classement-slider .mySwiper').addClass('only-child');
 		options = {
      loop: false,
      autoplay: false,
    }
  } else {
  	$('.classement-slider .mySwiper').removeClass('only-child');
  	options = {
      slidesPerView: 1,
	    spaceBetween: 0,
	    pagination: {
	      el: ".swiper-pagination",
	      clickable: true,
	    },
	    navigation: {
	      nextEl: ".button-next",
	      prevEl: ".button-prev",
	    },
	    loop: true,
	    autoplay: {
	      delay: 4000,
	      disableOnInteraction: false,
	    },
	    breakpoints: {
	      768: {
	        slidesPerView: 1,
	        spaceBetween: 30,
	      }
	    }
	  }
  }
	var swiper = new Swiper(".mySwiper", options);

	function initSwiper() {
    var screenWidth = $(window).width();
    if(screenWidth < 992) {
    	$('.classement-slider .mySwiper').addClass('mobile-list');
      swiper.destroy(false,true);
    } else {
    	$('.classement-slider .mySwiper').removeClass('mobile-list');
      swiper = new Swiper(".mySwiper", options);
    }
	}

	if($('.classement-slider').length){
		initSwiper();

		//Swiper plugin initialization on window resize
		$(window).on('resize', function(){
		    initSwiper();
		});
	}

	// CLOSE COOKIES
	$("#close-ck").click(function () {
		$(".footer-cookies").slideUp("200");
	});

	pageScroll();

	$('[data-toggle="tooltip"]').tooltip();

	$('.js-count').appear(function() {
		var count;
		count = $(this);
		count.countTo({
			speed: 1200,
			refreshInterval: 60
		});
	});


	if ($('#search_name_school').length > 0) {
		let urlSearch;
		if (typeof searchIdSchoolType !== 'undefined') {
			urlSearch = "/search?idSchoolType="+searchIdSchoolType;
		}else{
			urlSearch = "/search";
		}
		$.typeahead({
			input: "#search_name_school",
			maxItem: 20,
			minLength: 3,
			searchOnFocus: true,
			hint: true,
			dynamic: true,
			delay: 300,
			accent: true,
			template: $('#school-item-template').html(),
			source: {
				'Établissement': {
					display: 'name',
					ajax: function (query) {
						return {
							type: "GET",
							url: urlSearch,
							data: {
								keyword: "{{query}}"
							},
							callback: {
								done: function (data) {
									return data;
								}
							}
						}
					}
				},
			},
			emptyTemplate: $('#empty-template').html(),
			callback: {
				onClickBefore: function (param1, param2, node) {
					window.location = node.url;
				},
				onLayoutBuiltAfter: 'typeahead_scrollable'
			}
		});
	}
});

function typeahead_scrollable() {
	$(".typeahead__list").mCustomScrollbar();
}

$(window).on("load resize", function() {
  $('.classement-item .c-box').syncHeight({updateOnResize: true});
  //$('.box-green').syncHeight({updateOnResize: true});
});

function pageScroll() {
  $('a.page-scroll').on('click', function(e){
  	e.stopPropagation();
		var anchor = $(this);
		var href = anchor.attr('href').substring(1);
		var $href = $('#' + href);
		if($href.length > 0)
		{
			$('html, body').stop().animate({
				scrollTop: $href.offset().top - 100
			}, 800);
		}
	});

	var hash = window.location.hash;
	if (hash != '') {
	    if(hash.length > 0)
	    {
	        $('html, body').animate({
	            'scrollTop': $(hash).offset().top - 100
	        }, 1000);
	    }
	};
}
